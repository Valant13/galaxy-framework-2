﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VRage;
using VRage.Collections;
using VRage.Game;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ObjectBuilders.Definitions;
using VRageMath;

namespace IngameScript
{
    partial class Program
    {
        public class UiApi_IdLayoutBuilder
        {
            private UiApi_Data_IdLayout layout = new UiApi_Data_IdLayout();

            public UiApi_IdLayoutBuilder Reset()
            {
                layout = new UiApi_Data_IdLayout();

                return this;
            }

            public UiApi_IdLayoutSectionInstaller AddSection(string title = null)
            {
                var section = new UiApi_Data_IdSectionLayout()
                {
                    Title = title
                };

                layout.Sections.Add(section);

                return new UiApi_IdLayoutSectionInstaller(section);
            }

            public UiApi_Data_IdLayout Build()
            {
                var result = layout;
                Reset();

                return result;
            }
        }
    }
}
