﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VRage;
using VRage.Collections;
using VRage.Game;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ObjectBuilders.Definitions;
using VRageMath;

namespace IngameScript
{
    partial class Program
    {
        public class UiApi_Data_WsGenericBinding<T> : UiApi_Data_WsBinding
        {
            public delegate UiApi_Data_WsStatus MapperDelegate(T value);

            private MapperDelegate mapper;

            public UiApi_Data_WsGenericBinding(MapperDelegate mapper)
            {
                this.mapper = mapper;
            }

            public void MapStatus(T status)
            {
                SetStatus(mapper(status));
            }
        }
    }
}
