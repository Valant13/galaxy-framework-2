﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VRage;
using VRage.Collections;
using VRage.Game;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ObjectBuilders.Definitions;
using VRageMath;

namespace IngameScript
{
    partial class Program
    {
        public class UiApi_Data_MfdIndicatorBinding
        {
            public event EventHandler StatusChanged;

            private UiApi_Data_MfdIndicatorStatus status;

            public UiApi_Data_MfdIndicatorStatus GetStatus()
            {
                return status;
            }

            public void SetStatus(UiApi_Data_MfdIndicatorStatus status)
            {
                this.status = status;

                StatusChanged?.Invoke(this, new EventArgs());
            }
        }
    }
}
