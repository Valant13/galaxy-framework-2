﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VRage;
using VRage.Collections;
using VRage.Game;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ObjectBuilders.Definitions;
using VRageMath;

namespace IngameScript
{
    partial class Program
    {
        public class UiApi_Data_IdGenericRecordBinding<T> : UiApi_Data_IdRecordBinding
        {
            public delegate string RendererDelegate(T value);
            public delegate bool NullCondition(T value);

            private RendererDelegate renderer;
            private NullCondition nullCondition;

            public UiApi_Data_IdGenericRecordBinding()
            { }

            public UiApi_Data_IdGenericRecordBinding(RendererDelegate renderer)
            {
                this.renderer = renderer;
            }

            public UiApi_Data_IdGenericRecordBinding(RendererDelegate renderer, NullCondition nullCondition)
            {
                this.renderer = renderer;
                this.nullCondition = nullCondition;
            }

            public void RenderValue(T value)
            {
                if (value == null)
                {
                    SetValue(null);
                }
                else if (nullCondition != null && nullCondition(value))
                {
                    SetValue(null);
                }
                else if (renderer != null)
                {
                    SetValue(renderer(value));
                }
                else
                {
                    SetValue(value.ToString());
                }
            }
        }
    }
}
