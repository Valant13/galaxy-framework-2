﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VRage;
using VRage.Collections;
using VRage.Game;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ObjectBuilders.Definitions;
using VRageMath;

namespace IngameScript
{
    partial class Program
    {
        public class UiApi_Data_MfdLayout
        {
            public List<UiApi_Data_MfdSectionLayout> Sections { get; set; } = new List<UiApi_Data_MfdSectionLayout>();
            public Dictionary<int, UiApi_Data_MfdButtonLayout> Buttons { get; set; } = new Dictionary<int, UiApi_Data_MfdButtonLayout>();
            public Vector2I GridSize { get; set; } = Vector2I.Zero;
        }
    }
}
