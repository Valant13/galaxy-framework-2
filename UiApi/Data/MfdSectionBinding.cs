﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VRage;
using VRage.Collections;
using VRage.Game;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ObjectBuilders.Definitions;
using VRageMath;

namespace IngameScript
{
    partial class Program
    {
        public class UiApi_Data_MfdSectionBinding<T>
        {
            private Dictionary<T, UiApi_Data_MfdIndicatorBinding> indicators = new Dictionary<T, UiApi_Data_MfdIndicatorBinding>();

            public UiApi_Data_MfdSectionBinding(List<T> indicatorKeys)
            {
                foreach (var key in indicatorKeys)
                {
                    indicators[key] = new UiApi_Data_MfdIndicatorBinding();
                }
            }

            public void HideIndicators(List<T> indicatorKeys = null)
            {
                SetIndicatorStatuses(UiApi_Data_MfdIndicatorStatus.Hidden, indicatorKeys);
            }

            public void HideIndicator(T indicatorKey)
            {
                SetIndicatorStatus(UiApi_Data_MfdIndicatorStatus.Hidden, indicatorKey);
            }

            public void DisableIndicators(List<T> indicatorKeys = null)
            {
                SetIndicatorStatuses(UiApi_Data_MfdIndicatorStatus.Disabled, indicatorKeys);
            }

            public void DisableIndicator(T indicatorKey)
            {
                SetIndicatorStatus(UiApi_Data_MfdIndicatorStatus.Disabled, indicatorKey);
            }

            public void EnableIndicators(List<T> indicatorKeys = null)
            {
                SetIndicatorStatuses(UiApi_Data_MfdIndicatorStatus.Enabled, indicatorKeys);
            }

            public void EnableIndicator(T indicatorKey)
            {
                SetIndicatorStatus(UiApi_Data_MfdIndicatorStatus.Enabled, indicatorKey);
            }

            public UiApi_Data_MfdIndicatorBinding this[T indicatorKey]
            {
                get { return GetIndicator(indicatorKey); }
            }

            public UiApi_Data_MfdIndicatorBinding GetIndicator(T indicatorKey)
            {
                return indicators[indicatorKey];
            }

            private void SetIndicatorStatuses(UiApi_Data_MfdIndicatorStatus status, List<T> indicatorKeys = null)
            {
                indicatorKeys = indicatorKeys ?? indicators.Keys.ToList();

                foreach (var key in indicatorKeys)
                {
                    indicators[key].SetStatus(status);
                }
            }

            private void SetIndicatorStatus(UiApi_Data_MfdIndicatorStatus status, T indicatorKey)
            {
                indicators[indicatorKey].SetStatus(status);
            }
        }
    }
}
