﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VRage;
using VRage.Collections;
using VRage.Game;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ObjectBuilders.Definitions;
using VRageMath;

namespace IngameScript
{
    partial class Program
    {
        public class UiApi_MfdLayoutSectionInstaller
        {
            private UiApi_Data_MfdSectionLayout section;

            public UiApi_MfdLayoutSectionInstaller(UiApi_Data_MfdSectionLayout section)
            {
                this.section = section;
            }

            public UiApi_MfdLayoutSectionInstaller AddIndicator(string title, UiApi_Data_MfdIndicatorBinding binding, Vector2I positionInGrid)
            {
                var indicator = new UiApi_Data_MfdIndicatorLayout()
                {
                    Title = title,
                    Binding = binding,
                    PositionInGrid = positionInGrid
                };

                section.Indicators.Add(indicator);

                return this;
            }
        }
    }
}
