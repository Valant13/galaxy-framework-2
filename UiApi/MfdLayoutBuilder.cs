﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VRage;
using VRage.Collections;
using VRage.Game;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ObjectBuilders.Definitions;
using VRageMath;

namespace IngameScript
{
    partial class Program
    {
        public class UiApi_MfdLayoutBuilder
        {
            private UiApi_Data_MfdLayout layout = new UiApi_Data_MfdLayout();

            public UiApi_MfdLayoutBuilder Reset()
            {
                layout = new UiApi_Data_MfdLayout();

                return this;
            }

            public UiApi_MfdLayoutSectionInstaller AddSection()
            {
                var section = new UiApi_Data_MfdSectionLayout();

                layout.Sections.Add(section);

                return new UiApi_MfdLayoutSectionInstaller(section);
            }

            public UiApi_MfdLayoutBuilder AddButton(int number, string title, string command)
            {
                var button = new UiApi_Data_MfdButtonLayout()
                {
                    Number = number,
                    Title = title,
                    Command = command
                };

                layout.Buttons[number] = button;

                return this;
            }

            public UiApi_MfdLayoutBuilder SetGridSize(Vector2I gridSize)
            {
                layout.GridSize = gridSize;

                return this;
            }

            public UiApi_Data_MfdLayout Build()
            {
                var result = layout;
                Reset();

                return result;
            }
        }
    }
}
