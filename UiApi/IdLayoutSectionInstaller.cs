﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VRage;
using VRage.Collections;
using VRage.Game;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ObjectBuilders.Definitions;
using VRageMath;

namespace IngameScript
{
    partial class Program
    {
        public class UiApi_IdLayoutSectionInstaller
        {
            private UiApi_Data_IdSectionLayout section;

            public UiApi_IdLayoutSectionInstaller(UiApi_Data_IdSectionLayout section)
            {
                this.section = section;
            }

            public UiApi_IdLayoutSectionInstaller AddRecord(string title, UiApi_Data_IdRecordBinding binding)
            {
                var record = new UiApi_Data_IdRecordLayout()
                {
                    Title = title,
                    Binding = binding
                };

                section.Records.Add(record);

                return this;
            }
        }
    }
}
