﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System;
using VRage.Collections;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ObjectBuilders.Definitions;
using VRage.Game;
using VRage;
using VRageMath;

namespace IngameScript
{
    partial class Program
    {
        public class Config : Framework_Bootstrap_Data_IConfig
        {
            public string GetName()
            {
                return "A 76M";
            }

            public UpdateFrequency GetUpdateFrequency()
            {
                return UpdateFrequency.Update1 | UpdateFrequency.Update10 | UpdateFrequency.Update100;
            }

            public void SetupDependencies()
            {
                Framework_DependencyConfig.Setup();
                TerminalUi_DependencyConfig.Setup();
                FcsWeaponPack_DependencyConfig.Setup();
                Fcs_DependencyConfig.Setup();
                Ui_DependencyConfig.Setup();
            }

            public void SetupComponents()
            {
                var searchCriteriaBuilder = new Framework_Actor_SearchCriteriaBuilder();
                var idLayoutBuilder = new UiApi_IdLayoutBuilder();
                var mfdLayoutBuilder = new UiApi_MfdLayoutBuilder();

                var cockpit1 = searchCriteriaBuilder
                    .SetCustomId("Cockpit1")
                    .Build();

                var cockpit2 = searchCriteriaBuilder
                    .SetCustomId("Cockpit2")
                    .Build();

                new TerminalUi_Component_Display(null, cockpit1, 3, 1.15f, 12);
                new TerminalUi_Component_Display(null, cockpit2, 3, 1.15f, 12);

                new TerminalUi_Component_Display(null, null, 0, 1, 17);

                var mergeBlockL1 = searchCriteriaBuilder
                    .SetCustomId("MergeBlockL1")
                    .Build();

                var mergeBlockL2 = searchCriteriaBuilder
                    .SetCustomId("MergeBlockL2")
                    .Build();

                var mergeBlockL3 = searchCriteriaBuilder
                    .SetCustomId("MergeBlockL3")
                    .Build();

                var mergeBlockR1 = searchCriteriaBuilder
                    .SetCustomId("MergeBlockR1")
                    .Build();

                var mergeBlockR2 = searchCriteriaBuilder
                    .SetCustomId("MergeBlockR2")
                    .Build();

                var mergeBlockR3 = searchCriteriaBuilder
                    .SetCustomId("MergeBlockR3")
                    .Build();

                var camera = searchCriteriaBuilder
                    .SetCustomClass("Camera")
                    .Build();

                var soundBlock = searchCriteriaBuilder
                    .SetCustomId("SoundBlock")
                    .Build();

                var remoteControl = searchCriteriaBuilder
                    .SetCustomId("RemoteControl")
                    .Build();

                var turretBlock = searchCriteriaBuilder
                    .SetCustomId("Turret")
                    .Build();

                var fcsViewModel = new Fcs_ViewModel_FireControlSystem();
                var memoryViewModel = new Fcs_ViewModel_Memory();
                var raycastBasedViewModel = new Fcs_ViewModel_RaycastBased();
                
                idLayoutBuilder
                    .AddSection()
                    .AddRecord("Status", fcsViewModel.State)
                    .AddRecord("Target Distance", fcsViewModel.TargetDistance)
                    .AddRecord("Target Speed", fcsViewModel.TargetSpeed)
                    .AddRecord("Weapon Title", fcsViewModel.WeaponTitle)
                    .AddRecord("Hardpoint Ready", fcsViewModel.IsHardpointReady)
                    .AddRecord("Guided Weapons", fcsViewModel.GuidedWeaponsCount)
                    .AddRecord("Time to Hit", fcsViewModel.TimeToHit);

                idLayoutBuilder
                    .AddSection()
                    .AddRecord("Saved Target", memoryViewModel.TargetPosition);

                idLayoutBuilder
                    .AddSection()
                    .AddRecord("Raycast Interval", raycastBasedViewModel.IntervalTime)
                    .AddRecord("Raycast Distance", raycastBasedViewModel.Distance);

                var idLayout = idLayoutBuilder.Build();

                mfdLayoutBuilder
                    .AddSection()
                    .AddIndicator("L3", fcsViewModel.Hardpoints[FcsApi_Data_HardpointPosition.L3], new Vector2I(0, 0))
                    .AddIndicator("L2", fcsViewModel.Hardpoints[FcsApi_Data_HardpointPosition.L2], new Vector2I(1, 0))
                    .AddIndicator("L1", fcsViewModel.Hardpoints[FcsApi_Data_HardpointPosition.L1], new Vector2I(2, 0))
                    .AddIndicator("R1", fcsViewModel.Hardpoints[FcsApi_Data_HardpointPosition.R1], new Vector2I(3, 0))
                    .AddIndicator("R2", fcsViewModel.Hardpoints[FcsApi_Data_HardpointPosition.R2], new Vector2I(4, 0))
                    .AddIndicator("R3", fcsViewModel.Hardpoints[FcsApi_Data_HardpointPosition.R3], new Vector2I(5, 0));

                mfdLayoutBuilder
                    .AddSection()
                    .AddIndicator("NONE", fcsViewModel.Guidances[FcsApi_Data_GuidanceType.None], new Vector2I(0, 1))
                    .AddIndicator("ARH", fcsViewModel.Guidances[FcsApi_Data_GuidanceType.ActiveRadarHoming], new Vector2I(1, 1))
                    .AddIndicator("SARH", fcsViewModel.Guidances[FcsApi_Data_GuidanceType.SemiactiveRadarHoming], new Vector2I(2, 1))
                    .AddIndicator("LSBR", fcsViewModel.Guidances[FcsApi_Data_GuidanceType.BeamRiding], new Vector2I(3, 1))
                    .AddIndicator("GPS", fcsViewModel.Guidances[FcsApi_Data_GuidanceType.GlobalPositioning], new Vector2I(4, 1));

                mfdLayoutBuilder
                    .AddSection()
                    .AddIndicator("NONE", fcsViewModel.Detectors[FcsApi_Data_DetectorType.None], new Vector2I(0, 2))
                    .AddIndicator("RAD", fcsViewModel.Detectors[FcsApi_Data_DetectorType.Radar], new Vector2I(1, 2))
                    .AddIndicator("TUR", fcsViewModel.Detectors[FcsApi_Data_DetectorType.Turret], new Vector2I(2, 2))
                    .AddIndicator("LAS", fcsViewModel.Detectors[FcsApi_Data_DetectorType.LaserDesignator], new Vector2I(3, 2))
                    .AddIndicator("MEM", fcsViewModel.Detectors[FcsApi_Data_DetectorType.Memory], new Vector2I(4, 2));

                mfdLayoutBuilder
                    .AddButton(1, "Weapon", "fcs select-next-weapon")
                    .AddButton(2, "Guidance", "fcs select-next-guidance")
                    .AddButton(3, "Detector", "fcs select-next-detector");

                mfdLayoutBuilder.SetGridSize(new Vector2I(6, 3));

                var mfdLayout = mfdLayoutBuilder.Build();

                var anchor = new Framework_Component_Anchor(null, remoteControl);
                
                var hardpointL1 = new Fcs_Component_StaticHardpoint(null, mergeBlockL1, FcsApi_Data_HardpointPosition.L1);
                var hardpointL2 = new Fcs_Component_StaticHardpoint(null, mergeBlockL2, FcsApi_Data_HardpointPosition.L2);
                var hardpointL3 = new Fcs_Component_StaticHardpoint(null, mergeBlockL3, FcsApi_Data_HardpointPosition.L3);
                var hardpointR1 = new Fcs_Component_StaticHardpoint(null, mergeBlockR1, FcsApi_Data_HardpointPosition.R1);
                var hardpointR2 = new Fcs_Component_StaticHardpoint(null, mergeBlockR2, FcsApi_Data_HardpointPosition.R2);
                var hardpointR3 = new Fcs_Component_StaticHardpoint(null, mergeBlockR3, FcsApi_Data_HardpointPosition.R3);

                var radar = new Fcs_Component_Radar(null, camera, anchor, raycastBasedViewModel, 1800);
                var turret = new Fcs_Component_Turret(null, turretBlock);
                var laserDesignator = new Fcs_Component_LaserDesignator(null, camera, anchor, raycastBasedViewModel, 3000);
                var memory = new Fcs_Component_Memory("fcs-memory", laserDesignator, memoryViewModel);

                var fcs = new Fcs_Component_FireControlSystem(
                    "fcs",
                    anchor,
                    new FcsApi_Component_IHardpoint[] { hardpointL1, hardpointL2, hardpointL3, hardpointR1, hardpointR2, hardpointR3 },
                    new FcsApi_Component_IDetector[] { radar, turret, laserDesignator, memory },
                    fcsViewModel);

                new Ui_Component_InstrumentDisplay("id-1", idLayout, cockpit1, 0, 0.85f);
                new Ui_Component_InstrumentDisplay("id-2", idLayout, cockpit2, 0, 0.85f);
                new Ui_Component_MultifunctionDisplay("mfd-1", mfdLayout, cockpit1, 2);
                new Ui_Component_MultifunctionDisplay("mfd-2", mfdLayout, cockpit2, 2);
                new Ui_Component_WarningSystem("ws", new[] { fcsViewModel.WarningSystemState }, soundBlock);
            }
        }
    }
}
