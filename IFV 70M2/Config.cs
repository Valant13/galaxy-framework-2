﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System;
using VRage.Collections;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ObjectBuilders.Definitions;
using VRage.Game;
using VRage;
using VRageMath;

namespace IngameScript
{
    partial class Program
    {
        public class Config : Framework_Bootstrap_Data_IConfig
        {
            public string GetName()
            {
                return "IFV 70M2";
            }

            public UpdateFrequency GetUpdateFrequency()
            {
                return UpdateFrequency.Update1;
            }

            public void SetupDependencies()
            {
                Framework_DependencyConfig.Setup();
                TankSuspension_DependencyConfig.Setup();
                TerminalUi_DependencyConfig.Setup();
            }

            public void SetupComponents()
            {
                var cockpit1 = new Framework_Actor_Data_SearchCriteria()
                {
                    CustomId = "Cockpit1"
                };

                new TerminalUi_Component_Display(null, cockpit1, 1, 0.9F, 12);

                var cockpit2 = new Framework_Actor_Data_SearchCriteria()
                {
                    CustomId = "Cockpit2"
                };

                new TerminalUi_Component_Display(null, cockpit2, 1, 0.9F, 12);

                new TerminalUi_Component_Display(null);

                var gyroscope = new Framework_Actor_Data_SearchCriteria()
                {
                    Type = typeof(IMyGyro)
                };

                var suspensions = new[]
                {
                    new Framework_Actor_Data_SearchCriteria()
                    {
                        CustomClass = "Suspension",
                        CustomAttributes = new Association<string>()
                        {
                            { "Order", "1" }
                        }
                    },
                    new Framework_Actor_Data_SearchCriteria()
                    {
                        CustomClass = "Suspension",
                        CustomAttributes = new Association<string>()
                        {
                            { "Order", "2" }
                        }
                    },
                    new Framework_Actor_Data_SearchCriteria()
                    {
                        CustomClass = "Suspension",
                        CustomAttributes = new Association<string>()
                        {
                            { "Order", "3" }
                        }
                    },
                    new Framework_Actor_Data_SearchCriteria()
                    {
                        CustomClass = "Suspension",
                        CustomAttributes = new Association<string>()
                        {
                            { "Order", "4" }
                        }
                    },
                    new Framework_Actor_Data_SearchCriteria()
                    {
                        CustomClass = "Suspension",
                        CustomAttributes = new Association<string>()
                        {
                            { "Order", "5" }
                        }
                    },
                    new Framework_Actor_Data_SearchCriteria()
                    {
                        CustomClass = "Suspension",
                        CustomAttributes = new Association<string>()
                        {
                            { "Order", "6" }
                        }
                    }
                };

                new TankSuspension_Component_Suspension("suspension", cockpit1, gyroscope, suspensions, 0.004f, -0.46f, 0.24f, -0.32f);
            }
        }
    }
}
