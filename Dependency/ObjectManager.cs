﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System;
using VRage.Collections;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ObjectBuilders.Definitions;
using VRage.Game;
using VRage;
using VRageMath;

namespace IngameScript
{
    partial class Program
    {
        public static class ObjectManager
        {
            private static Dependency_Pool_ObjectService objectService;

            static ObjectManager()
            {
                objectService = new Dependency_Pool_ObjectService();
            }

            public static void Initialize(Dependency_Pool_Data_Config config)
            {
                objectService.Initialize(config);
            }

            public static T GetObject<T>()
            {
                return objectService.GetObject<T>();
            }
        }
    }
}
