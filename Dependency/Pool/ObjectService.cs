﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System;
using VRage.Collections;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ObjectBuilders.Definitions;
using VRage.Game;
using VRage;
using VRageMath;

namespace IngameScript
{
    partial class Program
    {
        public class Dependency_Pool_ObjectService
        {
            private bool isInitialized;
            private Dependency_Pool_Data_Config config;
            private Association<object> container = new Association<object>();

            public void Initialize(Dependency_Pool_Data_Config config)
            {
                if (isInitialized)
                {
                    throw new Exception("Already initialized");
                }

                this.config = config;

                isInitialized = true;
            }

            public T GetObject<T>()
            {
                return (T)GetObject(typeof(T));
            }

            private object GetObject(Type type)
            {
                // Not protected from cyclical dependencies
                if (config == null)
                {
                    throw new Exception("Config is not registered");
                }

                if (!container.ContainsKey(type.Name))
                {
                    if (!config.Objects.ContainsKey(type.Name))
                    {
                        throw new Exception($"Unknown object {type.Name}");
                    }

                    var configObject = config.Objects[type.Name];

                    var args = new Association<Association<object>>();
                    foreach (var configObjectArgument in configObject.Arguments.Values)
                    {
                        args[configObjectArgument.Name] = new Association<object>();
                        foreach (var configObjectArgumentValue in configObjectArgument.Values)
                        {
                            args[configObjectArgument.Name][configObjectArgumentValue.Key] = GetObject(configObjectArgumentValue.Value);
                        }
                    }

                    container[type.Name] = configObject.FactoryClosure(args);
                }

                return container[type.Name];
            }
        }
    }
}
