﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System;
using VRage.Collections;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ObjectBuilders.Definitions;
using VRage.Game;
using VRage;
using VRageMath;

namespace IngameScript
{
    partial class Program
    {
        public class Dependency_Setup_ObjectInstaller
        {
            private Dependency_Pool_Data_Object @object;
            private bool isWritable;

            public Dependency_Setup_ObjectInstaller(Dependency_Pool_Data_Object @object, bool isWritable)
            {
                this.@object = @object;
                this.isWritable = isWritable;
            }

            public Dependency_Setup_ObjectInstaller SetFactoryClosure(Dependency_Pool_Data_Config.FactoryClosure factoryClosure)
            {
                if (isWritable)
                {
                    @object.FactoryClosure = factoryClosure;
                }

                return this;
            }

            public Dependency_Setup_ObjectInstaller AddArgument(string name)
            {
                var argument = new Dependency_Pool_Data_ObjectArgument();
                argument.Name = name;

                if (isWritable)
                {
                    @object.Arguments[name] = argument;
                }

                return this;
            }

            public Dependency_Setup_ObjectInstaller AddArgumentValue(string argument, string key, Type value)
            {
                if (!@object.Arguments.ContainsKey(argument))
                {
                    AddArgument(argument);
                }

                if (isWritable)
                {
                    @object.Arguments[argument].Values[key] = value;
                }

                return this;
            }
        }
    }
}
