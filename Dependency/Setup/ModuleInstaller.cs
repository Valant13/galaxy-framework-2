﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System;
using VRage.Collections;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ObjectBuilders.Definitions;
using VRage.Game;
using VRage;
using VRageMath;

namespace IngameScript
{
    partial class Program
    {
        public class Dependency_Setup_ModuleInstaller
        {
            private Dependency_Pool_Data_Config config;
            private bool isWritable;

            public Dependency_Setup_ModuleInstaller(Dependency_Pool_Data_Config config, bool isWritable)
            {
                this.config = config;
                this.isWritable = isWritable;
            }

            public Dependency_Setup_ObjectInstaller DefineObject(Type type, string[] arguments, Dependency_Pool_Data_Config.FactoryClosure factoryClosure)
            {
                var @object = new Dependency_Pool_Data_Object();
                @object.Type = type;
                @object.FactoryClosure = factoryClosure;

                if (isWritable)
                {
                    config.Objects[type.Name] = @object;
                }

                var objectHandler = new Dependency_Setup_ObjectInstaller(@object, isWritable);
                foreach(var argument in arguments)
                {
                    objectHandler.AddArgument(argument);
                }

                return objectHandler;
            }

            public Dependency_Setup_ObjectInstaller DefineObject(Type type, string[] arguments)
            {
                return DefineObject(type, arguments, null);
            }

            public Dependency_Setup_ObjectInstaller DefineObject<T>() where T : new()
            {
                return DefineObject(typeof(T), new string[0], args => new T());
            }

            public Dependency_Setup_ObjectInstaller ModifyObject<T>()
            {
                var type = typeof(T);

                if (!config.Objects.ContainsKey(type.Name))
                {
                    throw new Exception($"Unknown object {type.Name}");
                }

                return new Dependency_Setup_ObjectInstaller(config.Objects[type.Name], isWritable);
            }
        }
    }
}
