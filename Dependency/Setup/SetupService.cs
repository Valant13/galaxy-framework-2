﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System;
using VRage.Collections;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ObjectBuilders.Definitions;
using VRage.Game;
using VRage;
using VRageMath;

namespace IngameScript
{
    partial class Program
    {
        public class Dependency_Setup_SetupService
        {
            private List<string> createdModuleNames = new List<string>();
            private Dependency_Pool_Data_Config config = new Dependency_Pool_Data_Config();

            public Dependency_Setup_ModuleInstaller CreateModule(string name)
            {
                var isWritable = !createdModuleNames.Contains(name);
                createdModuleNames.Add(name);

                return new Dependency_Setup_ModuleInstaller(config, isWritable);
            }

            public Dependency_Pool_Data_Config GetConfig()
            {
                return config;
            }
        }
    }
}
