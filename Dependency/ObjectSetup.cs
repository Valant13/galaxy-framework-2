﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System;
using VRage.Collections;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ObjectBuilders.Definitions;
using VRage.Game;
using VRage;
using VRageMath;

namespace IngameScript
{
    partial class Program
    {
        public static class ObjectSetup
        {
            private static Dependency_Setup_SetupService setupService;

            static ObjectSetup()
            {
                setupService = new Dependency_Setup_SetupService();
            }

            public static Dependency_Setup_ModuleInstaller CreateModule(string name)
            {
                return setupService.CreateModule(name);
            }

            public static Dependency_Pool_Data_Config GetConfig()
            {
                return setupService.GetConfig();
            }
        }
    }
}
