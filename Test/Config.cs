﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System;
using VRage.Collections;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ObjectBuilders.Definitions;
using VRage.Game;
using VRage;
using VRageMath;

namespace IngameScript
{
    partial class Program
    {
        public class Config : Framework_Bootstrap_Data_IConfig
        {
            public string GetName()
            {
                return "Sandbox";
            }

            public UpdateFrequency GetUpdateFrequency()
            {
                return UpdateFrequency.Update1 | UpdateFrequency.Update10 | UpdateFrequency.Update100;
            }

            public void SetupDependencies()
            {
                Framework_DependencyConfig.Setup();
                TerminalUi_DependencyConfig.Setup();
                FcsWeaponPack_DependencyConfig.Setup();
                Fcs_DependencyConfig.Setup();
                Ui_DependencyConfig.Setup();
            }

            public void SetupComponents()
            {
                
            }
        }
    }
}
