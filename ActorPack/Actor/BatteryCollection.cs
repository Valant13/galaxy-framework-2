﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System;
using VRage.Collections;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ObjectBuilders.Definitions;
using VRage.Game;
using VRage;
using VRageMath;

namespace IngameScript
{
    partial class Program
    {
        public class ActorPack_Actor_BatteryCollection : Framework_Actor_AbstractActorCollection<IMyBatteryBlock>
        {
            private enum State
            {
                Enabled,
                Disabled
            }

            private State state = State.Enabled;

            public ActorPack_Actor_BatteryCollection(Framework_Actor_Data_SearchCriteria searchCriteria, bool useHardStrategy = false) : base(searchCriteria, useHardStrategy)
            { }

            public void Enable()
            {
                foreach (var block in GetBlocks())
                {
                    block.Enabled = true;
                }

                state = State.Enabled;
            }

            public void Disable()
            {
                foreach (var block in GetBlocks())
                {
                    block.Enabled = false;
                }

                state = State.Disabled;
            }

            public bool IsEnabled()
            {
                return state == State.Enabled;
            }
        }
    }
}
