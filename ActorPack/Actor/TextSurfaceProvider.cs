﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System;
using VRage.Collections;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ObjectBuilders.Definitions;
using VRage.Game;
using VRage;
using VRageMath;

namespace IngameScript
{
    partial class Program
    {
        public class ActorPack_Actor_TextSurfaceProvider : Framework_Actor_AbstractSingleActor<IMyTerminalBlock>
        {
            public ActorPack_Actor_TextSurfaceProvider(Framework_Actor_Data_SearchCriteria searchCriteria, bool useHardStrategy = false) : base(searchCriteria, useHardStrategy)
            { }

            public IMyTextSurface GetSurface(int index = 0)
            {
                if (HasBlock())
                {
                    if (GetBlock() is IMyTextSurfaceProvider)
                    {
                        var textSurfaceProvider = (IMyTextSurfaceProvider)GetBlock();

                        if (index < textSurfaceProvider.SurfaceCount)
                        {
                            return textSurfaceProvider.GetSurface(index);
                        }
                    }
                    else if (GetBlock() is IMyTextPanel)
                    {
                        return (IMyTextSurface)GetBlock();
                    }
                }

                return null;
            }
        }
    }
}
