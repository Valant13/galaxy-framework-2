﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System;
using VRage.Collections;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ObjectBuilders.Definitions;
using VRage.Game;
using VRage;
using VRageMath;

namespace IngameScript
{
    partial class Program
    {
        public class ActorPack_Actor_PistonCollection : Framework_Actor_AbstractActorCollection<IMyPistonBase>
        {
            private enum State
            {
                Stopped,
                ReachingPosition
            }

            private const float positionPrecision = 0.1F;

            private State state;
            private float targetPosition;

            public ActorPack_Actor_PistonCollection(Framework_Actor_Data_SearchCriteria searchCriteria, bool useHardStrategy = false) : base(searchCriteria, useHardStrategy)
            { }

            public void ReachPosition(float position, float velocity)
            {
                targetPosition = position;
                state = State.ReachingPosition;

                if ((targetPosition - GetCurrentPosition()) < -positionPrecision)
                {
                    SetVelocity(-Math.Abs(velocity));
                }
                else if ((targetPosition - GetCurrentPosition()) > positionPrecision)
                {
                    SetVelocity(Math.Abs(velocity));
                }
            }

            public event EventHandler PositionReached;

            protected override void Update()
            {
                if (state == State.ReachingPosition)
                {
                    if ((targetPosition - GetCurrentPosition()) >= -positionPrecision && (targetPosition - GetCurrentPosition()) <= positionPrecision)
                    {
                        state = State.Stopped;
                        SetVelocity(0);

                        PositionReached?.Invoke(this, new EventArgs());
                    }
                }
            }

            private void SetVelocity(float velocity)
            {
                foreach (var block in GetBlocks())
                {
                    block.Velocity = velocity;
                }
            }

            private float GetCurrentPosition()
            {
                var position = 0F;

                foreach (var block in GetBlocks())
                {
                    position += block.CurrentPosition;
                }

                return position;
            }
        }
    }
}
