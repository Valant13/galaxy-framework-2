﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System;
using VRage.Collections;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ObjectBuilders.Definitions;
using VRage.Game;
using VRage;
using VRageMath;

namespace IngameScript
{
    partial class Program
    {
        public class ActorPack_Actor_GasTankCollection : Framework_Actor_AbstractActorCollection<IMyGasTank>
        {
            public event EventHandler EmptinessChanged;

            private const double EmptyFilledRatio = 0.05;

            private bool isEmpty;

            public ActorPack_Actor_GasTankCollection(Framework_Actor_Data_SearchCriteria searchCriteria, bool useHardStrategy = false) : base(searchCriteria, useHardStrategy)
            {
                isEmpty = IsEmptyImpl();
            }

            public void EnableStockpile()
            {
                foreach (var block in GetBlocks())
                {
                    block.Stockpile = true;
                }
            }

            public void DisableStockpile()
            {
                foreach (var block in GetBlocks())
                {
                    block.Stockpile = false;
                }
            }

            public bool IsEmpty()
            {
                return isEmpty;
            }

            protected override void Update()
            {
                var wasEmpty = isEmpty;
                isEmpty = IsEmptyImpl();

                if (isEmpty != wasEmpty)
                {
                    EmptinessChanged?.Invoke(this, new EventArgs());
                }
            }

            private bool IsEmptyImpl()
            {
                foreach (var block in GetBlocks())
                {
                    if (block.FilledRatio > EmptyFilledRatio)
                    {
                        return false;
                    }
                }

                return true;
            }
        }
    }
}
