﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System;
using VRage.Collections;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ObjectBuilders.Definitions;
using VRage.Game;
using VRage;
using VRageMath;

namespace IngameScript
{
    partial class Program
    {
        public class ActorPack_Actor_CameraCollection : Framework_Actor_AbstractActorCollection<IMyCameraBlock>
        {
            public event EventHandler EntityDetected;

            private ActorPack_Actor_CameraCollection_Context context = new ActorPack_Actor_CameraCollection_Context();
            private Framework_Utility_StateMachine<ActorPack_Actor_CameraCollection_AbstractState> stateMachine;

            public ActorPack_Actor_CameraCollection(Framework_Actor_Data_SearchCriteria searchCriteria, bool useHardStrategy = false) : base(searchCriteria, useHardStrategy)
            {
                context.EntityDetected = () => { EntityDetected?.Invoke(this, new EventArgs()); };
                context.GetBlocks = GetBlocks;
                context.HasBlocks = HasBlocks;

                stateMachine = new Framework_Utility_StateMachine<ActorPack_Actor_CameraCollection_AbstractState>(context)
                    .AddState<ActorPack_Actor_CameraCollection_DisabledState>()
                    .AddState<ActorPack_Actor_CameraCollection_PreparingState>()
                    .AddState<ActorPack_Actor_CameraCollection_RaycastingState>()
                    .AddState<ActorPack_Actor_CameraCollection_WaitingState>()
                    .SetInitialState<ActorPack_Actor_CameraCollection_DisabledState>();
            }

            public void EnableRaycast(double raycastDistance = 1000)
            {
                stateMachine.GetState().EnableRaycast(raycastDistance);
            }

            public void DisableRaycast()
            {
                stateMachine.GetState().DisableRaycast();
            }

            public bool IsRaycastEnabled()
            {
                return !stateMachine.IsState<ActorPack_Actor_CameraCollection_DisabledState>();
            }

            public void DelayRaycast(float raycastDelay)
            {
                stateMachine.GetState().DelayRaycast(raycastDelay);
            }

            public void SetRaycastPosition(Vector3D raycastPosition)
            {
                context.RaycastPosition = raycastPosition;
            }

            public void ResetRaycastPosition()
            {
                context.RaycastPosition = Vector3D.Zero;
            }

            public float GetRaycastIntervalTime()
            {
                return context.RaycastIntervalTime;
            }

            public double GetRaycastDistance()
            {
                return context.RaycastDistance;
            }

            public MyDetectedEntityInfo GetLastEntityInfo()
            {
                return context.LastEntityInfo;
            }

            protected override void UpdateBlocks()
            {
                base.UpdateBlocks();

                context.RaycastUpcomingTime = CalculateRaycastUpcomingTime();
            }

            /// <summary>
            /// Calculate time of raycast distance upcoming in seconds for one meter
            /// </summary>
            private float CalculateRaycastUpcomingTime()
            {
                if (HasBlocks())
                {
                    var camera = GetBlocks().First();

                    return camera.TimeUntilScan(camera.AvailableScanRange + 1000) / 1000f / 1000f;
                }

                return 0;
            }
        }
    }
}
