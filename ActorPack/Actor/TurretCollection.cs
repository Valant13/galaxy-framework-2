﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VRage;
using VRage.Collections;
using VRage.Game;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ObjectBuilders.Definitions;
using VRageMath;

namespace IngameScript
{
    partial class Program
    {
        public class ActorPack_Actor_TurretCollection : Framework_Actor_AbstractActorCollection<IMyLargeTurretBase>
        {
            public event EventHandler EntityDetected;

            private MyDetectedEntityInfo lastEntityInfo;

            public ActorPack_Actor_TurretCollection(Framework_Actor_Data_SearchCriteria searchCriteria, bool useHardStrategy = false) : base(searchCriteria, useHardStrategy)
            { }

            public MyDetectedEntityInfo GetLastEntityInfo()
            {
                return lastEntityInfo;
            }

            protected override void Update()
            {
                foreach (var block in GetBlocks())
                {
                    if (block.HasTarget)
                    {
                        lastEntityInfo = block.GetTargetedEntity();

                        EntityDetected?.Invoke(this, new EventArgs());

                        break;
                    }
                }
            }
        }
    }
}
