﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System;
using VRage.Collections;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ObjectBuilders.Definitions;
using VRage.Game;
using VRage;
using VRageMath;

namespace IngameScript
{
    partial class Program
    {
        public class ActorPack_Actor_SoundBlock : Framework_Actor_AbstractSingleActor<IMySoundBlock>
        {
            private enum State
            {
                SoundPlaying,
                SoundStopped
            }

            private State state = State.SoundStopped;
            private string intervalId;
            private Framework_Utility_Timer timer;

            public ActorPack_Actor_SoundBlock(Framework_Actor_Data_SearchCriteria searchCriteria, bool useHardStrategy = false) : base(searchCriteria, useHardStrategy)
            {
                timer = ObjectManager.GetObject<Framework_Utility_Timer>();
            }

            public void PlaySound(float loopPeriod = 1800)
            {
                if (state == State.SoundStopped)
                {
                    intervalId = timer.SetInterval(loopPeriod, () => {
                        if (HasBlock())
                        {
                            GetBlock().Play();
                        }
                    });

                    if (HasBlock())
                    {
                        var block = GetBlock();

                        block.LoopPeriod = loopPeriod;
                        block.Play();
                    }

                    state = State.SoundPlaying;
                }
            }

            public void StopSound()
            {
                if (state == State.SoundPlaying)
                {
                    if (HasBlock())
                    {
                        GetBlock().Stop();
                    }

                    timer.ClearInterval(intervalId);

                    state = State.SoundStopped;
                }
            }

            public bool IsSoundPlaying()
            {
                return state == State.SoundPlaying;
            }

            public void SetSound(string sound)
            {
                if (HasBlock())
                {
                    GetBlock().SelectedSound = sound;
                }
            }
        }
    }
}
