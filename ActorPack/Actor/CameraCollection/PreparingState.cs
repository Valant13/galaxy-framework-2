﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VRage;
using VRage.Collections;
using VRage.Game;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ObjectBuilders.Definitions;
using VRageMath;

namespace IngameScript
{
    partial class Program
    {
        public class ActorPack_Actor_CameraCollection_PreparingState : ActorPack_Actor_CameraCollection_AbstractState
        {
            private string timeoutId;

            public override void Enter()
            {
                timeoutId = Timer.SetTimeout(Context.RaycastTime, () => {
                    StateMachine.ChangeState<ActorPack_Actor_CameraCollection_RaycastingState>();
                });
            }

            public override void DisableRaycast()
            {
                Timer.ClearTimeout(timeoutId);
                StateMachine.ChangeState<ActorPack_Actor_CameraCollection_DisabledState>();
            }
        }
    }
}
