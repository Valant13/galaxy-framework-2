﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VRage;
using VRage.Collections;
using VRage.Game;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ObjectBuilders.Definitions;
using VRageMath;

namespace IngameScript
{
    partial class Program
    {
        public class ActorPack_Actor_CameraCollection_RaycastingState : ActorPack_Actor_CameraCollection_AbstractState
        {
            private string intervalId;
            
            public override void Enter()
            {
                intervalId = Timer.SetInterval(Context.RaycastIntervalTime, RaycastByNextCamera);
            }

            public override void Exit()
            {
                Timer.ClearInterval(intervalId);
            }

            private void RaycastByNextCamera()
            {
                var camera = GetBlocks()[Context.CurrentCameraNumber];
                MyDetectedEntityInfo entityInfo;

                if (!Context.RaycastPosition.IsZero())
                {
                    entityInfo = camera.Raycast(Context.RaycastPosition);
                }
                else
                {
                    entityInfo = camera.Raycast(Context.RaycastDistance);
                }

                Context.CurrentCameraNumber++;
                if (Context.CurrentCameraNumber >= GetBlocks().Count)
                {
                    Context.CurrentCameraNumber = 0;
                }

                Context.LastEntityInfo = entityInfo;
                Context.EntityDetected();
            }

            public override void DelayRaycast(float raycastDelayTime)
            {
                Context.RaycastDelayTime = raycastDelayTime;
                StateMachine.ChangeState<ActorPack_Actor_CameraCollection_WaitingState>();
            }

            public override void DisableRaycast()
            {
                StateMachine.ChangeState<ActorPack_Actor_CameraCollection_DisabledState>();
            }
        }
    }
}
