﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VRage;
using VRage.Collections;
using VRage.Game;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ObjectBuilders.Definitions;
using VRageMath;

namespace IngameScript
{
    partial class Program
    {
        public class ActorPack_Actor_CameraCollection_DisabledState : ActorPack_Actor_CameraCollection_AbstractState
        {
            public override void Enter()
            {
                foreach (var camera in GetBlocks())
                {
                    camera.EnableRaycast = false;
                }

                Context.RaycastDistance = 0;
                Context.RaycastPosition = Vector3D.Zero;
                Context.RaycastTime = 0;
                Context.RaycastIntervalTime = 0;
            }

            public override void EnableRaycast(double raycastDistance)
            {
                if (!HasBlocks())
                {
                    return;
                }

                foreach (var camera in GetBlocks())
                {
                    camera.EnableRaycast = true;
                }

                Context.RaycastDistance = raycastDistance;
                Context.RaycastTime = Context.RaycastUpcomingTime * (float)Context.RaycastDistance;
                Context.RaycastIntervalTime = Context.RaycastTime / GetBlocks().Count;

                StateMachine.ChangeState<ActorPack_Actor_CameraCollection_PreparingState>();
            }
        }
    }
}
