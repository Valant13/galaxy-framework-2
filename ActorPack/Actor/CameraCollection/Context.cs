﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VRage;
using VRage.Collections;
using VRage.Game;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ObjectBuilders.Definitions;
using VRageMath;

namespace IngameScript
{
    partial class Program
    {
        public class ActorPack_Actor_CameraCollection_Context
        {
            public MyDetectedEntityInfo LastEntityInfo { get; set; }
            public double RaycastDistance { get; set; }
            public Vector3D RaycastPosition { get; set; }
            public float RaycastIntervalTime { get; set; }
            public float RaycastDelayTime { get; set; }
            public float RaycastTime { get; set; }
            public float RaycastUpcomingTime { get; set; }
            public int CurrentCameraNumber { get; set; }
            public Action EntityDetected { get; set; }
            public Func<bool> HasBlocks { get; set; }
            public Func<List<IMyCameraBlock>> GetBlocks { get; set; }
        }
    }
}
