﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VRage;
using VRage.Collections;
using VRage.Game;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ObjectBuilders.Definitions;
using VRageMath;

namespace IngameScript
{
    partial class Program
    {
        public abstract class ActorPack_Actor_CameraCollection_AbstractState : Framework_Utility_IState
        {
            protected ActorPack_Actor_CameraCollection_Context Context { get; set; }
            protected Framework_Utility_StateMachine<ActorPack_Actor_CameraCollection_AbstractState> StateMachine { get; set; }
            protected Framework_Utility_Timer Timer { get; set; }

            public virtual void Enter()
            { }

            public virtual void Exit()
            { }

            public void SetStateMachine(object stateMachine)
            {
                StateMachine = (Framework_Utility_StateMachine<ActorPack_Actor_CameraCollection_AbstractState>)stateMachine;
            }

            public void SetContext(object context)
            {
                Context = (ActorPack_Actor_CameraCollection_Context)context;
                Timer = ObjectManager.GetObject<Framework_Utility_Timer>();
            }

            public virtual void EnableRaycast(double raycastDistance)
            { }

            public virtual void DisableRaycast()
            { }

            public virtual void DelayRaycast(float raycastDelayTime)
            { }

            protected bool HasBlocks()
            {
                return Context.HasBlocks();
            }

            protected List<IMyCameraBlock> GetBlocks()
            {
                return Context.GetBlocks();
            }
        }
    }
}
