﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System;
using VRage.Collections;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ObjectBuilders.Definitions;
using VRage.Game;
using VRage;
using VRageMath;

namespace IngameScript
{
    partial class Program
    {
        public class ActorPack_Actor_WelderCollection : Framework_Actor_AbstractActorCollection<IMyShipWelder>
        {
            private enum State
            {
                Disabled,
                Enabled,
                WeldingBlock
            }

            private State state = State.Disabled;
            private string blockWeldingTimeoutId;
            private Framework_Utility_Timer timer;
            private Framework_Utility_WorldService worldService;

            public ActorPack_Actor_WelderCollection(Framework_Actor_Data_SearchCriteria searchCriteria, bool useHardStrategy = false) : base(searchCriteria, useHardStrategy)
            {
                timer = ObjectManager.GetObject<Framework_Utility_Timer>();
                worldService = ObjectManager.GetObject<Framework_Utility_WorldService>();
            }

            public void Enable()
            {
                EnableImpl();

                state = State.Enabled;
            }

            public void Disable()
            {
                DisableImpl();

                state = State.Disabled;
            }

            public bool IsEnabled()
            {
                return state == State.Enabled;
            }

            public void WeldBlock(float delay)
            {
                if (state == State.WeldingBlock)
                {
                    timer.ClearTimeout(blockWeldingTimeoutId);
                }

                EnableImpl();
                state = State.WeldingBlock;

                blockWeldingTimeoutId = timer.SetTimeout(delay / worldService.WeldingSpeed, () => {
                    DisableImpl();
                    state = State.Disabled;

                    BlockWelded?.Invoke(this, new EventArgs());
                });
            }

            public event EventHandler BlockWelded;

            private void EnableImpl()
            {
                foreach (var block in GetBlocks())
                {
                    block.Enabled = true;
                }
            }

            private void DisableImpl()
            {
                foreach (var block in GetBlocks())
                {
                    block.Enabled = false;
                }
            }
        }
    }
}
