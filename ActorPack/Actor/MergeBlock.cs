﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System;
using VRage.Collections;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ObjectBuilders.Definitions;
using VRage.Game;
using VRage;
using VRageMath;

namespace IngameScript
{
    partial class Program
    {
        public class ActorPack_Actor_MergeBlock : Framework_Actor_AbstractSingleActor<IMyShipMergeBlock>
        {
            private enum State
            {
                Enabled,
                Disabled
            }

            private State state = State.Enabled;

            public ActorPack_Actor_MergeBlock(Framework_Actor_Data_SearchCriteria searchCriteria, bool useHardStrategy = false) : base(searchCriteria, useHardStrategy)
            { }

            public void Enable()
            {
                if (HasBlock())
                {
                    GetBlock().Enabled = true;
                }

                state = State.Enabled;
            }

            public void Disable()
            {
                if (HasBlock())
                {
                    GetBlock().Enabled = false;
                }

                state = State.Disabled;
            }

            public bool IsEnabled()
            {
                return state == State.Enabled;
            }

            public IMyShipMergeBlock GetConnectedMergeBlock()
            {
                if (HasBlock())
                {
                    var block = GetBlock();

                    var leftDirection = Base6Directions.GetIntVector(block.Orientation.Left);

                    var connectedBlock = block.CubeGrid.GetCubeBlock(block.Position - leftDirection);

                    if (connectedBlock != null)
                    {
                        if (connectedBlock.FatBlock is IMyShipMergeBlock)
                        {
                            return (IMyShipMergeBlock)connectedBlock.FatBlock;
                        }
                    }
                }

                return null;
            }
        }
    }
}
