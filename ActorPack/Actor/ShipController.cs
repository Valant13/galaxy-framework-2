﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System;
using VRage.Collections;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ObjectBuilders.Definitions;
using VRage.Game;
using VRage;
using VRageMath;

namespace IngameScript
{
    partial class Program
    {
        public class ActorPack_Actor_ShipController : Framework_Actor_AbstractSingleActor<IMyShipController>
        {
            public event EventHandler RotationIndicatorChanged;

            private Vector2 rotationIndicator;

            public ActorPack_Actor_ShipController(Framework_Actor_Data_SearchCriteria searchCriteria, bool useHardStrategy = false) : base(searchCriteria, useHardStrategy)
            { }

            public Vector2 GetRotationIndicator()
            {
                return rotationIndicator;
            }

            public Vector3D GetNaturalGravity()
            {
                if (HasBlock())
                {
                    return GetBlock().GetNaturalGravity();
                }

                return Vector3D.Zero;
            }

            public Vector3D GetNaturalGravityDirection()
            {
                var naturalGravity = GetNaturalGravity();

                if (naturalGravity.IsZero())
                {
                    return Vector3D.Zero;
                }
                else
                {
                    return Vector3D.Normalize(naturalGravity);
                }
            }

            public void EnableDampenersOverride()
            {
                if (HasBlock())
                {
                    GetBlock().DampenersOverride = true;
                }
            }

            public void DisableDampenersOverride()
            {
                if (HasBlock())
                {
                    GetBlock().DampenersOverride = false;
                }
            }

            protected override void Update()
            {
                var oldRotationIndicator = rotationIndicator;
                rotationIndicator = GetRotationIndicatorImpl();

                if (rotationIndicator != oldRotationIndicator)
                {
                    RotationIndicatorChanged?.Invoke(this, new EventArgs());
                }
            }

            private Vector2 GetRotationIndicatorImpl()
            {
                if (HasBlock())
                {
                    return GetBlock().RotationIndicator;
                }

                return Vector2.Zero;
            }
        }
    }
}
