﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System;
using VRage.Collections;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ObjectBuilders.Definitions;
using VRage.Game;
using VRage;
using VRageMath;

namespace IngameScript
{
    partial class Program
    {
        public class ActorPack_Actor_Projector : Framework_Actor_AbstractSingleActor<IMyProjector>
        {
            private enum State
            {
                Enabled,
                Disabled
            }

            public const string Section = "Projection";
            public const string OffsetXKey = "OffsetX";
            public const string OffsetYKey = "OffsetY";
            public const string OffsetZKey = "OffsetZ";
            public const string RotationPitchKey = "RotationPitch";
            public const string RotationYawKey = "RotationYaw";
            public const string RotationRollKey = "RotationRoll";

            private State state = State.Disabled;
            
            public ActorPack_Actor_Projector(Framework_Actor_Data_SearchCriteria searchCriteria, bool useHardStrategy = false) : base(searchCriteria, useHardStrategy)
            { }

            public void Enable()
            {
                if (HasBlock())
                {
                    GetBlock().Enabled = true;
                }

                state = State.Enabled;
            }

            public void Disable()
            {
                if (HasBlock())
                {
                    GetBlock().Enabled = false;
                }

                state = State.Disabled;
            }

            public bool IsEnabled()
            {
                return state == State.Enabled;
            }

            protected override void UpdateBlocks()
            {
                base.UpdateBlocks();

                if (HasBlock())
                {
                    // Method is called before own constructor
                    var iniService = ObjectManager.GetObject<Framework_Actor_IniService>();

                    var block = GetBlock();
                    var ini = iniService.GetBlockCustomDataIni(block);

                    if (ini.ContainsSection(Section))
                    {
                        block.ProjectionOffset = new Vector3I()
                        {
                            X = iniService.GetInt(ini, Section, OffsetXKey),
                            Y = iniService.GetInt(ini, Section, OffsetYKey),
                            Z = iniService.GetInt(ini, Section, OffsetZKey)
                        };
                        
                        block.ProjectionRotation = new Vector3I()
                        {
                            X = iniService.GetInt(ini, Section, RotationPitchKey),
                            Y = iniService.GetInt(ini, Section, RotationYawKey),
                            Z = iniService.GetInt(ini, Section, RotationRollKey)
                        };

                        block.UpdateOffsetAndRotation();
                    }
                }
            }
        }
    }
}
