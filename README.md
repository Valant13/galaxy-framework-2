# GalaxyFramework2

The ultimate toolkit for Space Engineers scripting

## Features
  - Modules
  - Components
  - Actors
  - Improved block search
  - Improved terminal
  - Dependency injection
  - Utilities (timer, hash generator, PID controller, etc.)

## Coding standards

### Class members:
  - Delegates and events
  - Constants
  - Fields (DI values in the bottom)
  - Properties (one blank line between each)
  - Constructors (one blank line between each)
  - Methods (one blank line between each)

### Access modifiers:
  - Public
  - Protected
  - Private

## License
[MIT](https://choosealicense.com/licenses/mit/)
