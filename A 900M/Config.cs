﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System;
using VRage.Collections;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ObjectBuilders.Definitions;
using VRage.Game;
using VRage;
using VRageMath;

namespace IngameScript
{
    partial class Program
    {
        public class Config : Framework_Bootstrap_Data_IConfig
        {
            public string GetName()
            {
                return "A 900M";
            }

            public UpdateFrequency GetUpdateFrequency()
            {
                return UpdateFrequency.Update1 | UpdateFrequency.Update10 | UpdateFrequency.Update100;
            }

            public void SetupDependencies()
            {
                Framework_DependencyConfig.Setup();
                TerminalUi_DependencyConfig.Setup();
                FcsWeaponPack_DependencyConfig.Setup();
                Fcs_DependencyConfig.Setup();
                Ui_DependencyConfig.Setup();
            }

            public void SetupComponents()
            {
                var searchCriteriaBuilder = new Framework_Actor_SearchCriteriaBuilder();
                var idLayoutBuilder = new UiApi_IdLayoutBuilder();
                var mfdLayoutBuilder = new UiApi_MfdLayoutBuilder();

                var cockpit1 = searchCriteriaBuilder
                    .SetCustomId("Cockpit1")
                    .Build();

                var cockpit2 = searchCriteriaBuilder
                    .SetCustomId("Cockpit2")
                    .Build();

                new TerminalUi_Component_Display(null, cockpit1, 4, 1.1f, 12);
                new TerminalUi_Component_Display(null, cockpit2, 4, 1.1f, 12);

                new TerminalUi_Component_Display(null);

                var mergeBlockM1 = searchCriteriaBuilder
                    .SetCustomId("MergeBlockM1")
                    .Build();

                var welder = searchCriteriaBuilder
                    .SetType<IMyShipWelder>()
                    .Build();

                var connectorM1 = searchCriteriaBuilder
                    .SetCustomId("ConnectorM1")
                    .Build();

                var camera = searchCriteriaBuilder
                    .SetCustomClass("Camera")
                    .Build();

                var soundBlock = searchCriteriaBuilder
                    .SetCustomId("SoundBlock")
                    .Build();

                var remoteControl = searchCriteriaBuilder
                    .SetCustomId("RemoteControl")
                    .Build();

                var turretBlock = searchCriteriaBuilder
                    .SetCustomClass("Turret")
                    .Build();

                var fcsViewModel = new Fcs_ViewModel_FireControlSystem();
                var memoryViewModel = new Fcs_ViewModel_Memory();
                var raycastBasedViewModel = new Fcs_ViewModel_RaycastBased();
                var dynamicHardpointViewModel = new Fcs_ViewModel_DynamicHardpoint();

                idLayoutBuilder
                    .AddSection()
                    .AddRecord("Status", fcsViewModel.State)
                    .AddRecord("Target Distance", fcsViewModel.TargetDistance)
                    .AddRecord("Target Speed", fcsViewModel.TargetSpeed)
                    .AddRecord("Weapon Title", fcsViewModel.WeaponTitle)
                    .AddRecord("Hardpoint Ready", fcsViewModel.IsHardpointReady)
                    .AddRecord("Guided Weapons", fcsViewModel.GuidedWeaponsCount)
                    .AddRecord("Time to Hit", fcsViewModel.TimeToHit);

                idLayoutBuilder
                    .AddSection()
                    .AddRecord("Saved Target", memoryViewModel.TargetPosition);

                idLayoutBuilder
                    .AddSection()
                    .AddRecord("Raycast Interval", raycastBasedViewModel.IntervalTime)
                    .AddRecord("Raycast Distance", raycastBasedViewModel.Distance);

                var idLayout = idLayoutBuilder.Build();

                mfdLayoutBuilder
                    .AddSection()
                    .AddIndicator("M1", fcsViewModel.Hardpoints[FcsApi_Data_HardpointPosition.M1], new Vector2I(0, 0));

                mfdLayoutBuilder
                    .AddSection()
                    .AddIndicator("1", dynamicHardpointViewModel.Weapons[0], new Vector2I(2, 0))
                    .AddIndicator("2", dynamicHardpointViewModel.Weapons[1], new Vector2I(3, 0))
                    .AddIndicator("3", dynamicHardpointViewModel.Weapons[2], new Vector2I(4, 0))
                    .AddIndicator("4", dynamicHardpointViewModel.Weapons[3], new Vector2I(5, 0));

                mfdLayoutBuilder
                    .AddSection()
                    .AddIndicator("NONE", fcsViewModel.Guidances[FcsApi_Data_GuidanceType.None], new Vector2I(0, 1))
                    .AddIndicator("ARH", fcsViewModel.Guidances[FcsApi_Data_GuidanceType.ActiveRadarHoming], new Vector2I(1, 1))
                    .AddIndicator("SARH", fcsViewModel.Guidances[FcsApi_Data_GuidanceType.SemiactiveRadarHoming], new Vector2I(2, 1))
                    .AddIndicator("LSBR", fcsViewModel.Guidances[FcsApi_Data_GuidanceType.BeamRiding], new Vector2I(3, 1))
                    .AddIndicator("GPS", fcsViewModel.Guidances[FcsApi_Data_GuidanceType.GlobalPositioning], new Vector2I(4, 1));

                mfdLayoutBuilder
                    .AddSection()
                    .AddIndicator("NONE", fcsViewModel.Detectors[FcsApi_Data_DetectorType.None], new Vector2I(0, 2))
                    .AddIndicator("RAD", fcsViewModel.Detectors[FcsApi_Data_DetectorType.Radar], new Vector2I(1, 2))
                    .AddIndicator("TUR", fcsViewModel.Detectors[FcsApi_Data_DetectorType.Turret], new Vector2I(2, 2))
                    .AddIndicator("LAS", fcsViewModel.Detectors[FcsApi_Data_DetectorType.LaserDesignator], new Vector2I(3, 2))
                    .AddIndicator("MEM", fcsViewModel.Detectors[FcsApi_Data_DetectorType.Memory], new Vector2I(4, 2));

                mfdLayoutBuilder
                    .AddButton(1, "Weapon", "fcs select-next-weapon")
                    .AddButton(2, "Guidance", "fcs select-next-guidance")
                    .AddButton(3, "Detector", "fcs select-next-detector")
                    .AddButton(4, "M1", "hardpoint-m1 select-next-weapon");

                mfdLayoutBuilder.SetGridSize(new Vector2I(6, 3));

                var mfdLayout = mfdLayoutBuilder.Build();

                var anchor = new Framework_Component_Anchor(null, remoteControl);
                
                var hardpointM1 = new Fcs_Component_DynamicHardpoint(
                    "hardpoint-m1",
                    mergeBlockM1,
                    welder,
                    connectorM1,
                    FcsApi_Data_HardpointPosition.M1,
                    new[]
                    {
                        new Fcs_Data_WeaponAssemblyInfo(30, 0, searchCriteriaBuilder.SetCustomId("ProjectorM11").Build()),
                        new Fcs_Data_WeaponAssemblyInfo(40, 0, searchCriteriaBuilder.SetCustomId("ProjectorM12").Build()),
                        new Fcs_Data_WeaponAssemblyInfo(50, 4, searchCriteriaBuilder.SetCustomId("ProjectorM13").Build()),
                        new Fcs_Data_WeaponAssemblyInfo(50, 4, searchCriteriaBuilder.SetCustomId("ProjectorM14").Build())
                    },
                    dynamicHardpointViewModel);

                var radar = new Fcs_Component_Radar(null, camera, anchor, raycastBasedViewModel, 3000);
                var turret = new Fcs_Component_Turret(null, turretBlock);
                var laserDesignator = new Fcs_Component_LaserDesignator(null, camera, anchor, raycastBasedViewModel, 5000);
                var memory = new Fcs_Component_Memory("fcs-memory", laserDesignator, memoryViewModel);

                var fcs = new Fcs_Component_FireControlSystem(
                    "fcs",
                    anchor,
                    new FcsApi_Component_IHardpoint[] { hardpointM1 },
                    new FcsApi_Component_IDetector[] { radar, turret, laserDesignator, memory },
                    fcsViewModel);

                new Ui_Component_InstrumentDisplay("id-1", idLayout, cockpit1, 0, 0.85f);
                new Ui_Component_InstrumentDisplay("id-2", idLayout, cockpit2, 0, 0.85f);
                new Ui_Component_MultifunctionDisplay("mfd-1", mfdLayout, cockpit1, 1);
                new Ui_Component_MultifunctionDisplay("mfd-2", mfdLayout, cockpit2, 1);
                new Ui_Component_WarningSystem("ws", new[] { fcsViewModel.WarningSystemState }, soundBlock);
            }
        }
    }
}
