﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System;
using VRage.Collections;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ObjectBuilders.Definitions;
using VRage.Game;
using VRage;
using VRageMath;

namespace IngameScript
{
    partial class Program
    {
        public class Config : Framework_Bootstrap_Data_IConfig
        {
            public string GetName()
            {
                return "A 900";
            }

            public UpdateFrequency GetUpdateFrequency()
            {
                return UpdateFrequency.Update1;
            }

            public void SetupDependencies()
            {
                Framework_DependencyConfig.Setup();
                BombDropper_DependencyConfig.Setup();
                TerminalUi_DependencyConfig.Setup();
            }

            public void SetupComponents()
            {
                var projector = new Framework_Actor_Data_SearchCriteria()
                {
                    CustomClass = "Projector"
                };

                var mergeBlock = new Framework_Actor_Data_SearchCriteria()
                {
                    CustomClass = "MergeBlock"
                };

                var welder = new Framework_Actor_Data_SearchCriteria()
                {
                    Type = typeof(IMyShipWelder)
                };

                new BombDropper_Component_EasyBombDropper("bomb-dropper", projector, mergeBlock, welder, 28);

                var cockpit1 = new Framework_Actor_Data_SearchCriteria()
                {
                    CustomId = "Cockpit1"
                };

                new TerminalUi_Component_Display(null, cockpit1, 1, 0.9F, 12);

                var cockpit2 = new Framework_Actor_Data_SearchCriteria()
                {
                    CustomId = "Cockpit2"
                };

                new TerminalUi_Component_Display(null, cockpit2, 1, 0.9F, 12);

                new TerminalUi_Component_Display(null);
            }
        }
    }
}
