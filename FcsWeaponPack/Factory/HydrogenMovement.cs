﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VRage;
using VRage.Collections;
using VRage.Game;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ObjectBuilders.Definitions;
using VRageMath;

namespace IngameScript
{
    partial class Program
    {
        public class FcsWeaponPack_Factory_HydrogenMovement : FcsWeaponPack_Factory_DefaultMovement
        {
            public override FcsApi_Component_IWeaponMovement CreateMovement(Framework_Component_Anchor anchor, BoundingBoxI boundingBox, float decoupleTime)
            {
                if (anchor == null)
                {
                    throw new Exception("Anchor is missing");
                }

                return new FcsWeaponPack_Component_HydrogenMovement(
                    null,
                    GetForwardThruster(boundingBox),
                    GetBottomThruster(boundingBox),
                    GetThruster(boundingBox),
                    GetGyroscope(boundingBox),
                    GetBattery(boundingBox),
                    GetHydrogenTank(boundingBox),
                    GetRemoteControl(boundingBox),
                    anchor,
                    decoupleTime);
            }

            protected virtual Framework_Actor_Data_SearchCriteria GetHydrogenTank(BoundingBoxI boundingBox)
            {
                return new Framework_Actor_SearchCriteriaBuilder()
                    .SetCustomClass("WeaponHydrogenTank")
                    .SetBoundingBox(boundingBox)
                    .Build();
            }
        }
    }
}
