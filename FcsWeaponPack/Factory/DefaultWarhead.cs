﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VRage;
using VRage.Collections;
using VRage.Game;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ObjectBuilders.Definitions;
using VRageMath;

namespace IngameScript
{
    partial class Program
    {
        public class FcsWeaponPack_Factory_DefaultWarhead : FcsApi_IWeaponWarheadFactory
        {
            public FcsApi_Component_IWeaponWarhead CreateWarhead(Framework_Component_Anchor anchor, BoundingBoxI boundingBox)
            {
                if (anchor == null)
                {
                    throw new Exception("Anchor is missing");
                }

                var searchCriteriaBuilder = new Framework_Actor_SearchCriteriaBuilder();

                var warhead = searchCriteriaBuilder
                    .SetCustomClass("WeaponWarhead")
                    .SetBoundingBox(boundingBox)
                    .Build();

                return new FcsWeaponPack_Component_DefaultWarhead(null, warhead, anchor);
            }
        }
    }
}
