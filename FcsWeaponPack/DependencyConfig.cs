﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System;
using VRage.Collections;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ObjectBuilders.Definitions;
using VRage.Game;
using VRage;
using VRageMath;

namespace IngameScript
{
    partial class Program
    {
        public static class FcsWeaponPack_DependencyConfig
        {
            public static void Setup()
            {
                Framework_DependencyConfig.Setup();
                ActorPack_DependencyConfig.Setup();
                FcsApi_DependencyConfig.Setup();

                var module = ObjectSetup.CreateModule("FcsWeaponPack");

                module.DefineObject<FcsWeaponPack_Factory_DefaultMovement>();
                module.DefineObject<FcsWeaponPack_Factory_FlippedMovement>();
                module.DefineObject<FcsWeaponPack_Factory_HydrogenMovement>();
                module.DefineObject<FcsWeaponPack_Factory_DefaultRadar>();
                module.DefineObject<FcsWeaponPack_Factory_BombWarhead>();
                module.DefineObject<FcsWeaponPack_Factory_DefaultWarhead>();

                module.ModifyObject<FcsApi_IWeaponFactory>()
                    .AddArgumentValue("Movements", "Default", typeof(FcsWeaponPack_Factory_DefaultMovement))
                    .AddArgumentValue("Movements", "Flipped", typeof(FcsWeaponPack_Factory_FlippedMovement))
                    .AddArgumentValue("Movements", "Hydrogen", typeof(FcsWeaponPack_Factory_HydrogenMovement))
                    .AddArgumentValue("Radars", "Default", typeof(FcsWeaponPack_Factory_DefaultRadar))
                    .AddArgumentValue("Warheads", "Bomb", typeof(FcsWeaponPack_Factory_BombWarhead))
                    .AddArgumentValue("Warheads", "Default", typeof(FcsWeaponPack_Factory_DefaultWarhead));
            }
        }
    }
}
