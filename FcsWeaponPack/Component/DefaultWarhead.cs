﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VRage;
using VRage.Collections;
using VRage.Game;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ObjectBuilders.Definitions;
using VRageMath;

namespace IngameScript
{
    partial class Program
    {
        public class FcsWeaponPack_Component_DefaultWarhead : Framework_Component_AbstractComponent, FcsApi_Component_IWeaponWarhead
        {
            public event EventHandler FunctionalityChanged;
            public event EventHandler Detonated;

            private enum State
            {
                Disabled,
                Armed,
                Detonated
            }

            private const double detonationAcceleration = 250;

            private State state = State.Disabled;
            private ActorPack_Actor_WarheadCollection warhead;
            private Framework_Component_Anchor anchor;
            private Framework_Actor_MutationObserver warheadObserver;

            public FcsWeaponPack_Component_DefaultWarhead(
                string id,
                Framework_Actor_Data_SearchCriteria warheadSearchCriteria,
                Framework_Component_Anchor anchor) : base(id)
            {
                warhead = new ActorPack_Actor_WarheadCollection(warheadSearchCriteria, true);
                this.anchor = anchor;

                warheadObserver = new Framework_Actor_MutationObserver(warheadSearchCriteria, true);

                warheadObserver.FunctionalityChanged += Child_FunctionalityChanged;
                anchor.FunctionalityChanged += Child_FunctionalityChanged;
                anchor.AccelerationChanged += Anchor_AccelerationChanged;

                SetDescription("FcsWeaponPack_Component_DefaultWarhead");
            }

            private void Anchor_AccelerationChanged(object sender, EventArgs eventArgs)
            {
                if (state == State.Armed && anchor.IsFunctional())
                {
                    var acceleration = anchor.GetAcceleration().Value.Length();

                    if (acceleration >= detonationAcceleration)
                    {
                        Detonate();
                    }
                }
            }

            public void Arm()
            {
                if (state == State.Disabled)
                {
                    state = State.Armed;
                }
            }

            public void Detonate()
            {
                if (state == State.Disabled || state == State.Armed)
                {
                    warhead.Arm();
                    warhead.Detonate();

                    state = State.Detonated;
                    Detonated?.Invoke(this, new EventArgs());
                }
            }

            public override string GetContextName()
            {
                return "weapon-warhead";
            }

            public bool IsFunctional()
            {
                return warheadObserver.IsFunctional() && anchor.IsFunctional();
            }

            protected override void Cleanup()
            {
                warhead.Destruct();
            }

            private void Child_FunctionalityChanged(object sender, EventArgs eventArgs)
            {
                FunctionalityChanged?.Invoke(this, new EventArgs());
            }
        }
    }
}
