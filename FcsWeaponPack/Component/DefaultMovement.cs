﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VRage;
using VRage.Collections;
using VRage.Game;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ObjectBuilders.Definitions;
using VRageMath;

namespace IngameScript
{
    partial class Program
    {
        public class FcsWeaponPack_Component_DefaultMovement : Framework_Component_AbstractComponent, FcsApi_Component_IWeaponMovement
        {
            protected enum State
            {
                Disabled,
                Decoupling,
                Cruising
            }

            public event EventHandler FunctionalityChanged;

            protected const float updateIntervalTime = 0.16f;

            protected State state = State.Disabled;
            protected FcsApi_Data_TargetInfo targetInfo;
            protected float decoupleTime;
            protected string updateIntervalId;
            protected Framework_Utility_Timer timer;
            protected ActorPack_Actor_ThrusterCollection forwardThruster;
            protected ActorPack_Actor_ThrusterCollection bottomThruster;
            protected ActorPack_Actor_ThrusterCollection thruster;
            protected ActorPack_Actor_GyroscopeCollection gyroscope;
            protected ActorPack_Actor_BatteryCollection battery;
            protected ActorPack_Actor_ShipController remoteControl;
            protected Framework_Component_Anchor anchor;
            protected Framework_Actor_MutationObserver thrusterObserver;
            protected Framework_Actor_MutationObserver gyroscopeObserver;
            protected Framework_Actor_MutationObserver batteryObserver;
            protected Framework_Actor_MutationObserver remoteControlObserver;
            protected FcsWeaponPack_Navigation navigation;

            public FcsWeaponPack_Component_DefaultMovement(
                string id,
                Framework_Actor_Data_SearchCriteria forwardThrusterSearchCriteria,
                Framework_Actor_Data_SearchCriteria bottomThrusterSearchCriteria,
                Framework_Actor_Data_SearchCriteria thrusterSearchCriteria,
                Framework_Actor_Data_SearchCriteria gyroscopeSearchCriteria,
                Framework_Actor_Data_SearchCriteria batterySearchCriteria,
                Framework_Actor_Data_SearchCriteria remoteControlSearchCriteria,
                Framework_Component_Anchor anchor,
                float decoupleTime = 0.5f) : base(id)
            {
                timer = ObjectManager.GetObject<Framework_Utility_Timer>();

                forwardThruster = new ActorPack_Actor_ThrusterCollection(forwardThrusterSearchCriteria, true);
                bottomThruster = new ActorPack_Actor_ThrusterCollection(bottomThrusterSearchCriteria, true);
                thruster = new ActorPack_Actor_ThrusterCollection(thrusterSearchCriteria, true);
                gyroscope = new ActorPack_Actor_GyroscopeCollection(gyroscopeSearchCriteria, true);
                battery = new ActorPack_Actor_BatteryCollection(batterySearchCriteria, true);
                remoteControl = new ActorPack_Actor_ShipController(remoteControlSearchCriteria, true);
                this.anchor = anchor;
                this.decoupleTime = decoupleTime;

                thrusterObserver = new Framework_Actor_MutationObserver(thrusterSearchCriteria, true);
                gyroscopeObserver = new Framework_Actor_MutationObserver(gyroscopeSearchCriteria, true);
                batteryObserver = new Framework_Actor_MutationObserver(batterySearchCriteria, true);
                remoteControlObserver = new Framework_Actor_MutationObserver(remoteControlSearchCriteria, true);

                anchor.FunctionalityChanged += Child_FunctionalityChanged;
                thrusterObserver.FunctionalityChanged += Child_FunctionalityChanged;
                gyroscopeObserver.FunctionalityChanged += Child_FunctionalityChanged;
                batteryObserver.FunctionalityChanged += Child_FunctionalityChanged;
                remoteControlObserver.FunctionalityChanged += Child_FunctionalityChanged;

                SetDescription("FcsWeaponPack_Component_DefaultMovement");
            }

            public override string GetContextName()
            {
                return "weapon-movement";
            }

            public virtual void Decouple(FcsApi_Data_AttackType attackType, FcsApi_Data_TargetInfo targetInfo)
            {
                if (state == State.Disabled)
                {
                    this.targetInfo = targetInfo;

                    state = State.Decoupling;

                    thruster.Enable();
                    bottomThruster.SetThrustOverridePercentage(1);

                    gyroscope.Enable();
                    gyroscope.EnableOverride();

                    battery.Enable();

                    timer.SetTimeout(decoupleTime, () => {
                        Cruise(attackType);
                    });
                }
            }

            public virtual void UpdateTarget(FcsApi_Data_TargetInfo targetInfo)
            {
                this.targetInfo = targetInfo;
            }

            public virtual bool IsFunctional()
            {
                return thrusterObserver.IsFunctional()
                    && gyroscopeObserver.IsFunctional()
                    && batteryObserver.IsFunctional()
                    && remoteControlObserver.IsFunctional()
                    && anchor.IsFunctional();
            }

            protected virtual double GetIntermediateHeight()
            {
                return 600;
            }

            protected virtual double GetIntermediateRadius()
            {
                return 600;
            }

            protected override void Cleanup()
            {
                if (state == State.Cruising)
                {
                    timer.ClearInterval(updateIntervalId);
                }

                forwardThruster.Destruct();
                bottomThruster.Destruct();
                thruster.Destruct();
                gyroscope.Destruct();
                battery.Destruct();
                remoteControl.Destruct();
                thrusterObserver.Destruct();
                gyroscopeObserver.Destruct();
                batteryObserver.Destruct();
                remoteControlObserver.Destruct();
            }

            protected virtual void Child_FunctionalityChanged(object sender, EventArgs eventArgs)
            {
                FunctionalityChanged?.Invoke(this, new EventArgs());
            }

            protected virtual void Cruise(FcsApi_Data_AttackType attackType)
            {
                if (state == State.Decoupling)
                {
                    if (anchor.IsFunctional())
                    {
                        state = State.Cruising;

                        bottomThruster.SetThrustOverridePercentage(0);
                        forwardThruster.SetThrustOverridePercentage(1);
                        remoteControl.EnableDampenersOverride();

                        navigation = new FcsWeaponPack_Navigation(attackType, GetIntermediateHeight(), GetIntermediateRadius());

                        updateIntervalId = timer.SetInterval(updateIntervalTime, UpdateCruise);
                    }
                }
            }

            protected virtual void UpdateCruise()
            {
                if (state == State.Cruising)
                {
                    if (targetInfo != null && anchor.IsFunctional())
                    {
                        var weaponPosition = anchor.GetPosition().Value;
                        var weaponWorldMatrix = anchor.GetWorldMatrix().Value;
                        var gravityDirection = remoteControl.GetNaturalGravityDirection();

                        var cruiseAngles = navigation.GetCruiseAngles(targetInfo, weaponPosition, weaponWorldMatrix, gravityDirection);

                        gyroscope.AdjustForDirection(cruiseAngles, weaponWorldMatrix);
                    }
                }
            }
        }
    }
}
