﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VRage;
using VRage.Collections;
using VRage.Game;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ObjectBuilders.Definitions;
using VRageMath;

namespace IngameScript
{
    partial class Program
    {
        public class FcsWeaponPack_Component_HydrogenMovement : FcsWeaponPack_Component_DefaultMovement
        {
            protected ActorPack_Actor_GasTankCollection hydrogenTank;
            protected Framework_Actor_MutationObserver hydrogenTankObserver;
            
            public FcsWeaponPack_Component_HydrogenMovement(
                string id,
                Framework_Actor_Data_SearchCriteria forwardThrusterSearchCriteria,
                Framework_Actor_Data_SearchCriteria bottomThrusterSearchCriteria,
                Framework_Actor_Data_SearchCriteria thrusterSearchCriteria,
                Framework_Actor_Data_SearchCriteria gyroscopeSearchCriteria,
                Framework_Actor_Data_SearchCriteria batterySearchCriteria,
                Framework_Actor_Data_SearchCriteria hydrogenTankSearchCriteria,
                Framework_Actor_Data_SearchCriteria remoteControlSearchCriteria,
                Framework_Component_Anchor anchor,
                float decoupleTime = 0.5f) : base(
                    id,
                    forwardThrusterSearchCriteria,
                    bottomThrusterSearchCriteria,
                    thrusterSearchCriteria,
                    gyroscopeSearchCriteria,
                    batterySearchCriteria,
                    remoteControlSearchCriteria,
                    anchor,
                    decoupleTime)
            {
                hydrogenTank = new ActorPack_Actor_GasTankCollection(hydrogenTankSearchCriteria, true);
                hydrogenTankObserver = new Framework_Actor_MutationObserver(hydrogenTankSearchCriteria, true);
                
                hydrogenTank.EmptinessChanged += Child_FunctionalityChanged;
                hydrogenTankObserver.FunctionalityChanged += Child_FunctionalityChanged;
                
                SetDescription("FcsWeaponPack_Component_HydrogenMovement");
            }

            public override void Decouple(FcsApi_Data_AttackType attackType, FcsApi_Data_TargetInfo targetInfo)
            {
                if (state == State.Disabled)
                {
                    hydrogenTank.DisableStockpile();
                }

                base.Decouple(attackType, targetInfo);
            }

            public override bool IsFunctional()
            {
                return base.IsFunctional()
                    && hydrogenTankObserver.IsFunctional()
                    && !hydrogenTank.IsEmpty();
            }

            protected override void Cleanup()
            {
                base.Cleanup();

                hydrogenTank.Destruct();
                hydrogenTankObserver.Destruct();
            }
        }
    }
}
