﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VRage;
using VRage.Collections;
using VRage.Game;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ObjectBuilders.Definitions;
using VRageMath;

namespace IngameScript
{
    partial class Program
    {
        public class FcsWeaponPack_Component_BombWarhead : Framework_Component_AbstractComponent, FcsApi_Component_IWeaponWarhead
        {
            public event EventHandler FunctionalityChanged;
            public event EventHandler Detonated;

            private enum State
            {
                Disabled,
                Armed,
                Detonated
            }

            private State state = State.Disabled;
            private ActorPack_Actor_WarheadCollection warhead;
            private Framework_Actor_MutationObserver warheadObserver;

            public FcsWeaponPack_Component_BombWarhead(
                string id,
                Framework_Actor_Data_SearchCriteria warheadSearchCriteria) : base(id)
            {
                warhead = new ActorPack_Actor_WarheadCollection(warheadSearchCriteria, true);
                warheadObserver = new Framework_Actor_MutationObserver(warheadSearchCriteria, true);

                warheadObserver.FunctionalityChanged += Child_FunctionalityChanged;

                SetDescription("FcsWeaponPack_Component_BombWarhead");
            }

            public void Arm()
            {
                if (state == State.Disabled)
                {
                    state = State.Armed;
                    warhead.Arm();
                }
            }

            public void Detonate()
            {
                if (state == State.Disabled || state == State.Armed)
                {
                    warhead.Arm();
                    warhead.Detonate();

                    state = State.Detonated;
                    Detonated?.Invoke(this, new EventArgs());
                }
            }

            public override string GetContextName()
            {
                return "weapon-warhead";
            }

            public bool IsFunctional()
            {
                return warheadObserver.IsFunctional();
            }

            protected override void Cleanup()
            {
                warhead.Destruct();
            }

            private void Child_FunctionalityChanged(object sender, EventArgs eventArgs)
            {
                if (state == State.Disabled)
                {
                    FunctionalityChanged?.Invoke(this, new EventArgs());
                }
                else if (state == State.Armed)
                {
                    state = State.Detonated;
                    Detonated?.Invoke(this, new EventArgs());
                }
            }
        }
    }
}
