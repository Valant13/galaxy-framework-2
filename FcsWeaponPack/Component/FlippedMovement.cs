﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VRage;
using VRage.Collections;
using VRage.Game;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ObjectBuilders.Definitions;
using VRageMath;

namespace IngameScript
{
    partial class Program
    {
        public class FcsWeaponPack_Component_FlippedMovement : FcsWeaponPack_Component_HydrogenMovement
        {
            protected const double rollMultiplier = 0.25;
            protected const double gravityMultiplier = 5;

            public FcsWeaponPack_Component_FlippedMovement(
                string id,
                Framework_Actor_Data_SearchCriteria forwardThrusterSearchCriteria,
                Framework_Actor_Data_SearchCriteria bottomThrusterSearchCriteria,
                Framework_Actor_Data_SearchCriteria thrusterSearchCriteria,
                Framework_Actor_Data_SearchCriteria gyroscopeSearchCriteria,
                Framework_Actor_Data_SearchCriteria batterySearchCriteria,
                Framework_Actor_Data_SearchCriteria hydrogenTankSearchCriteria,
                Framework_Actor_Data_SearchCriteria remoteControlSearchCriteria,
                Framework_Component_Anchor anchor,
                float decoupleTime = 0.5f) : base(
                    id,
                    forwardThrusterSearchCriteria,
                    bottomThrusterSearchCriteria,
                    thrusterSearchCriteria,
                    gyroscopeSearchCriteria,
                    batterySearchCriteria,
                    hydrogenTankSearchCriteria,
                    remoteControlSearchCriteria,
                    anchor,
                    decoupleTime)
            {
                SetDescription("FcsWeaponPack_Component_FlippedMovement");
            }

            protected override double GetIntermediateHeight()
            {
                return 1200;
            }

            protected override double GetIntermediateRadius()
            {
                return 600;
            }

            protected override void UpdateCruise()
            {
                if (state == State.Cruising)
                {
                    if (targetInfo != null && anchor.IsFunctional())
                    {
                        var weaponPosition = anchor.GetPosition().Value;
                        var weaponVelocity = anchor.GetVelocity().Value;
                        var weaponWorldMatrix = anchor.GetWorldMatrix().Value;
                        var gravityDirection = remoteControl.GetNaturalGravityDirection();

                        var cruiseAngles = navigation.GetCruiseAngles(targetInfo, weaponPosition, weaponWorldMatrix, gravityDirection);

                        var potentialGravityVelocity = gravityDirection * gravityMultiplier;
                        var potentialWeaponVelocity = weaponVelocity + potentialGravityVelocity;

                        var localProjection = Vector3D.TransformNormal(potentialWeaponVelocity, MatrixD.Transpose(weaponWorldMatrix));

                        double roll;
                        if (localProjection.Y < 0)
                        {
                            roll = -Math.Atan2(localProjection.X, -localProjection.Y);
                        }
                        else
                        {
                            roll = -Math.Atan2(-localProjection.X, localProjection.Y);
                        }

                        cruiseAngles.Z = (float)(roll * rollMultiplier);

                        gyroscope.AdjustForDirection(cruiseAngles, weaponWorldMatrix);
                    }
                }
            }
        }
    }
}
