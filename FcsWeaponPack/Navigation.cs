﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VRage;
using VRage.Collections;
using VRage.Game;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ObjectBuilders.Definitions;
using VRageMath;

namespace IngameScript
{
    partial class Program
    {
        public class FcsWeaponPack_Navigation
        {
            private const double weaponSpeed = 100;
            
            private FcsApi_Data_AttackType attackType;
            private double intermediateHeight;
            private double intermediateRadius;
            private Framework_Utility_Timer timer;

            public FcsWeaponPack_Navigation(FcsApi_Data_AttackType attackType, double intermediateHeight = 600, double intermediateRadius = 600)
            {
                timer = ObjectManager.GetObject<Framework_Utility_Timer>();

                this.attackType = attackType;
                this.intermediateHeight = intermediateHeight;
                this.intermediateRadius = intermediateRadius;
            }

            public Vector3 GetCruiseAngles(FcsApi_Data_TargetInfo targetInfo, Vector3D weaponPosition, MatrixD weaponWorldMatrix, Vector3D gravityDirection)
            {
                var targetPosition = GetTargetPosition(targetInfo);

                if (IsIntermediatePointNeeded(weaponPosition, targetPosition, gravityDirection))
                {
                    var intermediatePoint = targetPosition - gravityDirection * intermediateHeight;

                    return GetAnglesToPoint(weaponPosition, weaponWorldMatrix, intermediatePoint);
                }
                else
                {
                    var targetVelocity = targetInfo.Velocity;
                    var targetOffset = targetPosition - weaponPosition;

                    var interceptionPoint = targetPosition + targetVelocity * GetInterceptionTime(weaponSpeed, targetVelocity, targetOffset);

                    return GetAnglesToPoint(weaponPosition, weaponWorldMatrix, interceptionPoint);
                }
            }

            private bool IsIntermediatePointNeeded(Vector3D weaponPosition, Vector3D targetPosition, Vector3D gravityDirection)
            {
                if (gravityDirection.IsZero())
                {
                    return false;
                }

                var targetOffset = targetPosition - weaponPosition;
                var projectedTargetOffset = Vector3D.ProjectOnPlane(ref targetOffset, ref gravityDirection);

                return attackType == FcsApi_Data_AttackType.Top && projectedTargetOffset.Length() > intermediateRadius;
            }

            private Vector3D GetTargetPosition(FcsApi_Data_TargetInfo targetInfo)
            {
                return targetInfo.Position + targetInfo.Velocity * (timer.GetTimeNow() - targetInfo.DetectionTime);
            }

            private Vector3 GetAnglesToPoint(Vector3D weaponPosition, MatrixD weaponWorldMatrix, Vector3D point)
            {
                var vectorToPoint = Vector3D.TransformNormal(point - weaponPosition, MatrixD.Transpose(weaponWorldMatrix));

                var pitch = Math.Atan2(vectorToPoint.Y, -vectorToPoint.Z);
                var yaw = Math.Atan2(vectorToPoint.X, -vectorToPoint.Z);

                return new Vector3(pitch, yaw, 0);
            }

            private double GetInterceptionTime(double weaponSpeed, Vector3D targetVelocity, Vector3D targetOffset)
            {
                var a = targetVelocity.LengthSquared() - Math.Pow(weaponSpeed, 2);
                var b = 2 * Vector3D.Dot(targetVelocity, targetOffset);
                var c = targetOffset.LengthSquared();

                var D = b * b - 4 * a * c;
                if (D > 0)
                {
                    var x1 = (-b + Math.Sqrt(D)) / (2 * a);
                    var x2 = (-b - Math.Sqrt(D)) / (2 * a);

                    if (x1 > 0)
                    {
                        if (x2 > 0)
                            return Math.Min(x1, x2);
                        return x1;
                    }
                    else if (x2 > 0)
                    {
                        return x2;
                    }
                    else
                    {
                        return double.NaN;
                    }
                }
                else if (D < 0)
                {
                    return double.PositiveInfinity;
                }
                else
                {
                    double result = -b / (2 * a);
                    if (result < 0)
                    {
                        return double.PositiveInfinity;
                    }
                    else
                    {
                        return result;
                    }
                }
            }
        }
    }
}
