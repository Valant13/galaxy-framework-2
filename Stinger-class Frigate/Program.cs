﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System;
using VRage.Collections;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ObjectBuilders.Definitions;
using VRage.Game;
using VRage;
using VRageMath;

namespace IngameScript
{
    partial class Program : MyGridProgram
    {
        private Framework_Bootstrap_Application application;

        public Program()
        {
            application = new Framework_Bootstrap_Application(new Config(), this);
        }

        public void Save()
        {
            Storage = application.Save();
        }

        public void Main(string argument, UpdateType updateSource)
        {
            application.Update(argument, updateSource);
        }
    }
}
