﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System;
using VRage.Collections;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ObjectBuilders.Definitions;
using VRage.Game;
using VRage;
using VRageMath;

namespace IngameScript
{
    partial class Program
    {
        public class Config : Framework_Bootstrap_Data_IConfig
        {
            public string GetName()
            {
                return "Invincible-class Destroyer";
            }

            public UpdateFrequency GetUpdateFrequency()
            {
                return UpdateFrequency.Update1 | UpdateFrequency.Update10 | UpdateFrequency.Update100;
            }

            public void SetupDependencies()
            {
                Framework_DependencyConfig.Setup();
                TerminalUi_DependencyConfig.Setup();
                FcsWeaponPack_DependencyConfig.Setup();
                Fcs_DependencyConfig.Setup();
                Ui_DependencyConfig.Setup();
            }

            public void SetupComponents()
            {
                var searchCriteriaBuilder = new Framework_Actor_SearchCriteriaBuilder();
                var idLayoutBuilder = new UiApi_IdLayoutBuilder();
                var mfdLayoutBuilder = new UiApi_MfdLayoutBuilder();

                var display1 = searchCriteriaBuilder
                    .SetCustomId("Display1")
                    .Build();

                var display2 = searchCriteriaBuilder
                    .SetCustomId("Display2")
                    .Build();

                var display3 = searchCriteriaBuilder
                    .SetCustomId("Display3")
                    .Build();

                new TerminalUi_Component_Display(null, display3, 0, 0.6f, 17);

                new TerminalUi_Component_Display(null);

                var mergeBlockL1 = searchCriteriaBuilder
                    .SetCustomId("MergeBlockL1")
                    .Build();

                var mergeBlockL2 = searchCriteriaBuilder
                    .SetCustomId("MergeBlockL2")
                    .Build();

                var mergeBlockL3 = searchCriteriaBuilder
                    .SetCustomId("MergeBlockL3")
                    .Build();

                var mergeBlockL4 = searchCriteriaBuilder
                    .SetCustomId("MergeBlockL4")
                    .Build();

                var mergeBlockR1 = searchCriteriaBuilder
                    .SetCustomId("MergeBlockR1")
                    .Build();

                var mergeBlockR2 = searchCriteriaBuilder
                    .SetCustomId("MergeBlockR2")
                    .Build();

                var mergeBlockR3 = searchCriteriaBuilder
                    .SetCustomId("MergeBlockR3")
                    .Build();

                var mergeBlockR4 = searchCriteriaBuilder
                    .SetCustomId("MergeBlockR4")
                    .Build();

                var mergeBlockM1 = searchCriteriaBuilder
                    .SetCustomId("MergeBlockM1")
                    .Build();

                var mergeBlockM2 = searchCriteriaBuilder
                    .SetCustomId("MergeBlockM2")
                    .Build();

                var camera = searchCriteriaBuilder
                    .SetCustomClass("Camera")
                    .Build();

                var soundBlock = searchCriteriaBuilder
                    .SetCustomId("SoundBlock")
                    .Build();

                var remoteControl = searchCriteriaBuilder
                    .SetCustomId("RemoteControl")
                    .Build();

                var turretBlock = searchCriteriaBuilder
                    .SetCustomClass("Turret")
                    .Build();

                var fcsViewModel = new Fcs_ViewModel_FireControlSystem();
                var memoryViewModel = new Fcs_ViewModel_Memory();
                var raycastBasedViewModel = new Fcs_ViewModel_RaycastBased();

                idLayoutBuilder
                    .AddSection()
                    .AddRecord("Status", fcsViewModel.State)
                    .AddRecord("Target Distance", fcsViewModel.TargetDistance)
                    .AddRecord("Target Speed", fcsViewModel.TargetSpeed)
                    .AddRecord("Weapon Title", fcsViewModel.WeaponTitle)
                    .AddRecord("Hardpoint Ready", fcsViewModel.IsHardpointReady)
                    .AddRecord("Guided Weapons", fcsViewModel.GuidedWeaponsCount)
                    .AddRecord("Time to Hit", fcsViewModel.TimeToHit);

                idLayoutBuilder
                    .AddSection()
                    .AddRecord("Saved Target", memoryViewModel.TargetPosition);

                idLayoutBuilder
                    .AddSection()
                    .AddRecord("Raycast Interval", raycastBasedViewModel.IntervalTime)
                    .AddRecord("Raycast Distance", raycastBasedViewModel.Distance);

                var idLayout = idLayoutBuilder.Build();

                mfdLayoutBuilder
                    .AddSection()
                    .AddIndicator("L4", fcsViewModel.Hardpoints[FcsApi_Data_HardpointPosition.L4], new Vector2I(0, 0))
                    .AddIndicator("L3", fcsViewModel.Hardpoints[FcsApi_Data_HardpointPosition.L3], new Vector2I(1, 0))
                    .AddIndicator("L2", fcsViewModel.Hardpoints[FcsApi_Data_HardpointPosition.L2], new Vector2I(2, 0))
                    .AddIndicator("L1", fcsViewModel.Hardpoints[FcsApi_Data_HardpointPosition.L1], new Vector2I(3, 0))
                    .AddIndicator("M1", fcsViewModel.Hardpoints[FcsApi_Data_HardpointPosition.M1], new Vector2I(4, 0))
                    .AddIndicator("M2", fcsViewModel.Hardpoints[FcsApi_Data_HardpointPosition.M2], new Vector2I(5, 0))
                    .AddIndicator("R1", fcsViewModel.Hardpoints[FcsApi_Data_HardpointPosition.R1], new Vector2I(6, 0))
                    .AddIndicator("R2", fcsViewModel.Hardpoints[FcsApi_Data_HardpointPosition.R2], new Vector2I(7, 0))
                    .AddIndicator("R3", fcsViewModel.Hardpoints[FcsApi_Data_HardpointPosition.R3], new Vector2I(8, 0))
                    .AddIndicator("R4", fcsViewModel.Hardpoints[FcsApi_Data_HardpointPosition.R4], new Vector2I(9, 0));

                mfdLayoutBuilder
                    .AddSection()
                    .AddIndicator("NONE", fcsViewModel.Guidances[FcsApi_Data_GuidanceType.None], new Vector2I(0, 1))
                    .AddIndicator("ARH", fcsViewModel.Guidances[FcsApi_Data_GuidanceType.ActiveRadarHoming], new Vector2I(1, 1))
                    .AddIndicator("SARH", fcsViewModel.Guidances[FcsApi_Data_GuidanceType.SemiactiveRadarHoming], new Vector2I(2, 1))
                    .AddIndicator("LSBR", fcsViewModel.Guidances[FcsApi_Data_GuidanceType.BeamRiding], new Vector2I(3, 1))
                    .AddIndicator("GPS", fcsViewModel.Guidances[FcsApi_Data_GuidanceType.GlobalPositioning], new Vector2I(4, 1));

                mfdLayoutBuilder
                    .AddSection()
                    .AddIndicator("NONE", fcsViewModel.Detectors[FcsApi_Data_DetectorType.None], new Vector2I(0, 2))
                    .AddIndicator("RAD", fcsViewModel.Detectors[FcsApi_Data_DetectorType.Radar], new Vector2I(1, 2))
                    .AddIndicator("TUR", fcsViewModel.Detectors[FcsApi_Data_DetectorType.Turret], new Vector2I(2, 2))
                    .AddIndicator("LAS", fcsViewModel.Detectors[FcsApi_Data_DetectorType.LaserDesignator], new Vector2I(3, 2))
                    .AddIndicator("MEM", fcsViewModel.Detectors[FcsApi_Data_DetectorType.Memory], new Vector2I(4, 2));

                mfdLayoutBuilder
                    .AddButton(1, "Weapon", "fcs select-next-weapon")
                    .AddButton(2, "Guidance", "fcs select-next-guidance")
                    .AddButton(3, "Detector", "fcs select-next-detector");

                mfdLayoutBuilder.SetGridSize(new Vector2I(10, 3));

                var mfdLayout = mfdLayoutBuilder.Build();

                var anchor = new Framework_Component_Anchor(null, remoteControl);

                var weaponBoundingBox = new BoundingBoxI(new Vector3I(1, -2, -1), new Vector3I(9, 0, 1));
                var hardpointL1 = new Fcs_Component_StaticHardpoint(null, mergeBlockL1, FcsApi_Data_HardpointPosition.L1, weaponBoundingBox);
                var hardpointL2 = new Fcs_Component_StaticHardpoint(null, mergeBlockL2, FcsApi_Data_HardpointPosition.L2, weaponBoundingBox);
                var hardpointL3 = new Fcs_Component_StaticHardpoint(null, mergeBlockL3, FcsApi_Data_HardpointPosition.L3, weaponBoundingBox);
                var hardpointL4 = new Fcs_Component_StaticHardpoint(null, mergeBlockL4, FcsApi_Data_HardpointPosition.L4, weaponBoundingBox);
                var hardpointR1 = new Fcs_Component_StaticHardpoint(null, mergeBlockR1, FcsApi_Data_HardpointPosition.R1, weaponBoundingBox);
                var hardpointR2 = new Fcs_Component_StaticHardpoint(null, mergeBlockR2, FcsApi_Data_HardpointPosition.R2, weaponBoundingBox);
                var hardpointR3 = new Fcs_Component_StaticHardpoint(null, mergeBlockR3, FcsApi_Data_HardpointPosition.R3, weaponBoundingBox);
                var hardpointR4 = new Fcs_Component_StaticHardpoint(null, mergeBlockR4, FcsApi_Data_HardpointPosition.R4, weaponBoundingBox);
                var hardpointM1 = new Fcs_Component_StaticHardpoint(null, mergeBlockM1, FcsApi_Data_HardpointPosition.M1, weaponBoundingBox);
                var hardpointM2 = new Fcs_Component_StaticHardpoint(null, mergeBlockM2, FcsApi_Data_HardpointPosition.M2, weaponBoundingBox);

                var radar = new Fcs_Component_Radar(null, camera, anchor, raycastBasedViewModel, 3000);
                var turret = new Fcs_Component_Turret(null, turretBlock);
                var laserDesignator = new Fcs_Component_LaserDesignator(null, camera, anchor, raycastBasedViewModel, 5000);
                var memory = new Fcs_Component_Memory("fcs-memory", laserDesignator, memoryViewModel);

                var fcs = new Fcs_Component_FireControlSystem(
                    "fcs",
                    anchor,
                    new FcsApi_Component_IHardpoint[]
                    {
                        hardpointL1,
                        hardpointL2,
                        hardpointL3,
                        hardpointL4,
                        hardpointR1,
                        hardpointR2,
                        hardpointR3,
                        hardpointR4,
                        hardpointM1,
                        hardpointM2
                    },
                    new FcsApi_Component_IDetector[] { radar, turret, laserDesignator, memory },
                    fcsViewModel);

                new Ui_Component_InstrumentDisplay("id", idLayout, display2, 0, 0.85f);
                new Ui_Component_MultifunctionDisplay("mfd", mfdLayout, display1);
                new Ui_Component_WarningSystem("ws", new[] { fcsViewModel.WarningSystemState }, soundBlock);
            }
        }
    }
}
