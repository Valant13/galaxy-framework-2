﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System;
using VRage.Collections;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ObjectBuilders.Definitions;
using VRage.Game;
using VRage;
using VRageMath;

namespace IngameScript
{
    partial class Program
    {
        public class Ui_Program_UpdateSurfaceTicker : Framework_Bootstrap_IUpdateHandler
        {
            private Ui_SurfaceTicker surfaceTicker;

            public Ui_Program_UpdateSurfaceTicker()
            {
                surfaceTicker = ObjectManager.GetObject<Ui_SurfaceTicker>();
            }

            public void Execute()
            {
                surfaceTicker.Update();
            }

            public UpdateType GetUpdateCondition()
            {
                return UpdateType.Update10 | UpdateType.Update100;
            }

            public int GetSortOrder()
            {
                return 400;
            }
        }
    }
}
