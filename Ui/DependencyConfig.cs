﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System;
using VRage.Collections;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ObjectBuilders.Definitions;
using VRage.Game;
using VRage;
using VRageMath;

namespace IngameScript
{
    partial class Program
    {
        public static class Ui_DependencyConfig
        {
            public static void Setup()
            {
                Framework_DependencyConfig.Setup();
                ActorPack_DependencyConfig.Setup();
                UiApi_DependencyConfig.Setup();

                var module = ObjectSetup.CreateModule("Ui");

                module.DefineObject<Ui_Program_UpdateSurfaceTicker>();
                module.DefineObject<Ui_Command_MultifunctionDisplay_PressButton>();
                module.DefineObject<Ui_Command_This_FixRender>();
                module.DefineObject<Ui_DrawingService>();
                module.DefineObject<Ui_SurfaceTicker>();

                module.ModifyObject<Framework_Bootstrap_RuntimeRegistry>()
                    .AddArgumentValue("UpdateHandlers", "UpdateSurfaceTicker", typeof(Ui_Program_UpdateSurfaceTicker));

                module.ModifyObject<Framework_Terminal_CommandRegistry>()
                    .AddArgumentValue("Commands", "MultifunctionDisplay_PressButton", typeof(Ui_Command_MultifunctionDisplay_PressButton))
                    .AddArgumentValue("Commands", "This_FixRender", typeof(Ui_Command_This_FixRender));
            }
        }
    }
}
