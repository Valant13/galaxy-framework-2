﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VRage;
using VRage.Collections;
using VRage.Game;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ObjectBuilders.Definitions;
using VRageMath;

namespace IngameScript
{
    partial class Program
    {
        public class Ui_DrawingService
        {
            private const string textureName = "SquareSimple";

            public void DrawBox(
                ref MySpriteDrawFrame frame,
                Vector2 position,
                Vector2 size,
                Color color,
                float borderWidth,
                Color borderColor)
            {
                if (borderWidth > 0)
                {
                    var borderSprite = new MySprite()
                    {
                        Type = SpriteType.TEXTURE,
                        Data = textureName,
                        Position = position,
                        Size = size,
                        Color = borderColor,
                        Alignment = TextAlignment.CENTER
                    };

                    frame.Add(borderSprite);
                }

                var boxSprite = new MySprite()
                {
                    Type = SpriteType.TEXTURE,
                    Data = textureName,
                    Position = position,
                    Size = size - new Vector2(borderWidth * 2),
                    Color = color,
                    Alignment = TextAlignment.CENTER
                };

                frame.Add(boxSprite);
            }

            public void DrawBox(
                ref MySpriteDrawFrame frame,
                Vector2 position,
                Vector2 size,
                Color color)
            {
                DrawBox(ref frame, position, size, color, 0, Color.White);
            }

            public void DrawLine(
                ref MySpriteDrawFrame frame,
                Vector2 positionA,
                Vector2 positionB,
                float width,
                Color color)
            {
                var lineVector = positionB - positionA;

                var position = (positionA + positionB) / 2;
                var rotation = (float)Math.Acos(lineVector.X / lineVector.Length());
                var size = new Vector2(lineVector.Length(), width);

                var lineSprite = new MySprite()
                {
                    Type = SpriteType.TEXTURE,
                    Data = textureName,
                    Position = position,
                    Size = size,
                    Color = color,
                    RotationOrScale = rotation,
                    Alignment = TextAlignment.CENTER
                };

                frame.Add(lineSprite);
            }

            public void DrawText(
                ref MySpriteDrawFrame frame,
                Vector2 position,
                string text,
                string font,
                float fontSize,
                Color color)
            {
                var titleSprite = new MySprite()
                {
                    Type = SpriteType.TEXT,
                    Data = text,
                    Position = position + new Vector2(0, -fontSize * 15),
                    RotationOrScale = fontSize,
                    Color = color,
                    Alignment = TextAlignment.CENTER,
                    FontId = font
                };

                frame.Add(titleSprite);
            }

            public void DrawTable(
                ref MySpriteDrawFrame frame,
                Ui_Data_TableItemCollection items,
                Vector2 position,
                Vector2 size,
                int rowsCount,
                int columnsCount,
                Color color,
                float borderWidth,
                Color borderColor)
            {
                var cellSize = (size + new Vector2(borderWidth) * new Vector2(columnsCount - 1, rowsCount - 1)) / new Vector2(columnsCount, rowsCount);

                var cellIntend = cellSize / 2;
                var cellInterval = cellSize - new Vector2(borderWidth);

                for (int i = 0; i < rowsCount; i++)
                {
                    for (int j = 0; j < columnsCount; j++)
                    {
                        var item = items.GetTableItem(i, j);

                        var cellPosition = cellIntend + cellInterval * new Vector2(j, i) + position - size / 2;
                        if (item == null)
                        {
                            DrawBox(ref frame, cellPosition, cellSize, color, borderWidth, borderColor);
                        }
                        else
                        {
                            DrawBox(ref frame, cellPosition, cellSize, item.BackgroundColor, borderWidth, borderColor);
                            DrawText(ref frame, cellPosition, item.Text, item.Font, item.FontSize, item.ForegroundColor);
                        }
                    }
                }
            }

            public RectangleF GetViewport(IMyTextSurface surface)
            {
                return new RectangleF((surface.TextureSize - surface.SurfaceSize) / 2, surface.SurfaceSize);
            }
        }
    }
}
