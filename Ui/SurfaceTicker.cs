﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VRage;
using VRage.Collections;
using VRage.Game;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ObjectBuilders.Definitions;
using VRageMath;

namespace IngameScript
{
    partial class Program
    {
        public class Ui_SurfaceTicker
        {
            public event EventHandler RenderUpdating;
            public event EventHandler RenderFixing;
            public event EventHandler RenderFixed;

            private const float renderFixingTime = 0.5f;

            private bool isRenderFixing;
            private Framework_Utility_Timer timer;

            public Ui_SurfaceTicker()
            {
                timer = ObjectManager.GetObject<Framework_Utility_Timer>();
            }

            public void Update()
            {
                if (!isRenderFixing)
                {
                    RenderUpdating?.Invoke(this, new EventArgs());
                }
            }

            public void FixRender()
            {
                if (!isRenderFixing)
                {
                    isRenderFixing = true;
                    RenderFixing?.Invoke(this, new EventArgs());

                    timer.SetTimeout(renderFixingTime, () => {
                        isRenderFixing = false;
                        RenderFixed.Invoke(this, new EventArgs());
                    });
                }
            }

            public bool IsRenderFixing()
            {
                return isRenderFixing;
            }
        }
    }
}
