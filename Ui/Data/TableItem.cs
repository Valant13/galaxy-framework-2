﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VRage;
using VRage.Collections;
using VRage.Game;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ObjectBuilders.Definitions;
using VRageMath;

namespace IngameScript
{
    partial class Program
    {
        public class Ui_Data_TableItem
        {
            public int RowNumber { get; set; }
            public int ColumnNumber { get; set; }
            public string Text { get; set; }
            public string Font { get; set; }
            public float FontSize { get; set; }
            public Color ForegroundColor { get; set; }
            public Color BackgroundColor { get; set; }

            public Ui_Data_TableItem()
            { }

            public Ui_Data_TableItem(int rowNumber, int columnNumber, string text, string font, float fontSize, Color foregroundColor, Color backgroundColor)
            {
                RowNumber = rowNumber;
                ColumnNumber = columnNumber;
                Text = text;
                Font = font;
                FontSize = fontSize;
                ForegroundColor = foregroundColor;
                BackgroundColor = backgroundColor;
            }
        }
    }
}
