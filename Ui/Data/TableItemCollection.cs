﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VRage;
using VRage.Collections;
using VRage.Game;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ObjectBuilders.Definitions;
using VRageMath;

namespace IngameScript
{
    partial class Program
    {
        public class Ui_Data_TableItemCollection
        {
            private Dictionary<int, Dictionary<int, Ui_Data_TableItem>> tableItems = new Dictionary<int, Dictionary<int, Ui_Data_TableItem>>();

            public void CreateTableItem(int rowNumber, int columnNumber, string text, string font, float fontSize, Color foregroundColor, Color backgroundColor)
            {
                var tableItem = new Ui_Data_TableItem(rowNumber, columnNumber, text, font, fontSize, foregroundColor, backgroundColor);

                if (!tableItems.ContainsKey(rowNumber))
                {
                    tableItems[rowNumber] = new Dictionary<int, Ui_Data_TableItem>();
                }

                tableItems[rowNumber][columnNumber] = tableItem;
            }

            public Ui_Data_TableItem GetTableItem(int rowNumber, int columnNumber)
            {
                if (!tableItems.ContainsKey(rowNumber))
                {
                    return null;
                }

                if (!tableItems[rowNumber].ContainsKey(columnNumber))
                {
                    return null;
                }

                return tableItems[rowNumber][columnNumber];
            }
        }
    }
}
