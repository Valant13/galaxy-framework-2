﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VRage;
using VRage.Collections;
using VRage.Game;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ObjectBuilders.Definitions;
using VRageMath;

namespace IngameScript
{
    partial class Program
    {
        public class Ui_Component_MultifunctionDisplay : Framework_Component_AbstractComponent, UiApi_Component_IMultifunctionDisplay
        {
            private const string font = "Debug";
            private const float buttonPressTime = 0.15f;

            private int surfaceIndex;
            private ActorPack_Actor_TextSurfaceProvider surfaceProvider;
            private UiApi_Data_MfdLayout layout;
            private bool isDataChanged;
            private Dictionary<int, string> pressedButtonTimeouts = new Dictionary<int, string>();
            private Vector2I gridSize;
            private Framework_Terminal_CommandService commandService;
            private Ui_DrawingService drawingService;
            private Framework_Utility_Timer timer;
            private Ui_SurfaceTicker surfaceTicker;

            public Ui_Component_MultifunctionDisplay(
                string id,
                UiApi_Data_MfdLayout layout,
                Framework_Actor_Data_SearchCriteria surfaceProviderSearchCriteria = null,
                int surfaceIndex = 0) : base(id)
            {
                commandService = ObjectManager.GetObject<Framework_Terminal_CommandService>();
                drawingService = ObjectManager.GetObject<Ui_DrawingService>();
                timer = ObjectManager.GetObject<Framework_Utility_Timer>();
                surfaceTicker = ObjectManager.GetObject<Ui_SurfaceTicker>();

                this.layout = layout;
                this.surfaceIndex = surfaceIndex;
                gridSize = layout.GridSize == Vector2I.Zero ? new Vector2I(6, 4) : layout.GridSize;

                SetDescription("Ui_Component_MultifunctionDisplay");

                if (surfaceProviderSearchCriteria == null)
                {
                    surfaceProviderSearchCriteria = new Framework_Actor_Data_SearchCriteria() { IsMe = true };
                }
                surfaceProvider = new ActorPack_Actor_TextSurfaceProvider(surfaceProviderSearchCriteria);

                foreach (var section in layout.Sections)
                {
                    foreach (var indicator in section.Indicators)
                    {
                        indicator.Binding.StatusChanged += Binding_StatusChanged;
                    }
                }

                surfaceTicker.RenderUpdating += SurfaceTicker_RenderUpdating;
                surfaceTicker.RenderFixing += SurfaceTicker_RenderFixing;
                surfaceTicker.RenderFixed += SurfaceTicker_RenderFixed;

                if (HasSurface())
                {
                    ConfigureSurface();
                    RenderContent(layout, pressedButtonTimeouts.Keys.ToList());
                }
            }

            public override string GetContextName()
            {
                return "multifunction-display";
            }

            public string GetButtonCommand(int buttonNumber)
            {
                if (!HasButton(buttonNumber))
                {
                    throw new Exception($"Button with number {buttonNumber} does not exist");
                }

                var button = layout.Buttons[buttonNumber];

                return button.Command;
            }

            public bool HasButton(int buttonNumber)
            {
                return layout.Buttons.ContainsKey(buttonNumber);
            }

            public void PressButton(int buttonNumber)
            {
                if (!HasButton(buttonNumber))
                {
                    throw new Exception($"Button with number {buttonNumber} does not exist");
                }

                var button = layout.Buttons[buttonNumber];

                commandService.ExecuteCommandsByString(button.Command);

                if (pressedButtonTimeouts.ContainsKey(buttonNumber))
                {
                    timer.SetTimeout(buttonPressTime, pressedButtonTimeouts[buttonNumber]);
                }
                else
                {
                    isDataChanged = true;

                    pressedButtonTimeouts[buttonNumber] = timer.SetTimeout(buttonPressTime, () => {
                        pressedButtonTimeouts.Remove(buttonNumber);
                        isDataChanged = true;
                    });
                }
            }

            protected override void Cleanup()
            {
                surfaceProvider.Destruct();
            }

            private void RenderContent(UiApi_Data_MfdLayout layout, List<int> pressedButtonNumbers)
            {
                var frame = GetSurface().DrawFrame();

                foreach (var sectionLayout in layout.Sections)
                {
                    RenderSection(ref frame, sectionLayout);
                }

                RenderToolbar(ref frame, layout, pressedButtonNumbers);

                frame.Dispose();
            }

            private void RenderSection(ref MySpriteDrawFrame frame, UiApi_Data_MfdSectionLayout sectionLayout)
            {
                RenderSectionLine(ref frame, sectionLayout);

                foreach (var indicatorLayout in sectionLayout.Indicators)
                {
                    RenderIndicator(ref frame, indicatorLayout);
                }
            }

            private void RenderSectionLine(ref MySpriteDrawFrame frame, UiApi_Data_MfdSectionLayout sectionLayout)
            {
                var minPositionInGrid = new Vector2I(999, 999);
                var maxPositionInGrid = new Vector2I(0, 0);

                var visibleIndicatorsCount = 0;
                foreach (var indicatorLayout in sectionLayout.Indicators)
                {
                    if (indicatorLayout.Binding.GetStatus() != UiApi_Data_MfdIndicatorStatus.Hidden)
                    {
                        minPositionInGrid.X = Math.Min(minPositionInGrid.X, indicatorLayout.PositionInGrid.X);
                        minPositionInGrid.Y = Math.Min(minPositionInGrid.Y, indicatorLayout.PositionInGrid.Y);
                        maxPositionInGrid.X = Math.Max(maxPositionInGrid.X, indicatorLayout.PositionInGrid.X);
                        maxPositionInGrid.Y = Math.Max(maxPositionInGrid.Y, indicatorLayout.PositionInGrid.Y);

                        visibleIndicatorsCount++;
                    }
                }

                if (visibleIndicatorsCount < 2)
                {
                    return;
                }

                var lineColor = GetForegroundColor();

                var viewport = GetViewport();
                var scale = GetGridScale(viewport);

                var linePositionA = ConvertPositionInGrid(viewport, minPositionInGrid);
                var linePositionB = ConvertPositionInGrid(viewport, maxPositionInGrid);
                var lineWidth = GetLineWidth(scale);

                drawingService.DrawLine(ref frame, linePositionA, linePositionB, lineWidth, lineColor);
            }

            private void RenderIndicator(ref MySpriteDrawFrame frame, UiApi_Data_MfdIndicatorLayout indicatorLayout)
            {
                var borderColor = new Color();
                var boxColor = new Color();
                var titleColor = new Color();

                var bindingStatus = indicatorLayout.Binding.GetStatus();
                if (bindingStatus == UiApi_Data_MfdIndicatorStatus.Hidden)
                {
                    return;
                }
                else if (bindingStatus == UiApi_Data_MfdIndicatorStatus.Enabled)
                {
                    borderColor = GetForegroundColor();
                    boxColor = GetForegroundColor();
                    titleColor = GetBackgroundColor();
                }
                else if (bindingStatus == UiApi_Data_MfdIndicatorStatus.Disabled)
                {
                    borderColor = GetForegroundColor();
                    boxColor = GetBackgroundColor();
                    titleColor = GetForegroundColor();
                }

                var viewport = GetViewport();
                var scale = GetGridScale(viewport);

                var indicatorSize = new Vector2(scale) * 0.75f;
                var indicatorPosition = ConvertPositionInGrid(viewport, indicatorLayout.PositionInGrid);

                var fontSize = scale * 0.01f;
                var borderWidth = GetLineWidth(scale);

                drawingService.DrawBox(ref frame, indicatorPosition, indicatorSize, boxColor, borderWidth, borderColor);
                drawingService.DrawText(ref frame, indicatorPosition, indicatorLayout.Title, font, fontSize, titleColor);
            }

            private void RenderToolbar(ref MySpriteDrawFrame frame, UiApi_Data_MfdLayout layout, List<int> pressedButtonNumbers)
            {
                var viewport = GetViewport();
                var gridScale = GetGridScale(viewport);
                var borderWidth = GetLineWidth(gridScale);

                var toolbarScale = GetToolbarScale(viewport);
                var fontSize = toolbarScale * 0.0025f;

                var tableSize = new Vector2(viewport.Width, GetToolbarHeight(toolbarScale));
                var tablePosition = viewport.Position + viewport.Size - tableSize / 2;

                var tableItems = new Ui_Data_TableItemCollection();

                var columnNumber = 0;
                foreach (var button in layout.Buttons.Values)
                {
                    Color foregroundColor;
                    Color backgroundColor;

                    if (pressedButtonNumbers.Contains(button.Number))
                    {
                        foregroundColor = GetBackgroundColor();
                        backgroundColor = GetForegroundColor();
                    }
                    else
                    {
                        foregroundColor = GetForegroundColor();
                        backgroundColor = GetBackgroundColor();
                    }

                    tableItems.CreateTableItem(0, columnNumber, button.Title, font, fontSize, foregroundColor, backgroundColor);

                    columnNumber++;
                }

                var borderColor = GetForegroundColor();
                var tableColor = GetBackgroundColor();

                drawingService.DrawTable(ref frame, tableItems, tablePosition, tableSize, 1, layout.Buttons.Count, tableColor, borderWidth, borderColor);
            }

            private void Binding_StatusChanged(object sender, EventArgs eventArgs)
            {
                isDataChanged = true;
            }

            private void SurfaceTicker_RenderUpdating(object sender, EventArgs eventArgs)
            {
                if (isDataChanged && HasSurface())
                {
                    RenderContent(layout, pressedButtonTimeouts.Keys.ToList());
                    isDataChanged = false;
                }
            }

            private void SurfaceTicker_RenderFixing(object sender, EventArgs eventArgs)
            {
                if (HasSurface())
                {
                    RenderContent(layout, pressedButtonTimeouts.Keys.ToList());
                    isDataChanged = false;
                }
            }

            private void SurfaceTicker_RenderFixed(object sender, EventArgs eventArgs)
            {
                if (HasSurface())
                {
                    RenderContent(layout, pressedButtonTimeouts.Keys.ToList());
                    isDataChanged = false;
                }
            }

            private float GetLineWidth(float gridScale)
            {
                return gridScale * 0.05f;
            }

            private float GetGridScale(RectangleF viewport)
            {
                var toolbarHeight = GetToolbarHeight(GetToolbarScale(viewport));

                var horizontalInterval = viewport.Width / gridSize.X;
                var verticalInterval = (viewport.Height - toolbarHeight) / gridSize.Y;

                return Math.Min(horizontalInterval, verticalInterval);
            }

            private float GetToolbarHeight(float toolbarScale)
            {
                return toolbarScale * 0.1f;
            }

            private float GetToolbarScale(RectangleF viewport)
            {
                return viewport.Height;
            }

            private Vector2 ConvertPositionInGrid(RectangleF viewport, Vector2I positionInGrid)
            {
                var scale = GetGridScale(viewport);

                var gridInterval = new Vector2(scale);
                var gridIndent = new Vector2(scale) / 2;

                return viewport.Position + gridIndent + gridInterval * positionInGrid;
            }

            private void ConfigureSurface()
            {
                var surface = GetSurface();

                surface.ContentType = ContentType.SCRIPT;
                surface.Script = "";
            }

            private Color GetForegroundColor()
            {
                var surface = GetSurface();

                return surfaceTicker.IsRenderFixing() ? surface.ScriptBackgroundColor : surface.ScriptForegroundColor;
            }

            private Color GetBackgroundColor()
            {
                var surface = GetSurface();

                return surfaceTicker.IsRenderFixing() ? surface.ScriptForegroundColor : surface.ScriptBackgroundColor;
            }

            private RectangleF GetViewport()
            {
                return drawingService.GetViewport(GetSurface());
            }

            private IMyTextSurface GetSurface()
            {
                return surfaceProvider.GetSurface(surfaceIndex);
            }

            private bool HasSurface()
            {
                return GetSurface() != null;
            }
        }
    }
}
