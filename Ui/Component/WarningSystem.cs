﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VRage;
using VRage.Collections;
using VRage.Game;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ObjectBuilders.Definitions;
using VRageMath;

namespace IngameScript
{
    partial class Program
    {
        public class Ui_Component_WarningSystem : Framework_Component_AbstractComponent, UiApi_Component_IWarningSystem
        {
            private readonly Dictionary<UiApi_Data_WsStatus, Ui_Data_WsSignal> signals = new Dictionary<UiApi_Data_WsStatus, Ui_Data_WsSignal>() {
                { UiApi_Data_WsStatus.None, null },
                { UiApi_Data_WsStatus.Scanning, new Ui_Data_WsSignal("SoundBlockAlert2") },
                { UiApi_Data_WsStatus.Locked, new Ui_Data_WsSignal("SoundBlockAlert2", 0.14f) },
                { UiApi_Data_WsStatus.Caution, null },
                { UiApi_Data_WsStatus.Warning, null }
            };

            private ActorPack_Actor_SoundBlock soundBlock;
            private List<UiApi_Data_WsBinding> bindings;
            private UiApi_Data_WsStatus currentStatus;

            public Ui_Component_WarningSystem(
                string id,
                UiApi_Data_WsBinding[] bindings,
                Framework_Actor_Data_SearchCriteria soundBlockSearchCriteria) : base(id)
            {
                SetDescription("Ui_Component_WarningSystem");

                soundBlock = new ActorPack_Actor_SoundBlock(soundBlockSearchCriteria);
                this.bindings = bindings.ToList();

                foreach (var binding in bindings)
                {
                    binding.StatusChanged += Binding_StatusChanged;
                }

                var status = GetStatus();
                currentStatus = status;
                EmitSignal(signals[status]);
            }

            private void Binding_StatusChanged(object sender, EventArgs eventArgs)
            {
                var status = GetStatus();

                if (status == currentStatus)
                {
                    return;
                }

                currentStatus = status;
                EmitSignal(signals[status]);
            }

            public override string GetContextName()
            {
                return "warning-system";
            }

            protected override void Cleanup()
            {
                soundBlock.Destruct();
            }

            private UiApi_Data_WsStatus GetStatus()
            {
                var status = UiApi_Data_WsStatus.None;

                foreach (var binding in bindings)
                {
                    var bindingStatus = binding.GetStatus();

                    if (bindingStatus > status)
                    {
                        status = bindingStatus;
                    }
                }

                return status;
            }

            private void EmitSignal(Ui_Data_WsSignal signal)
            {
                soundBlock.StopSound();

                if (signal == null)
                {
                    return;
                }

                soundBlock.SetSound(signal.Sound);
                if (signal.LoopPeriod.HasValue)
                {
                    soundBlock.PlaySound(signal.LoopPeriod.Value);
                }
                else
                {
                    soundBlock.PlaySound();
                }
            }
        }
    }
}
