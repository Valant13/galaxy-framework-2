﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VRage;
using VRage.Collections;
using VRage.Game;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ObjectBuilders.Definitions;
using VRageMath;

namespace IngameScript
{
    partial class Program
    {
        public class Ui_Component_InstrumentDisplay : Framework_Component_AbstractComponent, UiApi_Component_IInstrumentDisplay
        {
            private const string font = "Debug";
            private const string defaultValue = "--";

            private int surfaceIndex;
            private float fontSize;
            private ActorPack_Actor_TextSurfaceProvider surfaceProvider;
            private UiApi_Data_IdLayout layout;
            private bool isDataChanged;

            public Ui_Component_InstrumentDisplay(
                string id,
                UiApi_Data_IdLayout layout,
                Framework_Actor_Data_SearchCriteria surfaceProviderSearchCriteria = null,
                int surfaceIndex = 0,
                float fontSize = 0.5f) : base(id)
            {
                var surfaceTicker = ObjectManager.GetObject<Ui_SurfaceTicker>();
                this.layout = layout;
                this.surfaceIndex = surfaceIndex;
                this.fontSize = fontSize;

                SetDescription("Ui_Component_InstrumentDisplay");

                if (surfaceProviderSearchCriteria == null)
                {
                    surfaceProviderSearchCriteria = new Framework_Actor_Data_SearchCriteria() { IsMe = true };
                }
                surfaceProvider = new ActorPack_Actor_TextSurfaceProvider(surfaceProviderSearchCriteria);

                foreach (var section in layout.Sections)
                {
                    foreach (var record in section.Records)
                    {
                        record.Binding.ValueChanged += Binding_ValueChanged;
                    }
                }

                surfaceTicker.RenderUpdating += SurfaceTicker_RenderUpdating;

                if (HasSurface())
                {
                    ConfigureSurface();
                    RenderContent(layout);
                }
            }

            public override string GetContextName()
            {
                return "indicator-display";
            }

            protected override void Cleanup()
            {
                surfaceProvider.Destruct();
            }

            private void RenderContent(UiApi_Data_IdLayout layout)
            {
                var content = "";

                foreach (var section in layout.Sections)
                {
                    var sectionTitle = section.Title;

                    if (sectionTitle != null)
                    {
                        content += $"{sectionTitle}\n";
                    }

                    foreach (var record in section.Records)
                    {
                        var value = record.Binding.GetValue();

                        value = string.IsNullOrEmpty(value) ? defaultValue : value;
                        content += $"{record.Title}: {value}\n";
                    }

                    content += $"\n";
                }

                GetSurface().WriteText(content);
            }

            private void Binding_ValueChanged(object sender, EventArgs eventArgs)
            {
                isDataChanged = true;
            }

            private void SurfaceTicker_RenderUpdating(object sender, EventArgs eventArgs)
            {
                if (isDataChanged && HasSurface())
                {
                    RenderContent(layout);
                    isDataChanged = false;
                }
            }

            private void ConfigureSurface()
            {
                var surface = GetSurface();

                surface.ContentType = ContentType.TEXT_AND_IMAGE;
                surface.Font = font;
                surface.FontSize = fontSize;
                surface.Alignment = TextAlignment.CENTER;
            }

            private IMyTextSurface GetSurface()
            {
                return surfaceProvider.GetSurface(surfaceIndex);
            }

            private bool HasSurface()
            {
                return GetSurface() != null;
            }
        }
    }
}
