﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System;
using VRage.Collections;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ObjectBuilders.Definitions;
using VRage.Game;
using VRage;
using VRageMath;

namespace IngameScript
{
    partial class Program
    {
        public class Ui_Command_MultifunctionDisplay_PressButton : Framework_Terminal_AbstractCommand
        {
            public Ui_Command_MultifunctionDisplay_PressButton()
            {
                SetDescription("Press MFD button");
                AddArgument("button-number");
            }

            public override int Execute(Framework_Terminal_ISubject subject, Framework_Terminal_Input input, Framework_Terminal_Output output)
            {
                var mfd = (UiApi_Component_IMultifunctionDisplay)subject;

                int buttonNumber;

                try
                {
                    buttonNumber = int.Parse(input.GetArgument("button-number"));
                }
                catch (Exception e)
                {
                    output.WriteLine(e.Message);

                    return 1;
                }

                if (!mfd.HasButton(buttonNumber))
                {
                    output.WriteLine($"MFD does not have button with number {buttonNumber}");

                    return 0;
                }

                var command = mfd.GetButtonCommand(buttonNumber);
                output.WriteLine($"Executing \"{command}\"");

                mfd.PressButton(buttonNumber);

                return 0;
            }

            public override string GetContextName()
            {
                return "multifunction-display";
            }

            public override string GetName()
            {
                return "press-button";
            }
        }
    }
}
