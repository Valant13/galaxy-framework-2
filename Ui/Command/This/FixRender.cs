﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System;
using VRage.Collections;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ObjectBuilders.Definitions;
using VRage.Game;
using VRage;
using VRageMath;

namespace IngameScript
{
    partial class Program
    {
        public class Ui_Command_This_FixRender : Framework_Terminal_AbstractCommand
        {
            private Ui_SurfaceTicker surfaceTicker;

            public Ui_Command_This_FixRender()
            {
                surfaceTicker = ObjectManager.GetObject<Ui_SurfaceTicker>();

                SetDescription("Fix render of UI elements");
            }

            public override string GetName()
            {
                return "fix-render";
            }

            public override string GetContextName()
            {
                return "this";
            }

            public override int Execute(Framework_Terminal_ISubject subject, Framework_Terminal_Input input, Framework_Terminal_Output output)
            {
                surfaceTicker.FixRender();

                output.WriteLine("Done");

                return 0;
            }
        }
    }
}
