﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System;
using VRage.Collections;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ObjectBuilders.Definitions;
using VRage.Game;
using VRage;
using VRageMath;

namespace IngameScript
{
    partial class Program
    {
        public class Framework_Terminal_TerminalService
        {
            public delegate void InputProcessingEventHandler(object sender, string input);
            public delegate void OutputReturningEventHandler(object sender, string output);
            public event InputProcessingEventHandler InputProcessing;
            public event OutputReturningEventHandler OutputReturning;

            private bool isInputProcessing;
            private Framework_Terminal_CommandService commandService;

            public Framework_Terminal_TerminalService()
            {
                commandService = ObjectManager.GetObject<Framework_Terminal_CommandService>();
            }

            public string ProcessInput(string input)
            {
                if (isInputProcessing)
                {
                    throw new Exception("An input is already processing");
                }

                isInputProcessing = true;

                InputProcessing?.Invoke(this, input);
                commandService.ExecuteCommandsByString(input);

                var output = commandService.GetOutputBuffer();
                commandService.ClearOutputBuffer();
                OutputReturning?.Invoke(this, output);

                isInputProcessing = false;

                return output;
            }
        }
    }
}
