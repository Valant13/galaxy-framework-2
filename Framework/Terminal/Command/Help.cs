﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System;
using VRage.Collections;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ObjectBuilders.Definitions;
using VRage.Game;
using VRage;
using VRageMath;

namespace IngameScript
{
    partial class Program
    {
        public class Framework_Terminal_Command_Help : Framework_Terminal_AbstractCommand
        {
            private Framework_Terminal_SubjectRegistry subjectRegistry;

            public Framework_Terminal_Command_Help()
            {
                subjectRegistry = ObjectManager.GetObject<Framework_Terminal_SubjectRegistry>();

                SetDescription("Provides information about existing commands and their usage");
                AddArgument("context", true);
                AddArgument("command", true);
            }

            public override string GetName()
            {
                return "help";
            }

            public override string GetContextName()
            {
                return "this";
            }

            public override int Execute(Framework_Terminal_ISubject subject, Framework_Terminal_Input input, Framework_Terminal_Output output)
            {
                if (!input.HasArgument("context") && !input.HasArgument("command"))
                {
                    ExecuteGeneralHelp(output);
                }
                else if (input.HasArgument("context") && !input.HasArgument("command"))
                {
                    var contextName = input.GetArgument("context");

                    ExecuteContextHelp(output, contextName);
                }
                else
                {
                    var contextName = input.GetArgument("context");
                    var commandName = input.GetArgument("command");

                    ExecuteCommandHelp(output, contextName, commandName);
                }

                return 0;
            }

            private void ExecuteGeneralHelp(Framework_Terminal_Output output)
            {
                output.WriteLine("Command syntax:");
                output.WriteLine("<subject> <command> [<argumen>] [<option>[=value]]");
                output.WriteLine();
                output.WriteLine("Commands:");
                foreach (var context in GetCommandRegistry().GetContexts().Values)
                {
                    output.WriteListItem(context.Name);
                    foreach (var command in context.Commands.Values)
                    {
                        output.WriteListSubitem(command.GetName(), command.GetDescription());
                    }
                }
            }

            private void ExecuteContextHelp(Framework_Terminal_Output output, string contextName)
            {
                if (GetCommandRegistry().HasContext(contextName))
                {
                    var context = GetCommandRegistry().GetContext(contextName);

                    output.WriteLine("Commands:");
                    foreach (var command in context.Commands.Values)
                    {
                        output.WriteListItem(command.GetName(), command.GetDescription());
                    }

                    output.WriteLine();
                    output.WriteLine("Subjects:");
                    foreach (var subject in subjectRegistry.GetSubjects().Values)
                    {
                        if (subject.GetContextName() == contextName)
                        {
                            output.WriteListItem(subject.GetSubjectName(), subject.GetDescription());
                        }
                    }
                }
                else
                {
                    output.WriteLine($"Context {contextName} does not exist");
                }
            }

            private void ExecuteCommandHelp(Framework_Terminal_Output output, string contextName, string commandName)
            {
                if (GetCommandRegistry().HasContext(contextName))
                {
                    var context = GetCommandRegistry().GetContext(contextName);
                    if (context.Commands.ContainsKey(commandName))
                    {
                        var command = context.Commands[commandName];

                        var argumentsString = "";
                        foreach (var argument in command.GetArguments())
                        {
                            if (argument.IsOptional)
                            {
                                argumentsString += $"[{argument.Name}] ";
                            }
                            else
                            {
                                argumentsString += $"{argument.Name} ";
                            }
                        }

                        output.WriteLine("Command syntax:");
                        output.WriteLine($"<subject> {command.GetName()} {argumentsString}");
                        output.WriteLine(command.GetDescription());
                        output.WriteLine();
                        output.WriteLine("Arguments:");
                        foreach (var argument in command.GetArguments())
                        {
                            output.WriteListItem(argument.Name, argument.Description);
                        }
                        output.WriteLine();
                        output.WriteLine("Options:");
                        foreach (var option in command.GetOptions())
                        {
                            if (string.IsNullOrEmpty(option.Shortcut))
                            {
                                output.WriteListItem($"  --{option.Name}", option.Description);
                            }
                            else
                            {
                                output.WriteListItem($"  --{option.Name} | -{option.Shortcut}", option.Description);
                            }
                        }
                        output.WriteLine();
                        output.WriteLine("Subjects:");
                        foreach (var subject in subjectRegistry.GetSubjects().Values)
                        {
                            if (subject.GetContextName() == contextName)
                            {
                                output.WriteListItem(subject.GetSubjectName(), subject.GetDescription());
                            }
                        }
                    }
                    else
                    {
                        output.WriteLine($"Command {commandName} does not exist in context {contextName}");
                    }
                }
                else
                {
                    output.WriteLine($"Context {contextName} does not exist");
                }
            }
        }
    }
}
