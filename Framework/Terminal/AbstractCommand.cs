﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System;
using VRage.Collections;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ObjectBuilders.Definitions;
using VRage.Game;
using VRage;
using VRageMath;

namespace IngameScript
{
    partial class Program
    {
        public abstract class Framework_Terminal_AbstractCommand : Framework_Terminal_ICommand
        {
            private string description;
            private List<Framework_Terminal_Data_CommandArgument> arguments = new List<Framework_Terminal_Data_CommandArgument>();
            private List<Framework_Terminal_Data_CommandOption> options = new List<Framework_Terminal_Data_CommandOption>();

            public Framework_Terminal_AbstractCommand()
            {
                AddOption("help", "h", "Show usage");
                AddOption("quiet", "q", "Do not show output");
            }

            public abstract string GetName();

            public abstract string GetContextName();

            public abstract int Execute(Framework_Terminal_ISubject subject, Framework_Terminal_Input input, Framework_Terminal_Output output);

            public string GetDescription()
            {
                return description;
            }

            protected void SetDescription(string description)
            {
                this.description = description;
            }

            public List<Framework_Terminal_Data_CommandArgument> GetArguments()
            {
                return arguments;
            }

            public List<Framework_Terminal_Data_CommandOption> GetOptions()
            {
                return options;
            }

            protected void AddArgument(string name, bool isOptional = false, string description = null)
            {
                var argument = new Framework_Terminal_Data_CommandArgument
                {
                    Name = name,
                    IsOptional = isOptional,
                    Description = description
                };

                arguments.Add(argument);
            }

            protected void AddOption(string name, string shortcut = null, string description = null)
            {
                var option = new Framework_Terminal_Data_CommandOption
                {
                    Name = name,
                    Shortcut = shortcut,
                    Description = description
                };

                options.Add(option);
            }

            /// <summary>
            /// Use to get command service avoiding circular dependency
            /// Do not call in constructors
            /// </summary>
            protected Framework_Terminal_CommandService GetCommandService()
            {
                return ObjectManager.GetObject<Framework_Terminal_CommandService>();
            }

            /// <summary>
            /// Use to get command registry avoiding circular dependency
            /// Do not call in constructors
            /// </summary>
            protected Framework_Terminal_CommandRegistry GetCommandRegistry()
            {
                return ObjectManager.GetObject<Framework_Terminal_CommandRegistry>();
            }
        }
    }
}
