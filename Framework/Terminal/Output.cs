﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System;
using VRage.Collections;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ObjectBuilders.Definitions;
using VRage.Game;
using VRage;
using VRageMath;

namespace IngameScript
{
    partial class Program
    {
        public class Framework_Terminal_Output
        {
            public enum WriteMode
            {
                Normal,
                Quiet
            }

            private string buffer = "";
            private WriteMode writeMode = WriteMode.Normal;

            public void WriteFrame(string value)
            {
                buffer += "================\n";
                buffer += $"{value}\n";
                buffer += "================\n";
            }

            public void WriteLine(string value = null)
            {
                if (writeMode == WriteMode.Quiet)
                {
                    return;
                }

                buffer += $"{value}\n";
            }

            public void WriteList(List<string> values)
            {
                if (writeMode == WriteMode.Quiet)
                {
                    return;
                }

                foreach (var value in values)
                {
                    buffer += $"  {value}\n";
                }
            }

            public void WriteListItem(string value, string comment = null)
            {
                if (writeMode == WriteMode.Quiet)
                {
                    return;
                }

                if (string.IsNullOrEmpty(comment))
                {
                    buffer += $"  {value}\n";
                }
                else
                {
                    buffer += $"  {value}  {comment}\n";
                }
            }

            public void WriteListSubitem(string value, string comment = null)
            {
                if (writeMode == WriteMode.Quiet)
                {
                    return;
                }

                if (string.IsNullOrEmpty(comment))
                {
                    buffer += $"    {value}\n";
                }
                else
                {
                    buffer += $"    {value}  {comment}\n";
                }
            }

            public void ClearBuffer()
            {
                if (writeMode == WriteMode.Quiet)
                {
                    return;
                }

                buffer = "";
            }

            public string GetBuffer()
            {
                return buffer;
            }

            public WriteMode GetWriteMode()
            {
                return writeMode;
            }

            public void SetWriteMode(WriteMode writeMode)
            {
                this.writeMode = writeMode;
            }
        }
    }
}
