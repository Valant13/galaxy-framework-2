﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System;
using VRage.Collections;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ObjectBuilders.Definitions;
using VRage.Game;
using VRage;
using VRageMath;

namespace IngameScript
{
    partial class Program
    {
        public class Framework_Terminal_CommandService
        {
            private Framework_Terminal_Output output = new Framework_Terminal_Output();
            private Framework_Terminal_CommandRegistry commandRegistry;
            private Framework_Terminal_SubjectRegistry subjectRegistry;

            public Framework_Terminal_CommandService()
            {
                commandRegistry = ObjectManager.GetObject<Framework_Terminal_CommandRegistry>();
                subjectRegistry = ObjectManager.GetObject<Framework_Terminal_SubjectRegistry>();
            }

            public int ExecuteCommand(Framework_Terminal_Data_CommandData commandData)
            {
                Framework_Terminal_Input input;
                try
                {
                    input = ConvertCommandDataToInput(commandData);
                }
                catch (Framework_Terminal_Data_CommandException exception)
                {
                    output.WriteLine(exception.Message);

                    return exception.ExitCode;
                }

                var context = commandRegistry.GetContext(input.GetContextName());
                var command = context.Commands[input.GetName()];
                var subject = subjectRegistry.GetSubject(input.GetSubjectName());

                int exitCode;

                if (input.HasOption("quiet"))
                {
                    var oldWriteMode = output.GetWriteMode();
                    output.SetWriteMode(Framework_Terminal_Output.WriteMode.Quiet);

                    exitCode = command.Execute(subject, input, output);

                    output.SetWriteMode(oldWriteMode);
                }
                else
                {
                    exitCode = command.Execute(subject, input, output);
                }

                return exitCode;
            }

            public int ExecuteCommands(List<Framework_Terminal_Data_CommandData> commandDatum)
            {
                foreach (var commandData in commandDatum)
                {
                    var exitCode = ExecuteCommand(commandData);
                    if (exitCode != Framework_Terminal_Data_ExitCode.Success)
                    {
                        return exitCode;
                    }
                }

                return Framework_Terminal_Data_ExitCode.Success;
            }

            public int ExecuteCommandsByString(string commandString)
            {
                var commands = SplitCommandString(commandString);

                foreach (var command in commands)
                {
                    Framework_Terminal_Data_CommandData commandData;
                    try
                    {
                        commandData = ParseCommandString(command);
                    }
                    catch (Framework_Terminal_Data_CommandException exception)
                    {
                        output.WriteLine(exception.Message);

                        return exception.ExitCode;
                    }

                    var exitCode = ExecuteCommand(commandData);
                    if (exitCode != Framework_Terminal_Data_ExitCode.Success)
                    {
                        return exitCode;
                    }
                }

                return Framework_Terminal_Data_ExitCode.Success;
            }

            public void ClearOutputBuffer()
            {
                output.SetWriteMode(Framework_Terminal_Output.WriteMode.Normal);
                output.ClearBuffer();
            }

            public string GetOutputBuffer()
            {
                return output.GetBuffer();
            }

            private Framework_Terminal_Input CreateHelpInputForCommand(string contextName, string commandName)
            {
                var input = new Framework_Terminal_Input();
                input.SetName("help");
                input.SetContextName("this");
                input.SetSubjectName("this");
                input.SetArguments(new Association<string>() { { "context", contextName }, { "command", commandName } });

                return input;
            }

            private Framework_Terminal_Input ConvertCommandDataToInput(Framework_Terminal_Data_CommandData commandData)
            {
                var subjectName = commandData.Subject;

                if (!subjectRegistry.HasSubject(subjectName))
                {
                    throw new Framework_Terminal_Data_CommandException(
                        $"Subject {subjectName} does not exist",
                        Framework_Terminal_Data_ExitCode.NotExistError);
                }

                var subject = subjectRegistry.GetSubject(subjectName);
                var contextName = subject.GetContextName();

                if (!commandRegistry.HasContext(contextName))
                {
                    throw new Framework_Terminal_Data_CommandException(
                        $"Context {contextName} does not exist",
                        Framework_Terminal_Data_ExitCode.NotExistError);
                }

                var context = commandRegistry.GetContext(contextName);
                var commandName = commandData.Command;

                if (!context.Commands.ContainsKey(commandName))
                {
                    throw new Framework_Terminal_Data_CommandException(
                        $"Command {commandName} does not exist in context {contextName}",
                        Framework_Terminal_Data_ExitCode.NotExistError);
                }

                var command = context.Commands[commandName];

                var argumentValues = commandData.Arguments;
                var argumentDefinitions = command.GetArguments();
                var arguments = new Association<string>();
                for (int i = 0; i < argumentDefinitions.Count; i++)
                {
                    var argumentDefinition = argumentDefinitions[i];

                    if (argumentValues.Count >= i + 1)
                    {
                        arguments[argumentDefinition.Name] = argumentValues[i];
                    }
                    else
                    {
                        if (!argumentDefinition.IsOptional)
                        {
                            throw new Framework_Terminal_Data_CommandException($"Argument {argumentDefinition.Name} is missing");
                        }
                    }
                }

                var optionNameValues = commandData.Options;
                var optionShortcutValues = commandData.OptionShortcuts;
                var optionDefinitions = command.GetOptions();
                var options = new Association<string>();
                foreach (var optionDefinition in optionDefinitions)
                {
                    if (optionNameValues.ContainsKey(optionDefinition.Name))
                    {
                        options[optionDefinition.Name] = optionNameValues[optionDefinition.Name];
                    }
                    else if (optionDefinition.Shortcut != null && optionShortcutValues.ContainsKey(optionDefinition.Shortcut))
                    {
                        options[optionDefinition.Name] = optionShortcutValues[optionDefinition.Shortcut];
                    }
                }

                var input = new Framework_Terminal_Input(commandName, contextName, subjectName, arguments, options);

                if (input.HasOption("help"))
                {
                    return CreateHelpInputForCommand(contextName, commandName);
                }

                return input;
            }

            private List<string> SplitCommandString(string commandString)
            {
                return commandString.Split(new string[] { "&&" }, StringSplitOptions.RemoveEmptyEntries).ToList();
            }

            private Framework_Terminal_Data_CommandData ParseCommandString(string commandString)
            {
                var words = commandString.Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries).ToList();
                if (words.Count < 2)
                {
                    throw new Framework_Terminal_Data_CommandException("No command entered", Framework_Terminal_Data_ExitCode.MisuseError);
                }

                var subject = words[0];
                var command = words[1];

                var arguments = new List<string>();
                var options = new Association<string>();
                var optionShortcuts = new Association<string>();

                for (int i = 2; i < words.Count; i++)
                {
                    var word = words[i];
                    if (word.IndexOf("--") == 0)
                    {
                        var optionPair = word.Split(new string[] { "=" }, StringSplitOptions.RemoveEmptyEntries).ToList();
                        var optionName = optionPair[0].Substring(2);
                        string optionValue = null;

                        if (optionPair.Count == 2)
                        {
                            optionValue = optionPair[1];
                        }

                        options[optionName] = optionValue;
                    }
                    else if (word.IndexOf("-") == 0)
                    {
                        var optionPair = word.Split(new string[] { "=" }, StringSplitOptions.RemoveEmptyEntries).ToList();
                        var optionShortcut = optionPair[0].Substring(1);
                        string optionValue = null;

                        if (optionPair.Count == 2)
                        {
                            optionValue = optionPair[1];
                        }

                        optionShortcuts[optionShortcut] = optionValue;
                    }
                    else
                    {
                        arguments.Add(word);
                    }
                }

                return new Framework_Terminal_Data_CommandData()
                {
                    Command = command,
                    Subject = subject,
                    Arguments = arguments,
                    Options = options,
                    OptionShortcuts = optionShortcuts
                };
            }
        }
    }
}
