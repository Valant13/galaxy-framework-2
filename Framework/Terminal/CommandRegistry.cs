﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System;
using VRage.Collections;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ObjectBuilders.Definitions;
using VRage.Game;
using VRage;
using VRageMath;

namespace IngameScript
{
    partial class Program
    {
        public class Framework_Terminal_CommandRegistry
        {
            private Association<Framework_Terminal_Data_Context> contexts = new Association<Framework_Terminal_Data_Context>();

            public Framework_Terminal_CommandRegistry(Association<Framework_Terminal_ICommand> commands)
            {
                foreach (var command in commands.Values)
                {
                    var commandName = command.GetName();
                    var contextName = command.GetContextName();

                    if (!contexts.ContainsKey(contextName))
                    {
                        contexts[contextName] = new Framework_Terminal_Data_Context()
                        {
                            Name = contextName
                        };
                    }

                    contexts[contextName].Commands[commandName] = command;
                }
            }

            public Association<Framework_Terminal_Data_Context> GetContexts()
            {
                return contexts.ToAssociation<Framework_Terminal_Data_Context>();
            }

            public Framework_Terminal_Data_Context GetContext(string contextName)
            {
                return contexts[contextName];
            }

            public bool HasContext(string contextName)
            {
                return contexts.ContainsKey(contextName);
            }
        }
    }
}
