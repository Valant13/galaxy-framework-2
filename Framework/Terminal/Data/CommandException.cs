﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System;
using VRage.Collections;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ObjectBuilders.Definitions;
using VRage.Game;
using VRage;
using VRageMath;

namespace IngameScript
{
    partial class Program
    {
        public class Framework_Terminal_Data_CommandException : Exception
        {
            public int ExitCode { get; private set; } = Framework_Terminal_Data_ExitCode.GeneralError;

            public Framework_Terminal_Data_CommandException()
            { }

            public Framework_Terminal_Data_CommandException(string message)
                : base(message)
            { }

            public Framework_Terminal_Data_CommandException(string message, int exitCode)
                : base(message)
            {
                ExitCode = exitCode;
            }

            public Framework_Terminal_Data_CommandException(string message, int exitCode, Exception innerException)
                : base(message, innerException)
            {
                ExitCode = exitCode;
            }
        }
    }
}
