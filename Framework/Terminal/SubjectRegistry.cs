﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System;
using VRage.Collections;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ObjectBuilders.Definitions;
using VRage.Game;
using VRage;
using VRageMath;

namespace IngameScript
{
    partial class Program
    {
        public class Framework_Terminal_SubjectRegistry
        {
            private Association<Framework_Terminal_ISubject> subjects = new Association<Framework_Terminal_ISubject>();
            private Framework_Utility_HashGenerator hashGenerator;

            public Framework_Terminal_SubjectRegistry()
            {
                hashGenerator = ObjectManager.GetObject<Framework_Utility_HashGenerator>();
            }

            public void RegisterSubject(Framework_Terminal_ISubject subject)
            {
                var subjectName = subject.GetSubjectName();

                if (string.IsNullOrEmpty(subjectName))
                {
                    throw new Exception("Subject name is empty");
                }

                if (subjects.ContainsKey(subjectName))
                {
                    throw new Exception($"Subject with name {subjectName} already exists");
                }

                subjects[subjectName] = subject;
            }

            public void UnregisterSubject(string subjectName)
            {
                subjects.Remove(subjectName);
            }

            public string GenerateSubjectName()
            {
                string subjectName;
                do
                {
                    subjectName = hashGenerator.GenerateRandomHash();
                } while (subjects.Keys.Contains(subjectName));

                return subjectName;
            }

            public Association<Framework_Terminal_ISubject> GetSubjects()
            {
                return subjects.ToAssociation<Framework_Terminal_ISubject>();
            }

            public Framework_Terminal_ISubject GetSubject(string subjectName)
            {
                return subjects[subjectName];
            }

            public bool HasSubject(string subjectName)
            {
                return subjects.ContainsKey(subjectName);
            }
        }
    }
}
