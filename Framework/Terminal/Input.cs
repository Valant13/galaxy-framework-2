﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System;
using VRage.Collections;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ObjectBuilders.Definitions;
using VRage.Game;
using VRage;
using VRageMath;

namespace IngameScript
{
    partial class Program
    {
        public class Framework_Terminal_Input
        {
            private string name;
            private string contextName;
            private string subjectName;
            private Association<string> arguments = new Association<string>();
            private Association<string> options = new Association<string>();

            public Framework_Terminal_Input()
            {}

            public Framework_Terminal_Input(string name, string contextName, string subjectName, Association<string> arguments, Association<string> options)
            {
                this.name = name;
                this.contextName = contextName;
                this.subjectName = subjectName;
                this.arguments = arguments;
                this.options = options;
            }

            public string GetName()
            {
                return name;
            }

            public void SetName(string name)
            {
                this.name = name;
            }

            public string GetContextName()
            {
                return contextName;
            }

            public void SetContextName(string contextName)
            {
                this.contextName = contextName;
            }

            public string GetSubjectName()
            {
                return subjectName;
            }

            public void SetSubjectName(string subjectName)
            {
                this.subjectName = subjectName;
            }

            public Association<string> GetArguments()
            {
                return arguments;
            }

            public void SetArguments(Association<string> arguments)
            {
                this.arguments = arguments;
            }

            public string GetArgument(string argumentName)
            {
                return arguments[argumentName];
            }

            public bool HasArgument(string argumentName)
            {
                return arguments.ContainsKey(argumentName);
            }

            public Association<string> GetOptions()
            {
                return options;
            }

            public void SetOptions(Association<string> options)
            {
                this.options = options;
            }

            public string GetOption(string optionName)
            {
                return options[optionName];
            }

            public bool HasOption(string optionName)
            {
                return options.ContainsKey(optionName);
            }
        }
    }
}
