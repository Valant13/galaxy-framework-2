﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System;
using VRage.Collections;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ObjectBuilders.Definitions;
using VRage.Game;
using VRage;
using VRageMath;

namespace IngameScript
{
    partial class Program
    {
        public class Framework_Terminal_Program_UpdateCommandString : Framework_Bootstrap_IUpdateHandler
        {
            private Framework_Terminal_TerminalService terminalService;
            private Framework_Bootstrap_ProgramService programService;

            public Framework_Terminal_Program_UpdateCommandString()
            {
                terminalService = ObjectManager.GetObject<Framework_Terminal_TerminalService>();
                programService = ObjectManager.GetObject<Framework_Bootstrap_ProgramService>();
            }

            public void Execute()
            {
                var output = terminalService.ProcessInput(programService.Argument);
                programService.Echo(output);
            }

            public UpdateType GetUpdateCondition()
            {
                return UpdateType.Trigger | UpdateType.Terminal;
            }

            public int GetSortOrder()
            {
                return 300;
            }
        }
    }
}
