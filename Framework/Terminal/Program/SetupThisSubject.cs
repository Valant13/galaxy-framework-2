﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System;
using VRage.Collections;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ObjectBuilders.Definitions;
using VRage.Game;
using VRage;
using VRageMath;

namespace IngameScript
{
    partial class Program
    {
        public class Framework_Terminal_Program_SetupThisSubject : Framework_Bootstrap_ISetupHandler
        {
            private Framework_Terminal_SubjectRegistry subjectRegistry;

            public Framework_Terminal_Program_SetupThisSubject()
            {
                subjectRegistry = ObjectManager.GetObject<Framework_Terminal_SubjectRegistry>();
            }

            public void Execute()
            {
                subjectRegistry.RegisterSubject(new Framework_Terminal_ThisSubject());
            }

            public int GetSortOrder()
            {
                return 300;
            }
        }
    }
}
