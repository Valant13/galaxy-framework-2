﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System;
using VRage.Collections;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ObjectBuilders.Definitions;
using VRage.Game;
using VRage;
using VRageMath;

namespace IngameScript
{
    partial class Program
    {
        public static class Framework_DependencyConfig
        {
            public static void Setup()
            {
                var module = ObjectSetup.CreateModule("Framework");

                // Bootstrap
                module.DefineObject<Framework_Bootstrap_ProgramService>();

                module.DefineObject(
                    typeof(Framework_Bootstrap_RuntimeRegistry),
                    new string[] { "SetupHandlers", "UpdateHandlers", "SaveHandlers" },
                    args => new Framework_Bootstrap_RuntimeRegistry(
                        args["SetupHandlers"].ToAssociation<Framework_Bootstrap_ISetupHandler>(),
                        args["UpdateHandlers"].ToAssociation<Framework_Bootstrap_IUpdateHandler>(),
                        args["SaveHandlers"].ToAssociation<Framework_Bootstrap_ISaveHandler>()))
                    .AddArgumentValue("SetupHandlers", "SetupWorldService", typeof(Framework_Utility_Program_SetupWorldService))
                    .AddArgumentValue("SetupHandlers", "SetupBlockCache", typeof(Framework_Actor_Program_SetupBlockCache))
                    .AddArgumentValue("SetupHandlers", "SetupThisSubject", typeof(Framework_Terminal_Program_SetupThisSubject))
                    .AddArgumentValue("UpdateHandlers", "UpdateTimer", typeof(Framework_Utility_Program_UpdateTimer))
                    .AddArgumentValue("UpdateHandlers", "UpdateActors", typeof(Framework_Actor_Program_UpdateActors))
                    .AddArgumentValue("UpdateHandlers", "UpdateExecutionQueue", typeof(Framework_Utility_Program_UpdateExecutionQueue))
                    .AddArgumentValue("UpdateHandlers", "UpdateCacheLock", typeof(Framework_Actor_Program_UpdateCacheLock))
                    .AddArgumentValue("UpdateHandlers", "UpdateCommandString", typeof(Framework_Terminal_Program_UpdateCommandString))
                    .AddArgumentValue("SaveHandlers", "SaveComponents", typeof(Framework_Component_Program_SaveComponents));

                // Component
                module.DefineObject<Framework_Component_ComponentRegistry>();
                module.DefineObject<Framework_Component_Program_SaveComponents>();

                // Actor
                module.DefineObject<Framework_Actor_CacheService>();
                module.DefineObject<Framework_Actor_BlockService>();
                module.DefineObject<Framework_Actor_IniService>();
                module.DefineObject<Framework_Actor_ActorRegistry>();
                module.DefineObject<Framework_Actor_Program_SetupBlockCache>();
                module.DefineObject<Framework_Actor_Program_UpdateActors>();
                module.DefineObject<Framework_Actor_Command_UpdateBlockCache>();
                module.DefineObject<Framework_Actor_Program_UpdateCacheLock>();

                // Terminal
                module.DefineObject<Framework_Terminal_SubjectRegistry>();
                module.DefineObject<Framework_Terminal_TerminalService>();
                module.DefineObject<Framework_Terminal_CommandService>();
                module.DefineObject<Framework_Terminal_Program_SetupThisSubject>();
                module.DefineObject<Framework_Terminal_Program_UpdateCommandString>();
                module.DefineObject<Framework_Terminal_Command_Help>();

                module.DefineObject(
                    typeof(Framework_Terminal_CommandRegistry),
                    new string[] { "Commands" },
                    args => new Framework_Terminal_CommandRegistry(
                        args["Commands"].ToAssociation<Framework_Terminal_ICommand>()))
                    .AddArgumentValue("Commands", "This_Help", typeof(Framework_Terminal_Command_Help))
                    .AddArgumentValue("Commands", "This_UpdateBlockCache", typeof(Framework_Actor_Command_UpdateBlockCache));

                // Utility
                module.DefineObject<Framework_Utility_HashGenerator>();
                module.DefineObject<Framework_Utility_Timer>();
                module.DefineObject<Framework_Utility_Program_UpdateTimer>();
                module.DefineObject<Framework_Utility_WorldService>();
                module.DefineObject<Framework_Utility_Program_SetupWorldService>();
                module.DefineObject<Framework_Utility_Program_UpdateExecutionQueue>();
                module.DefineObject<Framework_Utility_ExecutionQueue>();
            }
        }
    }
}
