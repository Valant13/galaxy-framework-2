﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System;
using VRage.Collections;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ObjectBuilders.Definitions;
using VRage.Game;
using VRage;
using VRageMath;

namespace IngameScript
{
    partial class Program
    {
        public class Framework_Bootstrap_RuntimeRegistry
        {
            public Association<Framework_Bootstrap_ISetupHandler> SetupHandlers { get; private set; } = new Association<Framework_Bootstrap_ISetupHandler>();
            public Association<Framework_Bootstrap_IUpdateHandler> UpdateHandlers { get; private set; } = new Association<Framework_Bootstrap_IUpdateHandler>();
            public Association<Framework_Bootstrap_ISaveHandler> SaveHandlers { get; private set; } = new Association<Framework_Bootstrap_ISaveHandler>();

            public Framework_Bootstrap_RuntimeRegistry(
                Association<Framework_Bootstrap_ISetupHandler> setupHandlers,
                Association<Framework_Bootstrap_IUpdateHandler> updateHandlers,
                Association<Framework_Bootstrap_ISaveHandler> saveHandlers)
            {
                SetupHandlers = setupHandlers;
                UpdateHandlers = updateHandlers;
                SaveHandlers = saveHandlers;
            }
        }
    }
}
