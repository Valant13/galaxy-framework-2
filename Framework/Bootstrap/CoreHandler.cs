﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System;
using VRage.Collections;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ObjectBuilders.Definitions;
using VRage.Game;
using VRage;
using VRageMath;

namespace IngameScript
{
    partial class Program
    {
        public class Framework_Bootstrap_CoreHandler
        {
            private List<Framework_Bootstrap_ISetupHandler> setupHandlers = new List<Framework_Bootstrap_ISetupHandler>();
            private List<Framework_Bootstrap_IUpdateHandler> updateHandlers = new List<Framework_Bootstrap_IUpdateHandler>();
            private List<Framework_Bootstrap_ISaveHandler> saveHandlers = new List<Framework_Bootstrap_ISaveHandler>();
            private Framework_Bootstrap_Data_IConfig config;
            private MyGridProgram program;
            private Framework_Bootstrap_ProgramService programService;
            private Framework_Bootstrap_RuntimeRegistry runtimeRegistry;

            public Framework_Bootstrap_CoreHandler(Framework_Bootstrap_Data_IConfig config, MyGridProgram program)
            {
                programService = ObjectManager.GetObject<Framework_Bootstrap_ProgramService>();
                runtimeRegistry = ObjectManager.GetObject<Framework_Bootstrap_RuntimeRegistry>();

                RegisterHandlers();
                this.config = config;
                this.program = program;
            }

            public void Setup()
            {
                programService.Initialize(config, program);

                program.Runtime.UpdateFrequency = config.GetUpdateFrequency();

                foreach(var handler in setupHandlers)
                {
                    handler.Execute();
                }

                config.SetupComponents();
            }

            public void Update(string argument, UpdateType updateType)
            {
                programService.Update(argument, updateType);

                foreach (var handler in updateHandlers)
                {
                    if ((handler.GetUpdateCondition() & updateType) != 0)
                    {
                        handler.Execute();
                    }
                }
            }

            public string Save()
            {
                foreach (var handler in saveHandlers)
                {
                    handler.Execute();
                }

                return programService.StorageIni.ToString();
            }

            private void RegisterHandlers()
            {
                setupHandlers = runtimeRegistry.SetupHandlers.Values.OrderBy(h => h.GetSortOrder()).ToList();
                updateHandlers = runtimeRegistry.UpdateHandlers.Values.OrderBy(h => h.GetSortOrder()).ToList();
                saveHandlers = runtimeRegistry.SaveHandlers.Values.OrderBy(h => h.GetSortOrder()).ToList();
            }
        }
    }
}
