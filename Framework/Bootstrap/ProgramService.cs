﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System;
using VRage.Collections;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ObjectBuilders.Definitions;
using VRage.Game;
using VRage;
using VRageMath;

namespace IngameScript
{
    partial class Program
    {
        public class Framework_Bootstrap_ProgramService
        {
            private Framework_Bootstrap_Data_IConfig config;
            private MyGridProgram program;
            private bool isInitialized = false;

            public IMyGridTerminalSystem GridTerminalSystem
            {
                get
                {
                    return program.GridTerminalSystem;
                }
            }

            public IMyProgrammableBlock Me
            {
                get
                {
                    return program.Me;
                }
            }

            public IMyGridProgramRuntimeInfo Runtime
            {
                get
                {
                    return program.Runtime;
                }
            }

            public IMyIntergridCommunicationSystem IGC
            {
                get
                {
                    return program.IGC;
                }
            }

            public string AssemblyName
            {
                get
                {
                    return config.GetName();
                }
            }

            public string Argument { get; private set; }
            public UpdateType UpdateType { get; private set; }
            public MyIni MeCustomDataIni { get; private set; }
            public MyIni StorageIni { get; set; }

            public void Initialize(Framework_Bootstrap_Data_IConfig config, MyGridProgram program)
            {
                if (isInitialized)
                {
                    throw new Exception("Already initialized");
                }

                this.config = config;
                this.program = program;

                MeCustomDataIni = GetCustomDataIni(Me.CustomData);
                StorageIni = GetStorageIni(program.Storage);

                isInitialized = true;
            }

            public void Update(string argument, UpdateType updateType)
            {
                Argument = argument;
                UpdateType = updateType;
            }

            public void Echo(string text)
            {
                program.Echo(text);
            }

            private MyIni GetCustomDataIni(string customData)
            {
                var customDataIni = new MyIni();
                if (customDataIni.TryParse(customData))
                {
                    return customDataIni;
                }
                else
                {
                    throw new Exception("Incorrect format of custom data in ME programmable block");
                }
            }

            private MyIni GetStorageIni(string storage)
            {
                var customDataIni = new MyIni();
                if (customDataIni.TryParse(storage))
                {
                    return customDataIni;
                }
                else
                {
                    throw new Exception("Incorrect format of program storage");
                }
            }
        }
    }
}
