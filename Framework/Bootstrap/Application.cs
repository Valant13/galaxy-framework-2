﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System;
using VRage.Collections;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ObjectBuilders.Definitions;
using VRage.Game;
using VRage;
using VRageMath;

namespace IngameScript
{
    partial class Program
    {
        public class Framework_Bootstrap_Application
        {
            private Framework_Bootstrap_CoreHandler coreHandler;

            public Framework_Bootstrap_Application(Framework_Bootstrap_Data_IConfig config, MyGridProgram program)
            {
                InitializeObjectManager(config);

                coreHandler = new Framework_Bootstrap_CoreHandler(config, program);

                coreHandler.Setup();
            }

            public void Update(string argument, UpdateType updateType)
            {
                coreHandler.Update(argument, updateType);
            }

            public string Save()
            {
                return coreHandler.Save();
            }

            private void InitializeObjectManager(Framework_Bootstrap_Data_IConfig config)
            {
                config.SetupDependencies();

                ObjectManager.Initialize(ObjectSetup.GetConfig());
            }
        }
    }
}
