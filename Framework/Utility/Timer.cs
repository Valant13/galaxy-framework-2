﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System;
using VRage.Collections;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ObjectBuilders.Definitions;
using VRage.Game;
using VRage;
using VRageMath;

namespace IngameScript
{
    partial class Program
    {
        public class Framework_Utility_Timer
        {
            private const int ticksPerSecond = 60;

            private long tickTimeNow = 0;
            private Association<Framework_Utility_Data_TimerItem> timeoutItems = new Association<Framework_Utility_Data_TimerItem>();
            private Association<Framework_Utility_Data_TimerItem> intervalItems = new Association<Framework_Utility_Data_TimerItem>();
            private Framework_Utility_HashGenerator hashGenerator;
            private Framework_Bootstrap_ProgramService programService;

            public Framework_Utility_Timer()
            {
                hashGenerator = ObjectManager.GetObject<Framework_Utility_HashGenerator>();
                programService = ObjectManager.GetObject<Framework_Bootstrap_ProgramService>();
            }

            public string SetTimeout(float delay, Framework_Utility_Data_TimerItem.TimerCallback callback)
            {
                var item = new Framework_Utility_Data_TimerItem()
                {
                    Id = GenerateTimeoutItemId(),
                    Callback = callback,
                    Delay = delay,
                    ExecutionTime = AddSeconds(tickTimeNow, delay)
                };

                timeoutItems[item.Id] = item;

                return item.Id;
            }

            public void SetTimeout(float delay, string itemId)
            {
                if (!timeoutItems.ContainsKey(itemId))
                {
                    throw new Exception($"Timeout item with id {itemId} does not exist");
                }

                var item = timeoutItems[itemId];

                item.Delay = delay;
                item.ExecutionTime = AddSeconds(tickTimeNow, delay);
            }

            public void ClearTimeout(string itemId)
            {
                if (itemId == null || !timeoutItems.ContainsKey(itemId))
                {
                    throw new Exception($"Timeout with id {itemId} does not exist");
                }

                timeoutItems.Remove(itemId);
            }

            public string SetInterval(float delay, Framework_Utility_Data_TimerItem.TimerCallback callback)
            {
                var item = new Framework_Utility_Data_TimerItem()
                {
                    Id = GenerateIntervalItemId(),
                    Callback = callback,
                    Delay = delay,
                    ExecutionTime = AddSeconds(tickTimeNow, delay)
                };

                intervalItems[item.Id] = item;

                return item.Id;
            }

            public void ClearInterval(string itemId)
            {
                if (itemId == null || !intervalItems.ContainsKey(itemId))
                {
                    throw new Exception($"Interval with id {itemId} does not exist");
                }

                intervalItems.Remove(itemId);
            }

            public void Update()
            {
                UpdateTimeoutItems();
                UpdateIntervalItems();

                UpdateTickTime();
            }

            public long GetTickTimeNow()
            {
                return tickTimeNow;
            }

            public float GetTimeNow()
            {
                return (float)tickTimeNow / ticksPerSecond;
            }

            private void UpdateTimeoutItems()
            {
                var itemsCopy = timeoutItems.Values.ToList();

                var executedItems = new List<Framework_Utility_Data_TimerItem>();
                foreach (var item in itemsCopy)
                {
                    if (!timeoutItems.ContainsValue(item))
                    {
                        continue;
                    }

                    if (tickTimeNow > item.ExecutionTime)
                    {
                        item.Callback();
                        executedItems.Add(item);
                    }
                }

                foreach (var item in executedItems)
                {
                    timeoutItems.Remove(item.Id);
                }
            }

            private void UpdateIntervalItems()
            {
                var itemsCopy = intervalItems.Values.ToList();

                foreach (var item in itemsCopy)
                {
                    if (!intervalItems.ContainsValue(item))
                    {
                        continue;
                    }

                    if (tickTimeNow > item.ExecutionTime)
                    {
                        item.Callback();
                        item.ExecutionTime = AddSeconds(tickTimeNow, item.Delay);
                    }
                }
            }

            private void UpdateTickTime()
            {
                var updateType = programService.UpdateType;

                if ((updateType & UpdateType.Update1) != 0)
                {
                    tickTimeNow += 1;
                }
                else if ((updateType & UpdateType.Update10) != 0)
                {
                    tickTimeNow += 10;
                }
                else if ((updateType & UpdateType.Update100) != 0)
                {
                    tickTimeNow += 100;
                }
            }

            private long AddSeconds(long tickTime, float seconds)
            {
                return (long)(tickTime + seconds * ticksPerSecond);
            }

            private string GenerateTimeoutItemId()
            {
                string id;
                do
                {
                    id = hashGenerator.GenerateRandomHash();
                } while (timeoutItems.Keys.Contains(id));

                return id;
            }

            private string GenerateIntervalItemId()
            {
                string id;
                do
                {
                    id = hashGenerator.GenerateRandomHash();
                } while (intervalItems.Keys.Contains(id));

                return id;
            }
        }
    }
}
