﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System;
using VRage.Collections;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ObjectBuilders.Definitions;
using VRage.Game;
using VRage;
using VRageMath;

namespace IngameScript
{
    partial class Program
    {
        public class Framework_Utility_Data_TimerItem
        {
            public delegate void TimerCallback();

            public string Id { get; set; }

            public TimerCallback Callback { get; set; }

            /// <summary>
            /// Delay in seconds
            /// </summary>
            public float Delay { get; set; }

            /// <summary>
            /// Execution tick time
            /// </summary>
            public long ExecutionTime { get; set; }
        }
    }
}
