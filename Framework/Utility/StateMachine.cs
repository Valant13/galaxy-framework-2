﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VRage;
using VRage.Collections;
using VRage.Game;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ObjectBuilders.Definitions;
using VRageMath;

namespace IngameScript
{
    partial class Program
    {
        public class Framework_Utility_StateMachine<T> where T : Framework_Utility_IState
        {
            private T currentState;
            private Association<T> states = new Association<T>();
            private object context;

            public Framework_Utility_StateMachine(object context)
            {
                this.context = context;
            }

            public Framework_Utility_StateMachine<T> AddState<S>() where S : T, new()
            {
                var state = new S();
                state.SetStateMachine(this);
                state.SetContext(context);

                states[typeof(S).Name] = state;

                return this;
            }

            public Framework_Utility_StateMachine<T> SetInitialState<S>() where S : T
            {
                ChangeState<S>();

                return this;
            }

            public void ChangeState<S>() where S : T
            {
                var typeName = typeof(S).Name;

                if (!states.ContainsKey(typeName))
                {
                    throw new Exception($"State of type {typeName} does not exist");
                }

                if (currentState != null)
                {
                    currentState.Exit();
                }

                currentState = states[typeName];

                currentState.Enter();
            }

            public T GetState()
            {
                return currentState;
            }

            public bool IsState<S>() where S : T
            {
                return currentState != null && currentState.GetType() == typeof(S);
            }
        }
    }
}
