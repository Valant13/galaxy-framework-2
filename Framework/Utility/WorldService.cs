﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System;
using VRage.Collections;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ObjectBuilders.Definitions;
using VRage.Game;
using VRage;
using VRageMath;

namespace IngameScript
{
    partial class Program
    {
        public class Framework_Utility_WorldService
        {
            public enum Framework_World_Data_GameMode
            {
                Creative,
                Survival
            }

            public const string WorldSettingsSection = "World";
            public const string GameModeKey = "GameMode";
            public const string WeldingSpeedKey = "WeldingSpeed";

            private Framework_Bootstrap_ProgramService programService;

            public Framework_World_Data_GameMode GameMode { get; private set; }
            public float WeldingSpeed { get; private set; }

            public Framework_Utility_WorldService()
            {
                programService = ObjectManager.GetObject<Framework_Bootstrap_ProgramService>();
            }

            public void Initialize()
            {
                GameMode = GetGameMode();
                WeldingSpeed = GetWeldingSpeed();
            }

            private Framework_World_Data_GameMode GetGameMode()
            {
                var gameModeKey = programService.MeCustomDataIni.Get(WorldSettingsSection, GameModeKey);
                if (!gameModeKey.IsEmpty)
                {
                    var gameMode = gameModeKey.ToString();

                    if (gameMode == "Survival")
                    {
                        return Framework_World_Data_GameMode.Survival;
                    }
                }

                return Framework_World_Data_GameMode.Creative;
            }

            private float GetWeldingSpeed()
            {
                if (GetGameMode() == Framework_World_Data_GameMode.Survival)
                {
                    var weldingSpeedKey = programService.MeCustomDataIni.Get(WorldSettingsSection, WeldingSpeedKey);
                    if (!weldingSpeedKey.IsEmpty)
                    {
                        var weldingSpeed = weldingSpeedKey.ToString();

                        switch (weldingSpeed)
                        {
                            case "Realistic":
                                return 1;
                            case "0.5x":
                                return 0.5F;
                            case "2x":
                                return 1.9F;
                            case "5x":
                                return 4.5F;
                        }
                    }
                }

                return 30;
            }
        }
    }
}
