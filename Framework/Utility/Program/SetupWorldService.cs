﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System;
using VRage.Collections;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ObjectBuilders.Definitions;
using VRage.Game;
using VRage;
using VRageMath;

namespace IngameScript
{
    partial class Program
    {
        public class Framework_Utility_Program_SetupWorldService : Framework_Bootstrap_ISetupHandler
        {
            private Framework_Utility_WorldService worldService;

            public Framework_Utility_Program_SetupWorldService()
            {
                worldService = ObjectManager.GetObject<Framework_Utility_WorldService>();
            }

            public void Execute()
            {
                worldService.Initialize();
            }

            public int GetSortOrder()
            {
                return 100;
            }
        }
    }
}
