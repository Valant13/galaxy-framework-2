﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VRage;
using VRage.Collections;
using VRage.Game;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ObjectBuilders.Definitions;
using VRageMath;

namespace IngameScript
{
    partial class Program
    {
        public class Framework_Utility_ExecutionQueue
        {
            public delegate void ExecutionCallback();

            private Queue<ExecutionCallback> executions = new Queue<ExecutionCallback>();

            public void AddExecution(ExecutionCallback execution)
            {
                executions.Enqueue(execution);
            }

            public void Update()
            {
                if (executions.Count > 0)
                {
                    executions.Dequeue()();
                }
            }
        }
    }
}
