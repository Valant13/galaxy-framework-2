﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VRage;
using VRage.Collections;
using VRage.Game;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ObjectBuilders.Definitions;
using VRageMath;

namespace IngameScript
{
    partial class Program
    {
        public class Framework_Actor_SearchCriteriaBuilder
        {
            private Framework_Actor_Data_SearchCriteria searchCriteria = new Framework_Actor_Data_SearchCriteria();

            public Framework_Actor_SearchCriteriaBuilder SetName(string name)
            {
                searchCriteria.Name = name;

                return this;
            }

            public Framework_Actor_SearchCriteriaBuilder SetNameSubstring(string nameSubstring)
            {
                searchCriteria.NameSubstring = nameSubstring;

                return this;
            }

            public Framework_Actor_SearchCriteriaBuilder SetEntityId(long? entityId)
            {
                searchCriteria.EntityId = entityId;

                return this;
            }

            public Framework_Actor_SearchCriteriaBuilder SetType(Type type)
            {
                searchCriteria.Type = type;

                return this;
            }

            public Framework_Actor_SearchCriteriaBuilder SetType<T>()
            {
                return SetType(typeof(T));
            }

            public Framework_Actor_SearchCriteriaBuilder SetIsMe(bool? isMe)
            {
                searchCriteria.IsMe = isMe;

                return this;
            }

            public Framework_Actor_SearchCriteriaBuilder SetBoundingBox(BoundingBoxI? boundingBox)
            {
                searchCriteria.BoundingBox = boundingBox;

                return this;
            }

            public Framework_Actor_SearchCriteriaBuilder SetCustomClass(string customClass)
            {
                searchCriteria.CustomClass = customClass;

                return this;
            }

            public Framework_Actor_SearchCriteriaBuilder SetCustomId(string customId)
            {
                searchCriteria.CustomId = customId;

                return this;
            }

            public Framework_Actor_SearchCriteriaBuilder SetCustomGrid(string customGrid)
            {
                searchCriteria.CustomGrid = customGrid;

                return this;
            }

            public Framework_Actor_SearchCriteriaBuilder AddCustomAttribute(string attributeName, string attributeValue)
            {
                searchCriteria.CustomAttributes.Add(attributeName, attributeValue);

                return this;
            }

            public Framework_Actor_SearchCriteriaBuilder Reset()
            {
                searchCriteria = new Framework_Actor_Data_SearchCriteria();

                return this;
            }

            public Framework_Actor_Data_SearchCriteria Build()
            {
                var result = searchCriteria;
                Reset();

                return result;
            }
        }
    }
}
