﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System;
using VRage.Collections;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ObjectBuilders.Definitions;
using VRage.Game;
using VRage;
using VRageMath;

namespace IngameScript
{
    partial class Program
    {
        public class Framework_Actor_MovementObserver : Framework_Actor_AbstractSingleActor<IMyTerminalBlock>
        {
            public event EventHandler PositionChanged;
            public event EventHandler WorldMatrixChanged;
            public event EventHandler VelocityChanged;
            public event EventHandler AccelerationChanged;

            private Vector3D? position;
            private Vector3D? velocity;
            private Vector3D? acceleration;
            private MatrixD? worldMatrix;
            private float time;
            private float? deltaTime;

            private Framework_Utility_Timer timer;

            public Framework_Actor_MovementObserver(
                Framework_Actor_Data_SearchCriteria searchCriteria,
                bool useHardStrategy = false,
                UpdateType updateCondition = UpdateType.Update10 | UpdateType.Update100) : base(searchCriteria, useHardStrategy)
            {
                timer = ObjectManager.GetObject<Framework_Utility_Timer>();

                SetUpdateCondition(updateCondition);
            }

            public Vector3D? GetPosition()
            {
                return position;
            }

            /// <summary>
            /// Can be null for functional block
            /// </summary>
            public Vector3D? GetVelocity()
            {
                return velocity;
            }

            /// <summary>
            /// Can be null for functional block
            /// </summary>
            public Vector3D? GetAcceleration()
            {
                return acceleration;
            }

            public MatrixD? GetWorldMatrix()
            {
                return worldMatrix;
            }

            /// <summary>
            /// Can be null for functional block
            /// </summary>
            public float? GetDeltaTime()
            {
                return deltaTime;
            }

            protected override void Update()
            {
                var previousTime = time;

                var previousPosition = position;
                var previousVelocity = velocity;
                var previousAcceleration = acceleration;
                var previousWorldMatrix = worldMatrix;

                // Sometimes while destroying the block SE sets its position to zero, but does not change its functionality

                if (HasBlock() && GetBlock().IsFunctional && !GetBlock().GetPosition().IsZero())
                {
                    var block = GetBlock();

                    time = timer.GetTimeNow();
                    
                    position = block.GetPosition();
                    if (previousPosition != null)
                    {
                        deltaTime = time - previousTime;

                        velocity = (previousPosition - position) / deltaTime;
                        if (previousVelocity != null)
                        {
                            acceleration = (previousVelocity - velocity) / deltaTime;
                        }
                    }

                    worldMatrix = block.WorldMatrix;
                }
                else
                {
                    position = null;
                    velocity = null;
                    acceleration = null;

                    worldMatrix = null;
                }

                if (position != previousPosition)
                {
                    PositionChanged?.Invoke(this, new EventArgs());
                }

                if (velocity != previousVelocity)
                {
                    VelocityChanged?.Invoke(this, new EventArgs());
                }

                if (acceleration != previousAcceleration)
                {
                    AccelerationChanged?.Invoke(this, new EventArgs());
                }

                if (worldMatrix != previousWorldMatrix)
                {
                    WorldMatrixChanged?.Invoke(this, new EventArgs());
                }
            }
        }
    }
}
