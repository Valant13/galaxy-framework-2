﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System;
using VRage.Collections;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ObjectBuilders.Definitions;
using VRage.Game;
using VRage;
using VRageMath;

namespace IngameScript
{
    partial class Program
    {
        public class Framework_Actor_Data_BlockInfo
        {
            public string Name { get; set; }

            public long EntityId { get; set; }

            public Type Type { get; set; }

            public bool IsMe { get; set; }

            public Vector3I Position { get; set; }

            public List<string> CustomClasses { get; set; } = new List<string>();

            public string CustomId { get; set; }

            public string CustomGrid { get; set; }

            public MyIni CustomDataIni { get; set; }

            public IMyTerminalBlock Block { get; set; }
        }
    }
}
