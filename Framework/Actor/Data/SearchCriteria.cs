﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System;
using VRage.Collections;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ObjectBuilders.Definitions;
using VRage.Game;
using VRage;
using VRageMath;

namespace IngameScript
{
    partial class Program
    {
        public class Framework_Actor_Data_SearchCriteria
        {
            public string Name { get; set; }

            public string NameSubstring { get; set; }

            public long? EntityId { get; set; }

            public Type Type { get; set; }

            public bool? IsMe { get; set; }

            public BoundingBoxI? BoundingBox { get; set; }

            public string CustomClass { get; set; }

            public string CustomId { get; set; }

            public string CustomGrid { get; set; }

            public Association<string> CustomAttributes { get; set; } = new Association<string>();

            public Framework_Actor_Data_SearchCriteria()
            { }

            public Framework_Actor_Data_SearchCriteria(string customClass) : this(customClass, null)
            { }

            public Framework_Actor_Data_SearchCriteria(string customClass, string customGrid)
            {
                CustomClass = customClass;
                CustomGrid = customGrid;
            }
        }
    }
}
