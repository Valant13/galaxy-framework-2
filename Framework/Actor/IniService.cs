﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VRage;
using VRage.Collections;
using VRage.Game;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ObjectBuilders.Definitions;
using VRageMath;

namespace IngameScript
{
    partial class Program
    {
        public class Framework_Actor_IniService
        {
            private Framework_Actor_BlockService blockService;

            public Framework_Actor_IniService()
            {
                blockService = ObjectManager.GetObject<Framework_Actor_BlockService>();
            }

            public MyIni GetBlockCustomDataIni(IMyTerminalBlock block)
            {
                return blockService.GetBlockCustomDataIni(block);
            }

            public string GetString(MyIni ini, string section, string key)
            {
                var iniValue = ini.Get(section, key);

                if (iniValue.IsEmpty)
                {
                    throw new Exception($"Key {key} is missing in section {section}");
                }

                return iniValue.ToString();
            }

            public string GetStringOrDefault(MyIni ini, string section, string key, string defaultValue = null)
            {
                var iniValue = ini.Get(section, key);

                if (iniValue.IsEmpty)
                {
                    return defaultValue;
                }

                return iniValue.ToString();
            }

            public int GetInt(MyIni ini, string section, string key)
            {
                return int.Parse(GetString(ini, section, key));
            }

            public int GetIntOrDefault(MyIni ini, string section, string key, int defaultValue = 0)
            {
                var valueString = GetStringOrDefault(ini, section, key);

                if (valueString == null)
                {
                    return defaultValue;
                }

                return int.Parse(valueString);
            }

            public float GetFloat(MyIni ini, string section, string key)
            {
                return float.Parse(GetString(ini, section, key));
            }

            public float GetFloatOrDefault(MyIni ini, string section, string key, float defaultValue = 0)
            {
                var valueString = GetStringOrDefault(ini, section, key);

                if (valueString == null)
                {
                    return defaultValue;
                }

                return float.Parse(valueString);
            }
        }
    }
}
