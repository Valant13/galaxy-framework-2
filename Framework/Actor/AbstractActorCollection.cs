﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System;
using VRage.Collections;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ObjectBuilders.Definitions;
using VRage.Game;
using VRage;
using VRageMath;

namespace IngameScript
{
    partial class Program
    {
        public abstract class Framework_Actor_AbstractActorCollection<T> : Framework_Actor_AbstractActor, Framework_Actor_IActorCollection<T> where T : IMyTerminalBlock
        {
            private List<T> blocks = new List<T>();

            public Framework_Actor_AbstractActorCollection(Framework_Actor_Data_SearchCriteria searchCriteria, bool useHardStrategy = false) : base(searchCriteria, useHardStrategy)
            { }

            public List<T> GetBlocks()
            {
                return blocks;
            }

            public bool HasBlocks()
            {
                return blocks.Count > 0;
            }

            protected override void UpdateBlocks()
            {
                blocks = GetBlockService().GetBlocks<T>(GetSearchCriteria());
            }
        }
    }
}
