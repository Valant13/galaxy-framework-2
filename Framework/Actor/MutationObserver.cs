﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System;
using VRage.Collections;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ObjectBuilders.Definitions;
using VRage.Game;
using VRage;
using VRageMath;

namespace IngameScript
{
    partial class Program
    {
        public class Framework_Actor_MutationObserver : Framework_Actor_AbstractActorCollection<IMyTerminalBlock>
        {
            public event EventHandler FunctionalityChanged;

            private bool isFunctional;

            public Framework_Actor_MutationObserver(
                Framework_Actor_Data_SearchCriteria searchCriteria,
                bool useHardStrategy = false,
                UpdateType updateCondition = UpdateType.Update10 | UpdateType.Update100) : base(searchCriteria, useHardStrategy)
            {
                SetUpdateCondition(updateCondition);

                isFunctional = IsFunctionalImpl();
            }

            public bool IsFunctional()
            {
                return isFunctional;
            }

            protected override void Update()
            {
                var wasFunctional = isFunctional;
                isFunctional = IsFunctionalImpl();

                if (isFunctional != wasFunctional)
                {
                    FunctionalityChanged?.Invoke(this, new EventArgs());
                }
            }

            private bool IsFunctionalImpl()
            {
                foreach (var block in GetBlocks())
                {
                    if (block.IsFunctional)
                    {
                        return true;
                    }
                }

                return false;
            }
        }
    }
}
