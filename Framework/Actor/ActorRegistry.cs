﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System;
using VRage.Collections;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ObjectBuilders.Definitions;
using VRage.Game;
using VRage;
using VRageMath;

namespace IngameScript
{
    partial class Program
    {
        public class Framework_Actor_ActorRegistry
        {
            private Association<Framework_Actor_Data_ActorInfo> actorInfos = new Association<Framework_Actor_Data_ActorInfo>();
            private Framework_Bootstrap_ProgramService programService;
            private Framework_Utility_HashGenerator hashGenerator;

            public Framework_Actor_ActorRegistry()
            {
                programService = ObjectManager.GetObject<Framework_Bootstrap_ProgramService>();
                hashGenerator = ObjectManager.GetObject<Framework_Utility_HashGenerator>();
            }

            public void RegisterActor(Framework_Actor_Data_ActorInfo actorInfo)
            {
                var actorId = actorInfo.Actor.GetId();

                if (string.IsNullOrEmpty(actorId))
                {
                    throw new Exception("Actor ID is empty");
                }

                if (actorInfos.ContainsKey(actorId))
                {
                    throw new Exception($"Actor with ID {actorId} already exists");
                }

                actorInfos[actorId] = actorInfo;
                actorInfo.UpdateBlocks();
            }

            public void UnregisterActor(string actorId)
            {
                actorInfos.Remove(actorId);
            }

            public string GenerateActorId()
            {
                string actorId;
                do
                {
                    actorId = hashGenerator.GenerateRandomHash();
                } while (actorInfos.Keys.Contains(actorId));

                return actorId;
            }

            public void UpdateActors()
            {
                var updateType = programService.UpdateType;

                var actorInfosCopy = actorInfos.Values.ToList();

                foreach (var actorInfo in actorInfosCopy)
                {
                    if (!actorInfos.ContainsValue(actorInfo))
                    {
                        continue;
                    }

                    if ((updateType & actorInfo.Actor.GetUpdateCondition()) != 0)
                    {
                        actorInfo.Update();
                    }
                }
            }

            public void UpdateBlocks(Framework_Actor_Data_BlockUpdateStrategy strategy = Framework_Actor_Data_BlockUpdateStrategy.Soft)
            {
                if (strategy == Framework_Actor_Data_BlockUpdateStrategy.Soft)
                {
                    foreach (var actorInfo in actorInfos.Values)
                    {
                        if (actorInfo.Actor.GetBlockUpdateStrategy() == Framework_Actor_Data_BlockUpdateStrategy.Soft)
                        {
                            actorInfo.UpdateBlocks();
                        }
                    }
                }
                else
                {
                    foreach (var actorInfo in actorInfos.Values)
                    {
                        actorInfo.UpdateBlocks();
                    }
                }
            }

            public Association<Framework_Actor_IActor> GetActors()
            {
                var actors = new Association<Framework_Actor_IActor>();
                foreach (var keyValuePair in actorInfos)
                {
                    actors[keyValuePair.Key] = keyValuePair.Value.Actor;
                }

                return actors;
            }

            public Framework_Actor_IActor GetActor(string actorId)
            {
                return actorInfos[actorId].Actor;
            }

            public bool HasActor(string actorId)
            {
                return actorInfos.ContainsKey(actorId);
            }
        }
    }
}
