﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System;
using VRage.Collections;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ObjectBuilders.Definitions;
using VRage.Game;
using VRage;
using VRageMath;

namespace IngameScript
{
    partial class Program
    {
        public class Framework_Actor_BlockService
        {
            public const string GeneralSection = "Block";
            public const string CustomClassKey = "Class";
            public const string CustomIdKey = "Id";
            public const string CustomGridKey = "Grid";

            private List<Framework_Actor_Data_BlockInfo> blockCache = new List<Framework_Actor_Data_BlockInfo>();
            private Association<Framework_Actor_Data_BlockInfo> blockCacheById = new Association<Framework_Actor_Data_BlockInfo>();
            private Association<List<Framework_Actor_Data_BlockInfo>> blockCacheByClass = new Association<List<Framework_Actor_Data_BlockInfo>>();
            private Framework_Bootstrap_ProgramService programService;

            public Framework_Actor_BlockService()
            {
                programService = ObjectManager.GetObject<Framework_Bootstrap_ProgramService>();
            }

            public MyIni GetBlockCustomDataIni(IMyTerminalBlock block)
            {
                foreach (var blockInfo in blockCache)
                {
                    if (block == blockInfo.Block)
                    {
                        return blockInfo.CustomDataIni;
                    }
                }

                return null;
            }

            public T GetBlock<T>(Framework_Actor_Data_SearchCriteria searchCriteria = null) where T : IMyTerminalBlock
            {
                return (T)GetBlock(searchCriteria);
            }

            public IMyTerminalBlock GetBlock(Framework_Actor_Data_SearchCriteria searchCriteria = null)
            {
                return GetBlocks(searchCriteria).FirstOrDefault();
            }

            public List<T> GetBlocks<T>(Framework_Actor_Data_SearchCriteria searchCriteria = null) where T : IMyTerminalBlock
            {
                return GetBlocks(searchCriteria).Cast<T>().ToList();
            }

            public List<IMyTerminalBlock> GetBlocks(Framework_Actor_Data_SearchCriteria searchCriteria = null)
            {
                if (searchCriteria == null)
                {
                    searchCriteria = new Framework_Actor_Data_SearchCriteria();
                }

                var blockInfos = GetBlockInfosBySearchCriteria(searchCriteria);

                var blocks = new List<IMyTerminalBlock>();
                foreach (var blockInfo in blockInfos)
                {
                    blocks.Add(blockInfo.Block);
                }

                return blocks;
            }

            public void UpdateBlockCache()
            {
                var blocks = new List<IMyTerminalBlock>();
                programService.GridTerminalSystem.GetBlocks(blocks);

                blockCache.Clear();
                blockCacheById.Clear();
                blockCacheByClass.Clear();

                foreach (var block in blocks)
                {
                    var blockInfo = CreateBlockInfo(block);

                    blockCache.Add(blockInfo);
                    if (blockInfo.CustomId != null)
                    {
                        blockCacheById[blockInfo.CustomId] = blockInfo;
                    }
                    foreach (var customClass in blockInfo.CustomClasses)
                    {
                        if (!blockCacheByClass.ContainsKey(customClass))
                        {
                            blockCacheByClass[customClass] = new List<Framework_Actor_Data_BlockInfo>();
                        }

                        blockCacheByClass[customClass].Add(blockInfo);
                    }
                }
            }

            private Framework_Actor_Data_BlockInfo CreateBlockInfo(IMyTerminalBlock block)
            {
                var blockInfo = new Framework_Actor_Data_BlockInfo()
                {
                    Name = block.CustomName,
                    EntityId = block.EntityId,
                    Type = block.GetType(),
                    IsMe = block == programService.Me,
                    Position = block.Position,
                    Block = block
                };

                var customDataIni = new MyIni();
                if (customDataIni.TryParse(block.CustomData))
                {
                    var customClassesValue = customDataIni.Get(GeneralSection, CustomClassKey);
                    if (!customClassesValue.IsEmpty)
                    {
                        blockInfo.CustomClasses = customClassesValue
                            .ToString()
                            .Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries)
                            .Select(s => s.Trim())
                            .ToList();
                    }

                    var customIdValue = customDataIni.Get(GeneralSection, CustomIdKey);
                    if (!customIdValue.IsEmpty)
                    {
                        blockInfo.CustomId = customIdValue.ToString();
                    }

                    var customGridValue = customDataIni.Get(GeneralSection, CustomGridKey);
                    if (!customGridValue.IsEmpty)
                    {
                        blockInfo.CustomGrid = customGridValue.ToString();
                    }

                    blockInfo.CustomDataIni = customDataIni;
                }

                return blockInfo;
            }

            private List<Framework_Actor_Data_BlockInfo> GetBlockInfosBySearchCriteria(Framework_Actor_Data_SearchCriteria searchCriteria)
            {
                var matchedBlocks = new List<Framework_Actor_Data_BlockInfo>();

                var searchId = searchCriteria.CustomId;
                var searchClass = searchCriteria.CustomClass;

                if (searchId != null)
                {
                    if (blockCacheById.ContainsKey(searchId))
                    {
                        var blockInfo = blockCacheById[searchId];

                        if (IsBlockInfoMatchSearchCriteria(blockInfo, searchCriteria))
                        {
                            matchedBlocks.Add(blockInfo);
                        }
                    }
                }
                else if (searchClass != null)
                {
                    if (blockCacheByClass.ContainsKey(searchClass))
                    {
                        foreach (var blockInfo in blockCacheByClass[searchClass])
                        {
                            if (IsBlockInfoMatchSearchCriteria(blockInfo, searchCriteria))
                            {
                                matchedBlocks.Add(blockInfo);
                            }
                        }
                    }
                }
                else
                {
                    foreach (var blockInfo in blockCache)
                    {
                        if (IsBlockInfoMatchSearchCriteria(blockInfo, searchCriteria))
                        {
                            matchedBlocks.Add(blockInfo);
                        }
                    }
                }

                return matchedBlocks;
            }

            private bool IsBlockInfoMatchSearchCriteria(Framework_Actor_Data_BlockInfo blockInfo, Framework_Actor_Data_SearchCriteria searchCriteria)
            {
                if (searchCriteria.Name != null)
                {
                    if (blockInfo.Name != searchCriteria.Name)
                    {
                        return false;
                    }
                }

                if (searchCriteria.NameSubstring != null)
                {
                    if (!blockInfo.Name.Contains(searchCriteria.NameSubstring))
                    {
                        return false;
                    }
                }

                if (searchCriteria.EntityId != null)
                {
                    if (blockInfo.EntityId != searchCriteria.EntityId)
                    {
                        return false;
                    }
                }

                if (searchCriteria.Type != null)
                {
                    if (blockInfo.Type.Name != searchCriteria.Type.Name.Substring(1))
                    {
                        return false;
                    }
                }

                if (searchCriteria.IsMe != null)
                {
                    if (blockInfo.IsMe != searchCriteria.IsMe)
                    {
                        return false;
                    }
                }

                if (searchCriteria.BoundingBox != null)
                {
                    var containmentType = searchCriteria.BoundingBox?.Contains(blockInfo.Position);

                    if (containmentType == ContainmentType.Disjoint)
                    {
                        return false;
                    }
                }

                if (searchCriteria.CustomClass != null)
                {
                    if (!blockInfo.CustomClasses.Contains(searchCriteria.CustomClass))
                    {
                        return false;
                    }
                }

                if (searchCriteria.CustomId != null)
                {
                    if (blockInfo.CustomId != searchCriteria.CustomId)
                    {
                        return false;
                    }
                }

                if (searchCriteria.CustomGrid != null)
                {
                    if (blockInfo.CustomGrid != searchCriteria.CustomGrid)
                    {
                        return false;
                    }
                }

                foreach (var customAttribute in searchCriteria.CustomAttributes)
                {
                    if (blockInfo.CustomDataIni == null)
                    {
                        return false;
                    }

                    var customDataValue = blockInfo.CustomDataIni.Get(GeneralSection, customAttribute.Key);
                    if (customDataValue.IsEmpty)
                    {
                        return false;
                    }

                    if (customDataValue.ToString() != customAttribute.Value)
                    {
                        return false;
                    }
                }

                return true;
            }
        }
    }
}
