﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System;
using VRage.Collections;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ObjectBuilders.Definitions;
using VRage.Game;
using VRage;
using VRageMath;

namespace IngameScript
{
    partial class Program
    {
        public class Framework_Actor_CacheService
        {
            private Framework_Actor_Data_BlockUpdateStrategy? currentStrategy;
            private Framework_Actor_ActorRegistry actorRegistry;
            private Framework_Actor_BlockService blockService;

            public Framework_Actor_CacheService()
            {
                actorRegistry = ObjectManager.GetObject<Framework_Actor_ActorRegistry>();
                blockService = ObjectManager.GetObject<Framework_Actor_BlockService>();
            }

            public void UpdateBlockCache(Framework_Actor_Data_BlockUpdateStrategy strategy = Framework_Actor_Data_BlockUpdateStrategy.Soft)
            {
                if (currentStrategy.HasValue && strategy <= currentStrategy)
                {
                    return;
                }

                blockService.UpdateBlockCache();
                actorRegistry.UpdateBlocks(strategy);

                currentStrategy = strategy;
            }

            public void Unlock()
            {
                currentStrategy = null;
            }
        }
    }
}
