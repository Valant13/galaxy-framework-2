﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VRage;
using VRage.Collections;
using VRage.Game;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ObjectBuilders.Definitions;
using VRageMath;

namespace IngameScript
{
    partial class Program
    {
        public class Framework_Actor_Program_UpdateCacheLock : Framework_Bootstrap_IUpdateHandler
        {
            private Framework_Actor_CacheService cacheService;

            public Framework_Actor_Program_UpdateCacheLock()
            {
                cacheService = ObjectManager.GetObject<Framework_Actor_CacheService>();
            }

            public void Execute()
            {
                cacheService.Unlock();
            }

            public UpdateType GetUpdateCondition()
            {
                return UpdateType.Update1 | UpdateType.Update10 | UpdateType.Update100;
            }

            public int GetSortOrder()
            {
                return 900;
            }
        }
    }
}
