﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System;
using VRage.Collections;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ObjectBuilders.Definitions;
using VRage.Game;
using VRage;
using VRageMath;

namespace IngameScript
{
    partial class Program
    {
        public class Framework_Actor_Program_SetupBlockCache : Framework_Bootstrap_ISetupHandler
        {
            private Framework_Actor_CacheService cacheService;

            public Framework_Actor_Program_SetupBlockCache()
            {
                cacheService = ObjectManager.GetObject<Framework_Actor_CacheService>();
            }

            public void Execute()
            {
                cacheService.UpdateBlockCache(Framework_Actor_Data_BlockUpdateStrategy.Hard);
            }

            public int GetSortOrder()
            {
                return 200;
            }
        }
    }
}
