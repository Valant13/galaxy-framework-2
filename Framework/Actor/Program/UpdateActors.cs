﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System;
using VRage.Collections;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ObjectBuilders.Definitions;
using VRage.Game;
using VRage;
using VRageMath;

namespace IngameScript
{
    partial class Program
    {
        public class Framework_Actor_Program_UpdateActors : Framework_Bootstrap_IUpdateHandler
        {
            private Framework_Actor_ActorRegistry actorRegistry;

            public Framework_Actor_Program_UpdateActors()
            {
                actorRegistry = ObjectManager.GetObject<Framework_Actor_ActorRegistry>();
            }

            public void Execute()
            {
                actorRegistry.UpdateActors();
            }

            public UpdateType GetUpdateCondition()
            {
                return UpdateType.Update1 | UpdateType.Update10 | UpdateType.Update100;
            }

            public int GetSortOrder()
            {
                return 200;
            }
        }
    }
}
