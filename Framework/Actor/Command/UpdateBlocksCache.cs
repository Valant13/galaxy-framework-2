﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System;
using VRage.Collections;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ObjectBuilders.Definitions;
using VRage.Game;
using VRage;
using VRageMath;

namespace IngameScript
{
    partial class Program
    {
        public class Framework_Actor_Command_UpdateBlockCache : Framework_Terminal_AbstractCommand
        {
            private Framework_Actor_CacheService cacheService;

            public Framework_Actor_Command_UpdateBlockCache()
            {
                cacheService = ObjectManager.GetObject<Framework_Actor_CacheService>();

                SetDescription("Updates block cache in block service and actors");
                AddOption("hard", null, "Use hard strategy");
            }

            public override string GetName()
            {
                return "update-block-cache";
            }

            public override string GetContextName()
            {
                return "this";
            }

            public override int Execute(Framework_Terminal_ISubject subject, Framework_Terminal_Input input, Framework_Terminal_Output output)
            {
                var strategy = Framework_Actor_Data_BlockUpdateStrategy.Soft;
                if (input.HasArgument("hard"))
                {
                    strategy = Framework_Actor_Data_BlockUpdateStrategy.Hard;
                }

                cacheService.UpdateBlockCache(strategy);

                output.WriteLine("Block cache updated");

                return 0;
            }
        }
    }
}
