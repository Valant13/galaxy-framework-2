﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System;
using VRage.Collections;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ObjectBuilders.Definitions;
using VRage.Game;
using VRage;
using VRageMath;

namespace IngameScript
{
    partial class Program
    {
        public abstract class Framework_Actor_AbstractActor : Framework_Actor_IActor
        {
            private UpdateType updateCondition = UpdateType.Update1 | UpdateType.Update10 | UpdateType.Update100;
            private string id;
            private Framework_Actor_Data_SearchCriteria searchCriteria;
            private Framework_Actor_Data_BlockUpdateStrategy blockUpdateStrategy;
            private Framework_Actor_ActorRegistry actorRegistry;
            private Framework_Actor_BlockService blockService;

            public Framework_Actor_AbstractActor(Framework_Actor_Data_SearchCriteria searchCriteria, bool useHardStrategy = false)
            {
                this.searchCriteria = searchCriteria;
                blockUpdateStrategy = useHardStrategy ? Framework_Actor_Data_BlockUpdateStrategy.Hard : Framework_Actor_Data_BlockUpdateStrategy.Soft;
                actorRegistry = ObjectManager.GetObject<Framework_Actor_ActorRegistry>();
                blockService = ObjectManager.GetObject<Framework_Actor_BlockService>();

                Register();
            }

            protected virtual void Cleanup()
            { }

            protected virtual void Update()
            { }

            protected abstract void UpdateBlocks();

            public void Destruct()
            {
                Cleanup();
                Unregister();
            }

            public string GetId()
            {
                return id;
            }

            public UpdateType GetUpdateCondition()
            {
                return updateCondition;
            }

            protected void SetUpdateCondition(UpdateType updateCondition)
            {
                this.updateCondition = updateCondition;
            }

            public Framework_Actor_Data_SearchCriteria GetSearchCriteria()
            {
                return searchCriteria;
            }

            public Framework_Actor_Data_BlockUpdateStrategy GetBlockUpdateStrategy()
            {
                return blockUpdateStrategy;
            }

            protected Framework_Actor_BlockService GetBlockService()
            {
                return blockService;
            }

            private void Register()
            {
                if (string.IsNullOrEmpty(id))
                {
                    id = actorRegistry.GenerateActorId();
                }

                var info = new Framework_Actor_Data_ActorInfo
                {
                    Actor = this,
                    Update = Update,
                    UpdateBlocks = UpdateBlocks
                };

                actorRegistry.RegisterActor(info);
            }

            private void Unregister()
            {
                actorRegistry.UnregisterActor(id);
            }
        }
    }
}
