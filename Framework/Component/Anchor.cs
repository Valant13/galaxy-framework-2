﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System;
using VRage.Collections;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ObjectBuilders.Definitions;
using VRage.Game;
using VRage;
using VRageMath;

namespace IngameScript
{
    partial class Program
    {
        public class Framework_Component_Anchor : Framework_Component_AbstractComponent
        {
            public event EventHandler PositionChanged;
            public event EventHandler VelocityChanged;
            public event EventHandler AccelerationChanged;
            public event EventHandler WorldMatrixChanged;
            public event EventHandler FunctionalityChanged;

            private bool isFunctional;
            private Framework_Actor_MutationObserver mutationObserver;
            private Framework_Actor_MovementObserver movementObserver;

            public Framework_Component_Anchor(
                string id,
                Framework_Actor_Data_SearchCriteria observableSearchCriteria,
                bool useHardStrategy = false) : base(id)
            {
                var updateCondition = UpdateType.Update1 | UpdateType.Update10 | UpdateType.Update100;

                mutationObserver = new Framework_Actor_MutationObserver(observableSearchCriteria, useHardStrategy, updateCondition);
                movementObserver = new Framework_Actor_MovementObserver(observableSearchCriteria, useHardStrategy, updateCondition);

                movementObserver.PositionChanged += MovementObserver_PositionChanged;
                movementObserver.VelocityChanged += MovementObserver_VelocityChanged;
                movementObserver.AccelerationChanged += MovementObserver_AccelerationChanged;
                movementObserver.WorldMatrixChanged += MovementObserver_WorldMatrixChanged;
                mutationObserver.FunctionalityChanged += MutationObserver_FunctionalityChanged;

                isFunctional = IsFunctionalImpl();

                SetDescription("Framework_Component_Anchor");
            }

            public override string GetContextName()
            {
                return "anchor";
            }

            public Vector3D? GetPosition()
            {
                return movementObserver.GetPosition();
            }

            /// <summary>
            /// Can be null for functional block
            /// </summary>
            public Vector3D? GetVelocity()
            {
                return movementObserver.GetVelocity();
            }

            /// <summary>
            /// Can be null for functional block
            /// </summary>
            public Vector3D? GetAcceleration()
            {
                return movementObserver.GetAcceleration();
            }

            /// <summary>
            /// Can be null for functional block
            /// </summary>
            public float? GetDeltaTime()
            {
                return movementObserver.GetDeltaTime();
            }

            public MatrixD? GetWorldMatrix()
            {
                return movementObserver.GetWorldMatrix();
            }

            public bool IsFunctional()
            {
                return isFunctional;
            }

            public IMyTerminalBlock GetBlock()
            {
                return movementObserver.GetBlock();
            }

            protected override void Cleanup()
            {
                movementObserver.Destruct();
            }

            private void MovementObserver_PositionChanged(object sender, EventArgs eventArgs)
            {
                UpdateFunctionality();

                PositionChanged?.Invoke(this, new EventArgs());
            }

            private void MovementObserver_VelocityChanged(object sender, EventArgs eventArgs)
            {
                VelocityChanged?.Invoke(this, new EventArgs());
            }

            private void MovementObserver_AccelerationChanged(object sender, EventArgs eventArgs)
            {
                AccelerationChanged?.Invoke(this, new EventArgs());
            }

            private void MovementObserver_WorldMatrixChanged(object sender, EventArgs eventArgs)
            {
                UpdateFunctionality();

                WorldMatrixChanged?.Invoke(this, new EventArgs());
            }

            private void MutationObserver_FunctionalityChanged(object sender, EventArgs eventArgs)
            {
                UpdateFunctionality();
            }

            private void UpdateFunctionality()
            {
                var wasFunctional = isFunctional;
                isFunctional = IsFunctionalImpl();

                if (isFunctional != wasFunctional)
                {
                    FunctionalityChanged?.Invoke(this, new EventArgs());
                }
            }

            private bool IsFunctionalImpl()
            {
                return mutationObserver.IsFunctional()
                    && movementObserver.GetPosition() != null
                    && movementObserver.GetWorldMatrix() != null;
            }
        }
    }
}
