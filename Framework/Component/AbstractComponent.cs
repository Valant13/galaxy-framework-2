﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System;
using VRage.Collections;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ObjectBuilders.Definitions;
using VRage.Game;
using VRage;
using VRageMath;

namespace IngameScript
{
    partial class Program
    {
        public abstract class Framework_Component_AbstractComponent : Framework_Component_IComponent, Framework_Terminal_ISubject
        {
            public event EventHandler Destructed;

            private string id;
            private string subjectName;
            private string description;
            private Framework_Component_ComponentRegistry componentRegistry;
            private Framework_Terminal_SubjectRegistry subjectRegistry;

            public Framework_Component_AbstractComponent(string id = null)
            {
                this.id = id;
                componentRegistry = ObjectManager.GetObject<Framework_Component_ComponentRegistry>();
                subjectRegistry = ObjectManager.GetObject<Framework_Terminal_SubjectRegistry>();

                Register();
            }

            public abstract string GetContextName();

            protected virtual void Cleanup()
            { }

            protected virtual void Save()
            { }

            public void Destruct()
            {
                Cleanup();
                Unregister();

                Destructed?.Invoke(this, new EventArgs());
            }

            public string GetId()
            {
                return id;
            }

            public string GetSubjectName()
            {
                return subjectName;
            }

            public string GetDescription()
            {
                return description;
            }

            protected void SetDescription(string description)
            {
                this.description = description;
            }

            private void Register()
            {
                if (string.IsNullOrEmpty(id))
                {
                    id = subjectRegistry.GenerateSubjectName();
                }

                subjectName = id;

                var info = new Framework_Component_Data_ComponentInfo
                {
                    Component = this,
                    Save = Save
                };

                componentRegistry.RegisterComponent(info);
                subjectRegistry.RegisterSubject(this);
            }

            private void Unregister()
            {
                componentRegistry.UnregisterComponent(id);
                subjectRegistry.UnregisterSubject(subjectName);
            }
        }
    }
}
