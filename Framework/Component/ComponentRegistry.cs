﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System;
using VRage.Collections;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ObjectBuilders.Definitions;
using VRage.Game;
using VRage;
using VRageMath;

namespace IngameScript
{
    partial class Program
    {
        public class Framework_Component_ComponentRegistry
        {
            private Association<Framework_Component_Data_ComponentInfo> componentInfos = new Association<Framework_Component_Data_ComponentInfo>();
            private Framework_Utility_HashGenerator hashGenerator;

            public Framework_Component_ComponentRegistry()
            {
                hashGenerator = ObjectManager.GetObject<Framework_Utility_HashGenerator>();
            }

            public void RegisterComponent(Framework_Component_Data_ComponentInfo componentInfo)
            {
                var componentId = componentInfo.Component.GetId();

                if (string.IsNullOrEmpty(componentId))
                {
                    throw new Exception("Component ID is empty");
                }

                if (componentInfos.ContainsKey(componentId))
                {
                    throw new Exception($"Component with ID {componentId} already exists");
                }

                componentInfos[componentId] = componentInfo;
            }

            public void UnregisterComponent(string componentId)
            {
                componentInfos.Remove(componentId);
            }

            public string GenerateComponentId()
            {
                string componentId;
                do
                {
                    componentId = hashGenerator.GenerateRandomHash();
                } while (componentInfos.Keys.Contains(componentId));

                return componentId;
            }

            public void SaveComponents()
            {
                foreach (var componentInfo in componentInfos.Values)
                {
                    componentInfo.Save();
                }
            }

            public Association<Framework_Component_IComponent> GetComponents()
            {
                var components = new Association<Framework_Component_IComponent>();
                foreach (var keyValuePair in componentInfos)
                {
                    components[keyValuePair.Key] = keyValuePair.Value.Component;
                }

                return components;
            }

            public Framework_Component_IComponent GetComponent(string componentId)
            {
                return componentInfos[componentId].Component;
            }

            public bool HasComponent(string componentId)
            {
                return componentInfos.ContainsKey(componentId);
            }
        }
    }
}
