﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System;
using VRage.Collections;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ObjectBuilders.Definitions;
using VRage.Game;
using VRage;
using VRageMath;

namespace IngameScript
{
    partial class Program
    {
        public class Framework_Component_Program_SaveComponents : Framework_Bootstrap_ISaveHandler
        {
            private Framework_Component_ComponentRegistry componentRegistry;

            public Framework_Component_Program_SaveComponents()
            {
                componentRegistry = ObjectManager.GetObject<Framework_Component_ComponentRegistry>();
            }

            public void Execute()
            {
                componentRegistry.SaveComponents();
            }

            public int GetSortOrder()
            {
                return 100;
            }
        }
    }
}
