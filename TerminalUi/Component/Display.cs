﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System;
using VRage.Collections;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ObjectBuilders.Definitions;
using VRage.Game;
using VRage;
using VRageMath;

namespace IngameScript
{
    partial class Program
    {
        public class TerminalUi_Component_Display : Framework_Component_AbstractComponent
        {
            private const string font = "Debug";

            private int surfaceIndex;
            private float fontSize;
            private int linesCount;
            private ActorPack_Actor_TextSurfaceProvider surfaceProvider;
            private TerminalUi_DisplayService displayService;

            public TerminalUi_Component_Display(
                string id,
                Framework_Actor_Data_SearchCriteria surfaceProviderSearchCriteria = null,
                int surfaceIndex = 0,
                float fontSize = 0.5F,
                int linesCount = 20) : base(id)
            {
                displayService = ObjectManager.GetObject<TerminalUi_DisplayService>();
                this.surfaceIndex = surfaceIndex;
                this.fontSize = fontSize;
                this.linesCount = linesCount;

                SetDescription("TerminalUi_Component_Display");

                if (surfaceProviderSearchCriteria == null)
                {
                    surfaceProviderSearchCriteria = new Framework_Actor_Data_SearchCriteria() { IsMe = true };
                }
                surfaceProvider = new ActorPack_Actor_TextSurfaceProvider(surfaceProviderSearchCriteria);

                displayService.ContentChanged += DisplayService_ContentChanged;

                if (HasSurface())
                {
                    ConfigureSurface();
                    RenderContentLines(displayService.GetContentLines());
                }
            }

            public void RenderContentLines(List<string> contentLines)
            {
                if (contentLines.Count > linesCount)
                {
                    contentLines = contentLines.GetRange(contentLines.Count - linesCount, linesCount);
                }

                GetSurface().WriteText(string.Join("\n", contentLines));
            }

            public override string GetContextName()
            {
                return "term-display";
            }

            protected override void Cleanup()
            {
                surfaceProvider.Destruct();
            }

            private void DisplayService_ContentChanged(object sender, List<string> contentLines)
            {
                if (HasSurface())
                {
                    RenderContentLines(contentLines);
                }
            }

            private void ConfigureSurface()
            {
                var surface = GetSurface();

                surface.ContentType = ContentType.TEXT_AND_IMAGE;
                surface.Font = font;
                surface.FontSize = fontSize;
            }

            private IMyTextSurface GetSurface()
            {
                return surfaceProvider.GetSurface(surfaceIndex);
            }

            private bool HasSurface()
            {
                return GetSurface() != null;
            }
        }
    }
}
