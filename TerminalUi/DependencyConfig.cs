﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System;
using VRage.Collections;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ObjectBuilders.Definitions;
using VRage.Game;
using VRage;
using VRageMath;

namespace IngameScript
{
    partial class Program
    {
        public static class TerminalUi_DependencyConfig
        {
            public static void Setup()
            {
                Framework_DependencyConfig.Setup();
                ActorPack_DependencyConfig.Setup();

                var module = ObjectSetup.CreateModule("TerminalUi");

                module.DefineObject<TerminalUi_DisplayService>();
                module.DefineObject<TerminalUi_Command_Clear>();

                module.ModifyObject<Framework_Terminal_CommandRegistry>()
                    .AddArgumentValue("Commands", "This_Clear", typeof(TerminalUi_Command_Clear));
            }
        }
    }
}
