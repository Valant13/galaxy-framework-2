﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System;
using VRage.Collections;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ObjectBuilders.Definitions;
using VRage.Game;
using VRage;
using VRageMath;

namespace IngameScript
{
    partial class Program
    {
        public class TerminalUi_DisplayService
        {
            public delegate void ContentChangedEventHandler(object sender, List<string> contentLines);
            public event ContentChangedEventHandler ContentChanged;

            private const string inputPrefix = "> ";

            private string contentText = "";
            private Framework_Bootstrap_ProgramService programService;
            private Framework_Terminal_TerminalService terminalService;

            public TerminalUi_DisplayService()
            {
                programService = ObjectManager.GetObject<Framework_Bootstrap_ProgramService>();
                terminalService = ObjectManager.GetObject<Framework_Terminal_TerminalService>();

                terminalService.InputProcessing += TerminalService_InputProcessing;
                terminalService.OutputReturning += TerminalService_OutputReturning;

                ClearContent(true);
                RenderOutput("");
            }

            public void RenderInput(string text)
            {
                contentText += text + "\n";

                ContentChanged?.Invoke(this, SplitContentText(contentText));
            }

            public void RenderOutput(string text)
            {
                contentText += text + inputPrefix;

                ContentChanged?.Invoke(this, SplitContentText(contentText));
            }

            public void ClearContent(bool showWelcomeMessage = false)
            {
                if (showWelcomeMessage)
                {
                    contentText = GetWelcomeMessage();
                }
                else
                {
                    contentText = "";
                }

                ContentChanged?.Invoke(this, SplitContentText(contentText));
            }

            public List<string> GetContentLines()
            {
                return SplitContentText(contentText);
            }

            private void TerminalService_InputProcessing(object sender, string input)
            {
                RenderInput(input);
            }

            private void TerminalService_OutputReturning(object sender, string output)
            {
                RenderOutput(output);
            }

            private string GetWelcomeMessage()
            {
                var assemblyName = programService.AssemblyName;
                
                var output = "";
                output += "================\n";
                output += $"{assemblyName}\n";
                output += $"developed with GalaxyFramework2\n";
                output += "================\n";
                output += "\n";

                return output;
            }

            private List<string> SplitContentText(string contentText)
            {
                return contentText.Split(new string[] { "\n" }, StringSplitOptions.None).ToList();
            }
        }
    }
}
