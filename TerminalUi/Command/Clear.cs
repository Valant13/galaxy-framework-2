﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System;
using VRage.Collections;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ObjectBuilders.Definitions;
using VRage.Game;
using VRage;
using VRageMath;

namespace IngameScript
{
    partial class Program
    {
        public class TerminalUi_Command_Clear : Framework_Terminal_AbstractCommand
        {
            private TerminalUi_DisplayService displayService;

            public TerminalUi_Command_Clear()
            {
                SetDescription("Clear terminal displays");
                AddOption("welcome", "w", "Show welcome message");
            }

            public override int Execute(Framework_Terminal_ISubject subject, Framework_Terminal_Input input, Framework_Terminal_Output output)
            {
                // Getting display service here to avoid circular dependency while the construction
                displayService = ObjectManager.GetObject<TerminalUi_DisplayService>();

                displayService.ClearContent(input.HasOption("welcome"));

                return 0;
            }

            public override string GetContextName()
            {
                return "this";
            }

            public override string GetName()
            {
                return "clear";
            }
        }
    }
}
