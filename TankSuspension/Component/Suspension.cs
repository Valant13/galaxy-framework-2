﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System;
using VRage.Collections;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ObjectBuilders.Definitions;
using VRage.Game;
using VRage;
using VRageMath;

namespace IngameScript
{
    partial class Program
    {
        public class TankSuspension_Component_Suspension : Framework_Component_AbstractComponent
        {
            private ActorPack_Actor_ShipController cockpit;
            private ActorPack_Actor_GyroscopeCollection gyroscope;
            private List<ActorPack_Actor_WheelSuspensionCollection> suspensions = new List<ActorPack_Actor_WheelSuspensionCollection>();
            private bool isAimEnabled;
            private float elevation;
            private float elevationSensitivity;
            private float minHeight;
            private float maxHeight;
            private float defaultHeight;

            public TankSuspension_Component_Suspension(
                string id,
                Framework_Actor_Data_SearchCriteria cockpitSearchCriteria,
                Framework_Actor_Data_SearchCriteria gyroscopeSearchCriteria,
                Framework_Actor_Data_SearchCriteria[] suspensionSearchCriterias,
                float elevationSensitivity = 0.004f,
                float minHeight = -0.32f,
                float maxHeight = 0.24f,
                float defaultHeight = -0.32f) : base(id)
            {
                cockpit = new ActorPack_Actor_ShipController(cockpitSearchCriteria);
                gyroscope = new ActorPack_Actor_GyroscopeCollection(gyroscopeSearchCriteria);
                foreach (var suspensionSearchCriteria in suspensionSearchCriterias)
                {
                    suspensions.Add(new ActorPack_Actor_WheelSuspensionCollection(suspensionSearchCriteria));
                }

                this.elevationSensitivity = elevationSensitivity;
                this.minHeight = minHeight;
                this.maxHeight = maxHeight;
                this.defaultHeight = defaultHeight;

                cockpit.RotationIndicatorChanged += Cockpit_RotationIndicatorChanged;

                SetDescription("TankSuspension_Component_Suspension");
            }

            private void Cockpit_RotationIndicatorChanged(object sender, EventArgs eventArgs)
            {
                if (isAimEnabled)
                {
                    var rotation = cockpit.GetRotationIndicator();

                    elevation += -rotation.X * elevationSensitivity;
                    elevation = Math.Min(elevation, 1);
                    elevation = Math.Max(elevation, -1);

                    var heightRange = maxHeight - minHeight;
                    var heightStep = heightRange / (suspensions.Count - 1);

                    var absoluteDefaultHeight = defaultHeight - minHeight;

                    for (int i = 0; i < suspensions.Count; i++)
                    {
                        ActorPack_Actor_WheelSuspensionCollection suspension;

                        if (elevation > 0)
                        {
                            suspension = suspensions[i];
                        }
                        else
                        {
                            suspension = suspensions[suspensions.Count - 1 - i];
                        }

                        suspension.SetHeight((heightStep * i - absoluteDefaultHeight) * Math.Abs(elevation) + defaultHeight);
                    }
                }
            }

            public override string GetContextName()
            {
                return "tank-suspension";
            }

            public void EnableAim()
            {
                gyroscope.Disable();

                isAimEnabled = true;
            }

            public void DisableAim()
            {
                elevation = 0;
                foreach (var suspension in suspensions)
                {
                    suspension.SetHeight(defaultHeight);
                }

                gyroscope.Enable();

                isAimEnabled = false;
            }

            public bool IsAimEnabled()
            {
                return isAimEnabled;
            }

            protected override void Cleanup()
            {
                cockpit.Destruct();
                gyroscope.Destruct();
                foreach (var suspension in suspensions)
                {
                    suspension.Destruct();
                }
            }
        }
    }
}
