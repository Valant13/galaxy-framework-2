﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System;
using VRage.Collections;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ObjectBuilders.Definitions;
using VRage.Game;
using VRage;
using VRageMath;

namespace IngameScript
{
    partial class Program
    {
        public class TankSuspension_Command_ToggleAim : Framework_Terminal_AbstractCommand
        {
            public TankSuspension_Command_ToggleAim()
            {
                SetDescription("Toggle aiming by suspension");
            }

            public override int Execute(Framework_Terminal_ISubject subject, Framework_Terminal_Input input, Framework_Terminal_Output output)
            {
                var suspension = (TankSuspension_Component_Suspension)subject;
                if (!suspension.IsAimEnabled())
                {
                    suspension.EnableAim();
                    output.WriteLine("Aim is enabled");
                }
                else
                {
                    suspension.DisableAim();
                    output.WriteLine("Aim is disabled");
                }
                
                return 0;
            }

            public override string GetContextName()
            {
                return "tank-suspension";
            }

            public override string GetName()
            {
                return "toggle-aim";
            }
        }
    }
}
