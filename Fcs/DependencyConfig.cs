﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System;
using VRage.Collections;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ObjectBuilders.Definitions;
using VRage.Game;
using VRage;
using VRageMath;

namespace IngameScript
{
    partial class Program
    {
        public static class Fcs_DependencyConfig
        {
            public static void Setup()
            {
                Framework_DependencyConfig.Setup();
                ActorPack_DependencyConfig.Setup();
                FcsApi_DependencyConfig.Setup();
                UiApi_DependencyConfig.Setup();

                var module = ObjectSetup.CreateModule("Fcs");

                module.DefineObject<Fcs_Command_CompositeHardpoint_SelectNextWeapon>();
                module.DefineObject<Fcs_Command_DynamicHardpoint_ReloadWeapon>();
                module.DefineObject<Fcs_Command_DynamicHardpoint_SelectNextWeapon>();
                module.DefineObject<Fcs_Command_Memory_SetTarget>();
                module.DefineObject<Fcs_Command_Memory_SetGps>();
                module.DefineObject<Fcs_Command_Memory_ResetTarget>();
                module.DefineObject<Fcs_Command_FireControlSystem_UpdateHardpoints>();
                module.DefineObject<Fcs_Command_FireControlSystem_Fire>();
                module.DefineObject<Fcs_Command_FireControlSystem_ToggleLock>();
                module.DefineObject<Fcs_Command_FireControlSystem_SelectNextWeapon>();
                module.DefineObject<Fcs_Command_FireControlSystem_SelectNextGuidance>();
                module.DefineObject<Fcs_Command_FireControlSystem_SelectNextDetector>();
                module.DefineObject<Fcs_MergeBlockService>();

                module.ModifyObject<Framework_Terminal_CommandRegistry>()
                    .AddArgumentValue("Commands", "CompositeHardpoint_SelectNextWeapon", typeof(Fcs_Command_CompositeHardpoint_SelectNextWeapon))
                    .AddArgumentValue("Commands", "DynamicHardpoint_ReloadWeapon", typeof(Fcs_Command_DynamicHardpoint_ReloadWeapon))
                    .AddArgumentValue("Commands", "DynamicHardpoint_SelectNextWeapon", typeof(Fcs_Command_DynamicHardpoint_SelectNextWeapon))
                    .AddArgumentValue("Commands", "Memory_SetTarget", typeof(Fcs_Command_Memory_SetTarget))
                    .AddArgumentValue("Commands", "Memory_SetGps", typeof(Fcs_Command_Memory_SetGps))
                    .AddArgumentValue("Commands", "Memory_ResetTarget", typeof(Fcs_Command_Memory_ResetTarget))
                    .AddArgumentValue("Commands", "FireControlSystem_UpdateHardpoints", typeof(Fcs_Command_FireControlSystem_UpdateHardpoints))
                    .AddArgumentValue("Commands", "FireControlSystem_Fire", typeof(Fcs_Command_FireControlSystem_Fire))
                    .AddArgumentValue("Commands", "FireControlSystem_ToggleLock", typeof(Fcs_Command_FireControlSystem_ToggleLock))
                    .AddArgumentValue("Commands", "FireControlSystem_SelectNextWeapon", typeof(Fcs_Command_FireControlSystem_SelectNextWeapon))
                    .AddArgumentValue("Commands", "FireControlSystem_SelectNextGuidance", typeof(Fcs_Command_FireControlSystem_SelectNextGuidance))
                    .AddArgumentValue("Commands", "FireControlSystem_SelectNextDetector", typeof(Fcs_Command_FireControlSystem_SelectNextDetector));

                module.ModifyObject<FcsApi_IWeaponFactory>()
                    .SetFactoryClosure(args => new Fcs_WeaponFactory(
                        args["Movements"].ToAssociation<FcsApi_IWeaponMovementFactory>(),
                        args["Radars"].ToAssociation<FcsApi_IWeaponRadarFactory>(),
                        args["Warheads"].ToAssociation<FcsApi_IWeaponWarheadFactory>()));
            }
        }
    }
}
