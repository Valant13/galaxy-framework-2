﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VRage;
using VRage.Collections;
using VRage.Game;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ObjectBuilders.Definitions;
using VRageMath;

namespace IngameScript
{
    partial class Program
    {
        public class Fcs_ViewModel_FireControlSystem
        {
            public UiApi_Data_IdGenericRecordBinding<FcsApi_Data_FcsState> State { get; }
            public UiApi_Data_IdGenericRecordBinding<double?> TargetDistance { get; }
            public UiApi_Data_IdGenericRecordBinding<double?> TargetSpeed { get; }
            public UiApi_Data_IdRecordBinding WeaponTitle { get; }
            public UiApi_Data_IdGenericRecordBinding<bool?> IsHardpointReady { get; }
            public UiApi_Data_IdGenericRecordBinding<int> GuidedWeaponsCount { get; }
            public UiApi_Data_IdGenericRecordBinding<float?> TimeToHit { get; }
            public UiApi_Data_MfdSectionBinding<FcsApi_Data_HardpointPosition> Hardpoints { get; }
            public UiApi_Data_MfdSectionBinding<FcsApi_Data_GuidanceType> Guidances { get; }
            public UiApi_Data_MfdSectionBinding<FcsApi_Data_DetectorType> Detectors { get; }
            public UiApi_Data_WsGenericBinding<FcsApi_Data_FcsState> WarningSystemState { get; }

            public Fcs_ViewModel_FireControlSystem()
            {
                State = new UiApi_Data_IdGenericRecordBinding<FcsApi_Data_FcsState>(v => {
                    switch (v)
                    {
                        case FcsApi_Data_FcsState.Scanning:
                            return "Scanning";

                        case FcsApi_Data_FcsState.Locked:
                            return "Locked";

                        default:
                            return "Waiting";
                    }
                });

                TargetDistance = new UiApi_Data_IdGenericRecordBinding<double?>(v => Math.Round(v.Value / 1000, 2).ToString() + "km");

                TargetSpeed = new UiApi_Data_IdGenericRecordBinding<double?>(v => Math.Round(v.Value).ToString() + "m/s");

                WeaponTitle = new UiApi_Data_IdRecordBinding();

                IsHardpointReady = new UiApi_Data_IdGenericRecordBinding<bool?>();

                GuidedWeaponsCount = new UiApi_Data_IdGenericRecordBinding<int>(null, v => v == 0);

                TimeToHit = new UiApi_Data_IdGenericRecordBinding<float?>(v => Math.Round(v.Value).ToString() + "s");

                Hardpoints = new UiApi_Data_MfdSectionBinding<FcsApi_Data_HardpointPosition>(Enum.GetValues(typeof(FcsApi_Data_HardpointPosition)).OfType<FcsApi_Data_HardpointPosition>().ToList());

                Guidances = new UiApi_Data_MfdSectionBinding<FcsApi_Data_GuidanceType>(Enum.GetValues(typeof(FcsApi_Data_GuidanceType)).OfType<FcsApi_Data_GuidanceType>().ToList());

                Detectors = new UiApi_Data_MfdSectionBinding<FcsApi_Data_DetectorType>(Enum.GetValues(typeof(FcsApi_Data_DetectorType)).OfType<FcsApi_Data_DetectorType>().ToList());

                WarningSystemState = new UiApi_Data_WsGenericBinding<FcsApi_Data_FcsState>(s => {
                    switch (s)
                    {
                        case FcsApi_Data_FcsState.Scanning:
                            return UiApi_Data_WsStatus.Scanning;

                        case FcsApi_Data_FcsState.Locked:
                            return UiApi_Data_WsStatus.Locked;

                        default:
                            return UiApi_Data_WsStatus.None;
                    }
                });
            }
        }
    }
}
