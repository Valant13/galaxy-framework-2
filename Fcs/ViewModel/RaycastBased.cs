﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VRage;
using VRage.Collections;
using VRage.Game;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ObjectBuilders.Definitions;
using VRageMath;

namespace IngameScript
{
    partial class Program
    {
        public class Fcs_ViewModel_RaycastBased
        {
            public UiApi_Data_IdGenericRecordBinding<float> IntervalTime { get; }
            public UiApi_Data_IdGenericRecordBinding<double> Distance { get; }

            public Fcs_ViewModel_RaycastBased()
            {
                IntervalTime = new UiApi_Data_IdGenericRecordBinding<float>(v => Math.Round(v, 2).ToString() + "s", v => v == 0);

                Distance = new UiApi_Data_IdGenericRecordBinding<double>(v => Math.Round(v / 1000, 2).ToString() + "km", v => v == 0);
            }
        }
    }
}
