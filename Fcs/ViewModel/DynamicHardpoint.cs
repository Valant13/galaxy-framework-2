﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VRage;
using VRage.Collections;
using VRage.Game;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ObjectBuilders.Definitions;
using VRageMath;

namespace IngameScript
{
    partial class Program
    {
        public class Fcs_ViewModel_DynamicHardpoint
        {
            public UiApi_Data_MfdSectionBinding<int> Weapons { get; }

            public Fcs_ViewModel_DynamicHardpoint()
            {
                Weapons = new UiApi_Data_MfdSectionBinding<int>(new List<int>() { 0, 1, 2, 3, 4, 5, 6, 7 });
            }
        }
    }
}
