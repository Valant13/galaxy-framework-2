﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VRage;
using VRage.Collections;
using VRage.Game;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ObjectBuilders.Definitions;
using VRageMath;

namespace IngameScript
{
    partial class Program
    {
        public class Fcs_Iterator_Context
        {
            public Association<FcsApi_Data_WeaponType> AvailableWeaponModels { get; set; }
            public Dictionary<FcsApi_Data_HardpointPosition, Fcs_Data_HardpointInfo> HardpointInfos { get; set; }
            public Dictionary<FcsApi_Data_WeaponType, FcsApi_Data_WeaponInfo> WeaponInfos { get; set; }
            public Dictionary<FcsApi_Data_GuidanceType, FcsApi_Data_GuidanceInfo> GuidanceInfos { get; set; }
            public List<FcsApi_Data_WeaponType> AvailableWeapons { get; set; }
            public List<FcsApi_Data_GuidanceType> AvailableGuidances { get; set; }
            public List<FcsApi_Data_DetectorType> AvailableDetectors { get; set; }
        }
    }
}
