﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VRage;
using VRage.Collections;
using VRage.Game;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ObjectBuilders.Definitions;
using VRageMath;

namespace IngameScript
{
    partial class Program
    {
        public class Fcs_Iterator_HardpointState : Fcs_Iterator_ArmedState
        {
            private FcsApi_Data_HardpointPosition currentHardpoint;
            private string currentWeaponModel;
            private FcsApi_Data_GuidanceType currentGuidance;
            private FcsApi_Data_DetectorType currentDetector;

            public override void Enter()
            {
                if (GetDynamicHardpointsWithWeapon().Count == 0)
                {
                    StateMachine.ChangeState<Fcs_Iterator_UnarmedState>();

                    return;
                }

                currentHardpoint = GetFirstHardpoint();
                currentWeaponModel = GetCurrentWeaponModelImpl();
                currentGuidance = GetFirstGuidance(GetCurrentWeapon());
                currentDetector = GetFirstDetector(currentGuidance);
            }

            public override void SelectNextWeapon()
            {
                var previousWeaponModel = currentWeaponModel;
                currentWeaponModel = GetCurrentWeaponModelImpl();

                if (currentWeaponModel == null || currentWeaponModel == previousWeaponModel)
                {
                    if (!HasNextHardpoint(currentHardpoint))
                    {
                        StateMachine.ChangeState<Fcs_Iterator_UnarmedState>();

                        return;
                    }

                    currentHardpoint = GetNextHardpoint(currentHardpoint);
                    currentWeaponModel = GetCurrentWeaponModelImpl();
                }

                currentGuidance = GetFirstGuidance(GetCurrentWeapon());
                currentDetector = GetFirstDetector(currentGuidance);
            }

            public override void SelectNextGuidance()
            {
                currentGuidance = GetNextGuidance(GetCurrentWeapon(), currentGuidance);
                currentDetector = GetFirstDetector(currentGuidance);
            }

            public override void SelectNextDetector()
            {
                currentDetector = GetNextDetector(currentGuidance, currentDetector);
            }

            public override string GetCurrentWeaponModel()
            {
                return currentWeaponModel;
            }

            public override List<FcsApi_Data_HardpointPosition> GetCurrentHardpoints()
            {
                return new List<FcsApi_Data_HardpointPosition>() { currentHardpoint };
            }

            public override FcsApi_Data_GuidanceType? GetCurrentGuidance()
            {
                return currentGuidance;
            }

            public override FcsApi_Data_DetectorType? GetCurrentDetector()
            {
                return currentDetector;
            }

            public override List<FcsApi_Data_HardpointPosition> GetHardpoints()
            {
                return Context.HardpointInfos.Keys.ToList();
            }

            public override List<FcsApi_Data_GuidanceType> GetGuidances()
            {
                var currentWeapon = GetCurrentWeapon();

                return GetGuidancesByWeapon(currentWeapon);
            }

            public override List<FcsApi_Data_DetectorType> GetDetectors()
            {
                return GetDetectorsByGuidance(currentGuidance);
            }

            private FcsApi_Data_HardpointPosition GetFirstHardpoint()
            {
                var hardpoints = GetDynamicHardpointsWithWeapon();

                return hardpoints.First();
            }

            private FcsApi_Data_HardpointPosition GetNextHardpoint(FcsApi_Data_HardpointPosition currentHardpoint)
            {
                var hardpoints = GetDynamicHardpointsWithWeapon();

                var index = hardpoints.IndexOf(currentHardpoint);
                index = index < hardpoints.Count - 1 ? index + 1 : 0;

                return hardpoints[index];
            }

            private bool HasNextHardpoint(FcsApi_Data_HardpointPosition currentHardpoint)
            {
                var hardpoints = GetDynamicHardpointsWithWeapon();

                var index = hardpoints.IndexOf(currentHardpoint);

                return index < hardpoints.Count - 1;
            }

            private List<FcsApi_Data_HardpointPosition> GetDynamicHardpointsWithWeapon()
            {
                var hardpoints = new List<FcsApi_Data_HardpointPosition>();

                foreach (var hardpointInfo in Context.HardpointInfos.Values)
                {
                    if (hardpointInfo.HardpointType != FcsApi_Data_HardpointType.Dynamic)
                    {
                        continue;
                    }

                    var weapon = hardpointInfo.WeaponType;
                    var weaponModel = hardpointInfo.WeaponModelName;

                    if (weapon == null || weaponModel == null)
                    {
                        continue;
                    }

                    hardpoints.Add(hardpointInfo.Position);
                }

                return hardpoints;
            }

            private FcsApi_Data_WeaponType GetCurrentWeapon()
            {
                return Context.AvailableWeaponModels[currentWeaponModel];
            }

            private string GetCurrentWeaponModelImpl()
            {
                return Context.HardpointInfos[currentHardpoint].WeaponModelName;
            }
        }
    }
}
