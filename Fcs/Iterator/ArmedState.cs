﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VRage;
using VRage.Collections;
using VRage.Game;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ObjectBuilders.Definitions;
using VRageMath;

namespace IngameScript
{
    partial class Program
    {
        public abstract class Fcs_Iterator_ArmedState : Fcs_Iterator_AbstractState
        {
            protected FcsApi_Data_GuidanceType GetFirstGuidance(FcsApi_Data_WeaponType currentWeapon)
            {
                var guidances = GetGuidancesByWeapon(currentWeapon);

                return guidances.First();
            }

            protected FcsApi_Data_DetectorType GetFirstDetector(FcsApi_Data_GuidanceType currentGuidance)
            {
                var detectors = GetDetectorsByGuidance(currentGuidance);

                return detectors.First();
            }

            protected FcsApi_Data_GuidanceType GetNextGuidance(FcsApi_Data_WeaponType currentWeapon, FcsApi_Data_GuidanceType currentGuidance)
            {
                var guidances = GetGuidancesByWeapon(currentWeapon);

                var index = guidances.IndexOf(currentGuidance);
                index = index < guidances.Count - 1 ? index + 1 : 0;

                return guidances[index];
            }

            protected FcsApi_Data_DetectorType GetNextDetector(FcsApi_Data_GuidanceType currentGuidance, FcsApi_Data_DetectorType currentDetector)
            {
                var detectors = GetDetectorsByGuidance(currentGuidance);

                var index = detectors.IndexOf(currentDetector);
                index = index < detectors.Count - 1 ? index + 1 : 0;

                return detectors[index];
            }

            protected List<FcsApi_Data_GuidanceType> GetGuidancesByWeapon(FcsApi_Data_WeaponType weapon)
            {
                var guidances = new List<FcsApi_Data_GuidanceType>();

                var weaponInfo = Context.WeaponInfos[weapon];

                foreach (var weaponGuidance in weaponInfo.GuidanceTypes)
                {
                    if (Context.AvailableGuidances.Contains(weaponGuidance))
                    {
                        guidances.Add(weaponGuidance);
                    }
                }

                return guidances;
            }

            protected List<FcsApi_Data_DetectorType> GetDetectorsByGuidance(FcsApi_Data_GuidanceType guidance)
            {
                var detectors = new List<FcsApi_Data_DetectorType>();

                var guidanceInfo = Context.GuidanceInfos[guidance];

                foreach (var guidanceDetector in guidanceInfo.DetectorTypes)
                {
                    if (Context.AvailableDetectors.Contains(guidanceDetector))
                    {
                        detectors.Add(guidanceDetector);
                    }
                }

                return detectors;
            }
        }
    }
}
