﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VRage;
using VRage.Collections;
using VRage.Game;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ObjectBuilders.Definitions;
using VRageMath;

namespace IngameScript
{
    partial class Program
    {
        public class Fcs_Iterator_WeaponState : Fcs_Iterator_ArmedState
        {
            private string currentWeaponModel;
            private FcsApi_Data_GuidanceType currentGuidance;
            private FcsApi_Data_DetectorType currentDetector;

            public override void Enter()
            {
                if (!HasWeaponModels())
                {
                    StateMachine.ChangeState<Fcs_Iterator_HardpointState>();

                    return;
                }

                currentWeaponModel = GetFirstWeaponModel();
                currentGuidance = GetFirstGuidance(GetCurrentWeapon());
                currentDetector = GetFirstDetector(currentGuidance);
            }

            public override void SelectNextWeapon()
            {
                if (!HasNextWeaponModel(currentWeaponModel))
                {
                    StateMachine.ChangeState<Fcs_Iterator_HardpointState>();

                    return;
                }

                currentWeaponModel = GetNextWeaponModel(currentWeaponModel);
                currentGuidance = GetFirstGuidance(GetCurrentWeapon());
                currentDetector = GetFirstDetector(currentGuidance);
            }

            public override void SelectNextGuidance()
            {
                currentGuidance = GetNextGuidance(GetCurrentWeapon(), currentGuidance);
                currentDetector = GetFirstDetector(currentGuidance);
            }

            public override void SelectNextDetector()
            {
                currentDetector = GetNextDetector(currentGuidance, currentDetector);
            }

            public override string GetCurrentWeaponModel()
            {
                return currentWeaponModel;
            }

            public override List<FcsApi_Data_HardpointPosition> GetCurrentHardpoints()
            {
                return GetStaticHardpointsByWeaponModel(currentWeaponModel);
            }

            public override FcsApi_Data_GuidanceType? GetCurrentGuidance()
            {
                return currentGuidance;
            }

            public override FcsApi_Data_DetectorType? GetCurrentDetector()
            {
                return currentDetector;
            }

            public override List<FcsApi_Data_HardpointPosition> GetHardpoints()
            {
                return Context.HardpointInfos.Keys.ToList();
            }

            public override List<FcsApi_Data_GuidanceType> GetGuidances()
            {
                var currentWeapon = GetCurrentWeapon();

                return GetGuidancesByWeapon(currentWeapon);
            }

            public override List<FcsApi_Data_DetectorType> GetDetectors()
            {
                return GetDetectorsByGuidance(currentGuidance);
            }

            private string GetFirstWeaponModel()
            {
                var weaponModels = Context.AvailableWeaponModels.Keys.ToList();

                foreach (var weaponModel in weaponModels)
                {
                    if (GetStaticHardpointsByWeaponModel(weaponModel).Count > 0)
                    {
                        return weaponModel;
                    }
                }

                return null;
            }

            private string GetNextWeaponModel(string currentWeaponModel)
            {
                var weaponModels = Context.AvailableWeaponModels.Keys.ToList();
                var index = weaponModels.IndexOf(currentWeaponModel);

                for (int i = index + 1; i < weaponModels.Count; i++)
                {
                    var weaponModel = weaponModels[i];

                    if (GetStaticHardpointsByWeaponModel(weaponModel).Count > 0)
                    {
                        return weaponModel;
                    }
                }

                return null;
            }

            private bool HasWeaponModels()
            {
                var weaponModels = Context.AvailableWeaponModels.Keys.ToList();

                foreach (var weaponModel in weaponModels)
                {
                    if (GetStaticHardpointsByWeaponModel(weaponModel).Count > 0)
                    {
                        return true;
                    }
                }

                return false;
            }

            private bool HasNextWeaponModel(string currentWeaponModel)
            {
                return GetNextWeaponModel(currentWeaponModel) != null;
            }

            private List<FcsApi_Data_HardpointPosition> GetStaticHardpointsByWeaponModel(string weaponModel)
            {
                var hardpoints = new List<FcsApi_Data_HardpointPosition>();

                foreach (var hardpointInfo in Context.HardpointInfos.Values)
                {
                    if (hardpointInfo.HardpointType != FcsApi_Data_HardpointType.Static)
                    {
                        continue;
                    }

                    if (hardpointInfo.WeaponModelName == weaponModel)
                    {
                        hardpoints.Add(hardpointInfo.Position);
                    }
                }

                return hardpoints;
            }

            private FcsApi_Data_WeaponType GetCurrentWeapon()
            {
                return Context.AvailableWeaponModels[currentWeaponModel];
            }
        }
    }
}
