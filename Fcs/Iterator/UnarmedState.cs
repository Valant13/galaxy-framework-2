﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VRage;
using VRage.Collections;
using VRage.Game;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ObjectBuilders.Definitions;
using VRageMath;

namespace IngameScript
{
    partial class Program
    {
        public class Fcs_Iterator_UnarmedState : Fcs_Iterator_AbstractState
        {
            private FcsApi_Data_DetectorType currentDetector;

            public override void Enter()
            {
                if (HasDetectors())
                {
                    currentDetector = GetFirstDetecor();
                }
            }

            public override void SelectNextWeapon()
            {
                StateMachine.ChangeState<Fcs_Iterator_WeaponState>();
            }

            public override void SelectNextDetector()
            {
                if (HasDetectors())
                {
                    currentDetector = GetNextDetecor(currentDetector);
                }
            }

            public override FcsApi_Data_DetectorType? GetCurrentDetector()
            {
                return currentDetector;
            }

            public override List<FcsApi_Data_HardpointPosition> GetHardpoints()
            {
                return Context.HardpointInfos.Keys.ToList();
            }

            public override List<FcsApi_Data_DetectorType> GetDetectors()
            {
                return Context.AvailableDetectors;
            }

            private FcsApi_Data_DetectorType GetFirstDetecor()
            {
                return Context.AvailableDetectors.First();
            }

            private FcsApi_Data_DetectorType GetNextDetecor(FcsApi_Data_DetectorType currentDetector)
            {
                var index = Context.AvailableDetectors.IndexOf(currentDetector);
                index = index < Context.AvailableDetectors.Count - 1 ? index + 1 : 0;

                return Context.AvailableDetectors[index];
            }

            private bool HasDetectors()
            {
                return Context.AvailableDetectors.Count > 0;
            }
        }
    }
}
