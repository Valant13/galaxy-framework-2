﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VRage;
using VRage.Collections;
using VRage.Game;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ObjectBuilders.Definitions;
using VRageMath;

namespace IngameScript
{
    partial class Program
    {
        public abstract class Fcs_Iterator_AbstractState : Framework_Utility_IState
        {
            protected Fcs_Iterator_Context Context { get; set; }
            protected Framework_Utility_StateMachine<Fcs_Iterator_AbstractState> StateMachine { get; set; }

            public virtual void Enter()
            { }

            public virtual void Exit()
            { }

            public void SetStateMachine(object stateMachine)
            {
                StateMachine = (Framework_Utility_StateMachine<Fcs_Iterator_AbstractState>)stateMachine;
            }

            public void SetContext(object context)
            {
                Context = (Fcs_Iterator_Context)context;
            }

            public virtual void SelectNextWeapon()
            {

            }

            public virtual void SelectNextGuidance()
            {

            }

            public virtual void SelectNextDetector()
            {

            }

            public virtual string GetCurrentWeaponModel()
            {
                return null;
            }

            public virtual List<FcsApi_Data_HardpointPosition> GetCurrentHardpoints()
            {
                return new List<FcsApi_Data_HardpointPosition>();
            }

            public virtual FcsApi_Data_GuidanceType? GetCurrentGuidance()
            {
                return null;
            }

            public virtual FcsApi_Data_DetectorType? GetCurrentDetector()
            {
                return null;
            }

            public virtual List<FcsApi_Data_HardpointPosition> GetHardpoints()
            {
                return new List<FcsApi_Data_HardpointPosition>();
            }

            public virtual List<FcsApi_Data_GuidanceType> GetGuidances()
            {
                return new List<FcsApi_Data_GuidanceType>();
            }

            public virtual List<FcsApi_Data_DetectorType> GetDetectors()
            {
                return new List<FcsApi_Data_DetectorType>();
            }
        }
    }
}
