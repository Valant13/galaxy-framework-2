﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VRage;
using VRage.Collections;
using VRage.Game;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ObjectBuilders.Definitions;
using VRageMath;

namespace IngameScript
{
    partial class Program
    {
        public class Fcs_Iterator
        {
            private Fcs_Iterator_Context context = new Fcs_Iterator_Context();
            private Framework_Utility_StateMachine<Fcs_Iterator_AbstractState> stateMachine;
            private FcsApi_GuidanceConfig guidanceConfig;
            private FcsApi_WeaponConfig weaponConfig;

            public Fcs_Iterator(List<FcsApi_Data_DetectorType> availableDetectors, List<Fcs_Data_HardpointInfo> hardpointInfos)
            {
                guidanceConfig = ObjectManager.GetObject<FcsApi_GuidanceConfig>();
                weaponConfig = ObjectManager.GetObject<FcsApi_WeaponConfig>();

                context.AvailableDetectors = availableDetectors;
                context.AvailableGuidances = GetAvailableGuidances(context.AvailableDetectors);
                context.AvailableWeapons = GetAvailableWeapons(context.AvailableGuidances);

                context.HardpointInfos = PrepareHardpointInfos(hardpointInfos);
                context.WeaponInfos = weaponConfig.GetWeaponInfos();
                context.GuidanceInfos = guidanceConfig.GetGuidanceInfos();

                UpdateAvailableWeaponModels(ref context);

                stateMachine = new Framework_Utility_StateMachine<Fcs_Iterator_AbstractState>(context)
                    .AddState<Fcs_Iterator_UnarmedState>()
                    .AddState<Fcs_Iterator_WeaponState>()
                    .AddState<Fcs_Iterator_HardpointState>()
                    .SetInitialState<Fcs_Iterator_WeaponState>();
            }

            public void UpdateHardpoints(List<Fcs_Data_HardpointInfo> hardpointInfos)
            {
                context.HardpointInfos = PrepareHardpointInfos(hardpointInfos);

                UpdateAvailableWeaponModels(ref context);
            }

            public void SelectNextWeapon()
            {
                stateMachine.GetState().SelectNextWeapon();
            }

            public void SelectNextGuidance()
            {
                stateMachine.GetState().SelectNextGuidance();
            }

            public void SelectNextDetector()
            {
                stateMachine.GetState().SelectNextDetector();
            }

            public string GetCurrentWeaponModel()
            {
                return stateMachine.GetState().GetCurrentWeaponModel();
            }

            public List<FcsApi_Data_HardpointPosition> GetCurrentHardpoints()
            {
                return stateMachine.GetState().GetCurrentHardpoints();
            }

            public FcsApi_Data_GuidanceType? GetCurrentGuidance()
            {
                return stateMachine.GetState().GetCurrentGuidance();
            }

            public FcsApi_Data_DetectorType? GetCurrentDetector()
            {
                return stateMachine.GetState().GetCurrentDetector();
            }

            public List<FcsApi_Data_HardpointPosition> GetHardpoints()
            {
                return stateMachine.GetState().GetHardpoints();
            }

            public List<FcsApi_Data_GuidanceType> GetGuidances()
            {
                return stateMachine.GetState().GetGuidances();
            }

            public List<FcsApi_Data_DetectorType> GetDetectors()
            {
                return stateMachine.GetState().GetDetectors();
            }

            private Dictionary<FcsApi_Data_HardpointPosition, Fcs_Data_HardpointInfo> PrepareHardpointInfos(List<Fcs_Data_HardpointInfo>  hardpointInfos)
            {
                var result = new Dictionary<FcsApi_Data_HardpointPosition, Fcs_Data_HardpointInfo>();

                var sortedHardpointInfos = hardpointInfos.OrderBy(x => x.Position).ToList();

                foreach (var hardpointInfo in sortedHardpointInfos)
                {
                    result[hardpointInfo.Position] = hardpointInfo;
                }

                return result;
            }

            private List<FcsApi_Data_GuidanceType> GetAvailableGuidances(List<FcsApi_Data_DetectorType> availableDetectors)
            {
                var guidanceInfos = guidanceConfig.GetGuidanceInfos();

                var availableGuidances = new List<FcsApi_Data_GuidanceType>();

                foreach (var guidanceInfo in guidanceInfos.Values)
                {
                    foreach (var guidanceDetector in guidanceInfo.DetectorTypes)
                    {
                        if (availableDetectors.Contains(guidanceDetector))
                        {
                            availableGuidances.Add(guidanceInfo.GuidanceType);

                            break;
                        }
                    }
                }

                return availableGuidances.OrderBy(x => x).ToList();
            }

            private List<FcsApi_Data_WeaponType> GetAvailableWeapons(List<FcsApi_Data_GuidanceType> availableGuidances)
            {
                var weaponInfos = weaponConfig.GetWeaponInfos();

                var availableWeapons = new List<FcsApi_Data_WeaponType>();

                foreach (var weaponInfo in weaponInfos.Values)
                {
                    foreach (var weaponGuidance in weaponInfo.GuidanceTypes)
                    {
                        if (availableGuidances.Contains(weaponGuidance))
                        {
                            availableWeapons.Add(weaponInfo.WeaponType);

                            break;
                        }
                    }
                }

                return availableWeapons.OrderBy(x => x).ToList();
            }

            private void UpdateAvailableWeaponModels(ref Fcs_Iterator_Context context)
            {
                if (context.AvailableWeaponModels == null)
                {
                    context.AvailableWeaponModels = new Association<FcsApi_Data_WeaponType>();
                }

                foreach (var hardpointInfo in context.HardpointInfos.Values)
                {
                    var weapon = hardpointInfo.WeaponType;
                    var weaponModel = hardpointInfo.WeaponModelName;

                    if (weapon == null || weaponModel == null)
                    {
                        continue;
                    }

                    if (context.AvailableWeapons.Contains(weapon.Value))
                    {
                        context.AvailableWeaponModels[weaponModel] = weapon.Value;
                    }
                }

                var sortedWeaponNames = context.AvailableWeaponModels.OrderBy(x => x.Value).ThenBy(x => x.Key);

                context.AvailableWeaponModels = new Association<FcsApi_Data_WeaponType>();
                foreach (var pair in sortedWeaponNames)
                {
                    context.AvailableWeaponModels[pair.Key] = pair.Value;
                }
            }
        }
    }
}
