﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System;
using VRage.Collections;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ObjectBuilders.Definitions;
using VRage.Game;
using VRage;
using VRageMath;

namespace IngameScript
{
    partial class Program
    {
        public class Fcs_Command_Memory_SetTarget : Framework_Terminal_AbstractCommand
        {
            private Framework_Utility_Timer timer;

            public Fcs_Command_Memory_SetTarget()
            {
                timer = ObjectManager.GetObject<Framework_Utility_Timer>();

                SetDescription("Set target position to FCS memory");
                AddArgument("x");
                AddArgument("y");
                AddArgument("z");
            }

            public override int Execute(Framework_Terminal_ISubject subject, Framework_Terminal_Input input, Framework_Terminal_Output output)
            {
                var memory = (Fcs_Component_Memory)subject;

                double x;
                double y;
                double z;

                try
                {
                    x = double.Parse(input.GetArgument("x"));
                    y = double.Parse(input.GetArgument("y"));
                    z = double.Parse(input.GetArgument("z"));
                }
                catch (Exception e)
                {
                    output.WriteLine(e.Message);

                    return 1;
                }

                var targetPosition = new Vector3D(x, y, z);
                var targetVelocity = Vector3D.Zero;
                var targetDetectionTime = timer.GetTimeNow();

                var targetInfo = new FcsApi_Data_TargetInfo(targetPosition, targetVelocity, targetDetectionTime);

                memory.SetTargetInfo(targetInfo);
                output.WriteLine("Target saved");

                return 0;
            }

            public override string GetContextName()
            {
                return "memory";
            }

            public override string GetName()
            {
                return "set-target";
            }
        }
    }
}
