﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System;
using VRage.Collections;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ObjectBuilders.Definitions;
using VRage.Game;
using VRage;
using VRageMath;

namespace IngameScript
{
    partial class Program
    {
        public class Fcs_Command_Memory_SetGps : Framework_Terminal_AbstractCommand
        {
            private Framework_Utility_Timer timer;

            public Fcs_Command_Memory_SetGps()
            {
                timer = ObjectManager.GetObject<Framework_Utility_Timer>();

                SetDescription("Set target GPS to FCS memory");
                AddArgument("name", false, "Formal argument");
                AddArgument("point", false, "String with coordinates");
            }

            public override int Execute(Framework_Terminal_ISubject subject, Framework_Terminal_Input input, Framework_Terminal_Output output)
            {
                var memory = (Fcs_Component_Memory)subject;

                Vector3D targetPosition;

                try
                {
                    targetPosition = ConvertGpsPointToPosition(input.GetArgument("point"));
                }
                catch (Exception e)
                {
                    output.WriteLine(e.Message);

                    return 1;
                }

                var targetVelocity = Vector3D.Zero;
                var targetDetectionTime = timer.GetTimeNow();

                var targetInfo = new FcsApi_Data_TargetInfo(targetPosition, targetVelocity, targetDetectionTime);

                memory.SetTargetInfo(targetInfo);
                output.WriteLine("Target saved");

                return 0;
            }

            public override string GetContextName()
            {
                return "memory";
            }

            public override string GetName()
            {
                return "set-gps";
            }

            private Vector3D ConvertGpsPointToPosition(string gpsPoint)
            {
                var portions = gpsPoint.Split(':');

                if (portions.Length < 4)
                {
                    throw new Exception("GPS point is incorrect");
                }

                var x = double.Parse(portions[1]);
                var y = double.Parse(portions[2]);
                var z = double.Parse(portions[3]);

                return new Vector3D(x, y, z);
            }
        }
    }
}
