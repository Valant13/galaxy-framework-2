﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System;
using VRage.Collections;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ObjectBuilders.Definitions;
using VRage.Game;
using VRage;
using VRageMath;

namespace IngameScript
{
    partial class Program
    {
        public class Fcs_Command_Memory_ResetTarget : Framework_Terminal_AbstractCommand
        {
            public Fcs_Command_Memory_ResetTarget()
            {
                SetDescription("Reset FCS memory target");
            }

            public override int Execute(Framework_Terminal_ISubject subject, Framework_Terminal_Input input, Framework_Terminal_Output output)
            {
                var memory = (Fcs_Component_Memory)subject;

                memory.SetTargetInfo(null);
                output.WriteLine("Target cleared");

                return 0;
            }

            public override string GetContextName()
            {
                return "memory";
            }

            public override string GetName()
            {
                return "reset-target";
            }
        }
    }
}
