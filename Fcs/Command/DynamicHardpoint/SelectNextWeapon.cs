﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VRage;
using VRage.Collections;
using VRage.Game;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ObjectBuilders.Definitions;
using VRageMath;

namespace IngameScript
{
    partial class Program
    {
        public class Fcs_Command_DynamicHardpoint_SelectNextWeapon : Framework_Terminal_AbstractCommand
        {
            public Fcs_Command_DynamicHardpoint_SelectNextWeapon()
            {
                SetDescription("Select next weapon of dynamic hardpoint");
            }

            public override int Execute(Framework_Terminal_ISubject subject, Framework_Terminal_Input input, Framework_Terminal_Output output)
            {
                var hardpoint = (Fcs_Component_DynamicHardpoint)subject;

                hardpoint.SelectNextWeapon();
                output.WriteLine("Done");

                return 0;
            }

            public override string GetContextName()
            {
                return "dynamic-hardpoint";
            }

            public override string GetName()
            {
                return "select-next-weapon";
            }
        }
    }
}
