﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System;
using VRage.Collections;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ObjectBuilders.Definitions;
using VRage.Game;
using VRage;
using VRageMath;

namespace IngameScript
{
    partial class Program
    {
        public class Fcs_Component_NoneDetector : Framework_Component_AbstractComponent, FcsApi_Component_IDetector
        {
            public event EventHandler TargetLocked;
            public event EventHandler TargetLost;
            public event EventHandler TargetChanged;

            public Fcs_Component_NoneDetector(string id) : base(id)
            {
                SetDescription("Fcs_Component_NoneDetector");
            }

            public void EnableScan()
            { }

            public void DisableScan()
            { }

            public bool IsScanEnabled()
            {
                return false;
            }

            public bool IsTargetLocked()
            {
                return false;
            }

            public FcsApi_Data_TargetInfo GetTargetInfo()
            {
                return null;
            }

            public override string GetContextName()
            {
                return "none-detector";
            }

            public FcsApi_Data_DetectorType GetDetectorType()
            {
                return FcsApi_Data_DetectorType.None;
            }
        }
    }
}
