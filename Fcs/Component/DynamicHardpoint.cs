﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VRage;
using VRage.Collections;
using VRage.Game;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ObjectBuilders.Definitions;
using VRageMath;

namespace IngameScript
{
    partial class Program
    {
        public class Fcs_Component_DynamicHardpoint : Framework_Component_AbstractComponent, FcsApi_Component_IHardpoint
        {
            private enum State
            {
                Loaded,
                Launching,
                Reloading
            }
            
            public event EventHandler WeaponChanged;
            public event EventHandler ReadinessChanged;

            public const string Section = "WeaponAssembly";
            public const string WeldingTimeKey = "WeldingTime";
            public const string RefuelingTimeKey = "RefuelingTime";
            private const float connectorConnectionTime = 1;

            private State state = State.Loaded;
            private BoundingBoxI weaponBoundingBox;
            private FcsApi_Data_HardpointPosition position;
            private Fcs_ViewModel_DynamicHardpoint viewModel;
            private ActorPack_Actor_MergeBlock mergeBlock;
            private ActorPack_Actor_WelderCollection welder;
            private ActorPack_Actor_ConnectorCollection connector;
            private FcsApi_Component_IWeapon loadedWeapon;
            private FcsApi_Component_IWeapon launchedWeapon;
            private List<Fcs_Data_WeaponAssembly> weaponAssemblies;
            private int currentWeaponNumber;
            private int assemblingWeaponNumber;
            private Framework_Actor_CacheService cacheService;
            private Framework_Actor_IniService iniService;
            private FcsApi_IWeaponFactory weaponFactory;
            private FcsApi_WeaponBlueprintFactory blueprintFactory;
            private Fcs_MergeBlockService mergeBlockService;
            private Framework_Utility_Timer timer;
            private Framework_Utility_WorldService worldService;

            public Fcs_Component_DynamicHardpoint(
                string id,
                Framework_Actor_Data_SearchCriteria mergeBlockSearchCriteria,
                Framework_Actor_Data_SearchCriteria welderSearchCriteria,
                Framework_Actor_Data_SearchCriteria connectorSearchCriteria,
                Framework_Actor_Data_SearchCriteria[] projectorSearchCriterias,
                FcsApi_Data_HardpointPosition position,
                Fcs_ViewModel_DynamicHardpoint viewModel = null,
                BoundingBoxI? weaponBoundingBox = null) : base(id)
            {
                this.position = position;
                this.viewModel = viewModel;
                cacheService = ObjectManager.GetObject<Framework_Actor_CacheService>();
                iniService = ObjectManager.GetObject<Framework_Actor_IniService>();
                weaponFactory = ObjectManager.GetObject<FcsApi_IWeaponFactory>();
                blueprintFactory = ObjectManager.GetObject<FcsApi_WeaponBlueprintFactory>();
                mergeBlockService = ObjectManager.GetObject<Fcs_MergeBlockService>();
                timer = ObjectManager.GetObject<Framework_Utility_Timer>();
                worldService = ObjectManager.GetObject<Framework_Utility_WorldService>();

                mergeBlock = new ActorPack_Actor_MergeBlock(mergeBlockSearchCriteria);
                welder = new ActorPack_Actor_WelderCollection(welderSearchCriteria);
                connector = new ActorPack_Actor_ConnectorCollection(connectorSearchCriteria);

                weaponAssemblies = PrepareWeaponAssemblies(projectorSearchCriterias.ToList());

                welder.BlockWelded += Welder_BlockWelded;

                this.weaponBoundingBox = weaponBoundingBox ?? new BoundingBoxI(new Vector3I(1, -2, 0), new Vector3I(3, 9, 0));

                SetDescription("Fcs_Component_DynamicHardpoint");

                UpdateWeapon();
                UpdateViewModel();
            }

            public override string GetContextName()
            {
                return "dynamic-hardpoint";
            }

            public void UpdateWeapon()
            {
                if (state == State.Loaded)
                {
                    if (loadedWeapon != null)
                    {
                        loadedWeapon.Destruct();
                        loadedWeapon = null;
                    }

                    launchedWeapon = null;

                    cacheService.UpdateBlockCache();

                    var weaponMergeBlock = mergeBlock.GetConnectedMergeBlock();
                    if (weaponMergeBlock != null)
                    {
                        var customDataIni = iniService.GetBlockCustomDataIni(weaponMergeBlock);
                        if (customDataIni != null && customDataIni.ContainsSection(FcsApi_WeaponBlueprintFactory.Section))
                        {
                            var blueprint = blueprintFactory.CreateBlueprintFromIni(customDataIni);
                            var adjustedBoundingBox = mergeBlockService.TransformBoundingBox(weaponBoundingBox, mergeBlock.GetBlock());

                            loadedWeapon = weaponFactory.CreateWeapon(blueprint, adjustedBoundingBox);
                            loadedWeapon.FunctionalityChanged += Weapon_FunctionalityChanged;
                        }
                    }

                    WeaponChanged?.Invoke(this, new EventArgs());
                    ReadinessChanged?.Invoke(this, new EventArgs());
                }
            }

            public FcsApi_Component_IWeapon GetWeapon()
            {
                if (state == State.Loaded)
                {
                    return loadedWeapon;
                }
                else
                {
                    return launchedWeapon;
                }
            }

            public void SelectNextWeapon()
            {
                currentWeaponNumber = currentWeaponNumber + 1 < weaponAssemblies.Count ? currentWeaponNumber + 1 : 0;

                UpdateViewModel();
            }

            public bool IsReady()
            {
                return state == State.Loaded && loadedWeapon != null && loadedWeapon.IsFunctional();
            }

            public void LaunchWeapon(FcsApi_Data_GuidanceType guidanceType, FcsApi_Data_TargetInfo targetInfo)
            {
                if (state == State.Loaded)
                {
                    if (!IsReady())
                    {
                        return;
                    }

                    state = State.Launching;

                    loadedWeapon.Launch(guidanceType, targetInfo);
                    loadedWeapon.FunctionalityChanged -= Weapon_FunctionalityChanged;
                    launchedWeapon = loadedWeapon;
                    loadedWeapon = null;

                    ReadinessChanged?.Invoke(this, new EventArgs());

                    timer.SetTimeout(launchedWeapon.GetDecoupleTime(), () => {
                        if (state == State.Launching)
                        {
                            state = State.Loaded;

                            ReloadWeapon();
                        }
                    });
                }
            }

            public void ReloadWeapon()
            {
                if (state == State.Loaded)
                {
                    state = State.Reloading;

                    assemblingWeaponNumber = currentWeaponNumber;
                    var weaponAssembly = weaponAssemblies[assemblingWeaponNumber];

                    weaponAssembly.Projector.Enable();
                    welder.WeldBlock(weaponAssembly.WeldingTime);
                }
            }

            public bool IsWeaponReloadAvailable()
            {
                return state == State.Loaded;
            }

            public FcsApi_Data_HardpointPosition GetPosition()
            {
                return position;
            }

            public FcsApi_Data_HardpointType GetHardpointType()
            {
                return FcsApi_Data_HardpointType.Dynamic;
            }

            private void Weapon_FunctionalityChanged(object sender, EventArgs eventArgs)
            {
                ReadinessChanged?.Invoke(this, new EventArgs());
            }

            private void Welder_BlockWelded(object sender, EventArgs eventArgs)
            {
                if (state == State.Reloading)
                {
                    var weaponAssembly = weaponAssemblies[assemblingWeaponNumber];

                    weaponAssembly.Projector.Disable();

                    if (weaponAssembly.RefuelingTime > 0 && worldService.GameMode != Framework_Utility_WorldService.Framework_World_Data_GameMode.Creative)
                    {
                        connector.Enable();

                        timer.SetTimeout(connectorConnectionTime, connector.Connect);

                        timer.SetTimeout(connectorConnectionTime + weaponAssembly.RefuelingTime, () => {
                            connector.Disconnect();
                            connector.Disable();

                            state = State.Loaded;
                            UpdateWeapon();
                        });
                    }
                    else
                    {
                        state = State.Loaded;
                        UpdateWeapon();
                    }
                }
            }

            private void UpdateViewModel()
            {
                if (viewModel != null)
                {
                    viewModel.Weapons.HideIndicators();

                    for (var i = 0; i < weaponAssemblies.Count; i++)
                    {
                        viewModel.Weapons.DisableIndicator(i);
                    }

                    viewModel.Weapons.EnableIndicator(currentWeaponNumber);
                }
            }

            private List<Fcs_Data_WeaponAssembly> PrepareWeaponAssemblies(List<Framework_Actor_Data_SearchCriteria> projectorSearchCriterias)
            {
                var assemblies = new List<Fcs_Data_WeaponAssembly>();

                foreach (var searchCriteria in projectorSearchCriterias)
                {
                    var projector = new ActorPack_Actor_Projector(searchCriteria);
                    var ini = iniService.GetBlockCustomDataIni(projector.GetBlock());

                    assemblies.Add(new Fcs_Data_WeaponAssembly()
                    {
                        WeldingTime = iniService.GetFloat(ini, Section, WeldingTimeKey),
                        RefuelingTime = iniService.GetFloat(ini, Section, RefuelingTimeKey),
                        Projector = projector
                    });
                }

                return assemblies;
            }
        }
    }
}
