﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VRage;
using VRage.Collections;
using VRage.Game;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ObjectBuilders.Definitions;
using VRageMath;

namespace IngameScript
{
    partial class Program
    {
        public class Fcs_Component_FireControlSystem : Framework_Component_AbstractComponent, FcsApi_Component_IFireControlSystem
        {
            private const float detonationTime = 4;

            private Fcs_ViewModel_FireControlSystem viewModel;
            private string weaponTitle;
            private Dictionary<FcsApi_Data_HardpointPosition, FcsApi_Component_IHardpoint> hardpoints;
            private Dictionary<FcsApi_Data_DetectorType, FcsApi_Component_IDetector> detectors;
            private List<FcsApi_Component_IWeapon> weapons = new List<FcsApi_Component_IWeapon>();
            private List<FcsApi_Component_IWeapon> guidedWeapons = new List<FcsApi_Component_IWeapon>();
            private Framework_Component_Anchor anchor;
            private Fcs_Iterator iterator;
            private Framework_Actor_CacheService cacheService;
            private Framework_Utility_Timer timer;

            public Fcs_Component_FireControlSystem(
                string id,
                Framework_Component_Anchor anchor,
                FcsApi_Component_IHardpoint[] hardpoints,
                FcsApi_Component_IDetector[] detectors,
                Fcs_ViewModel_FireControlSystem viewModel = null) : base(id)
            {
                cacheService = ObjectManager.GetObject<Framework_Actor_CacheService>();
                timer = ObjectManager.GetObject<Framework_Utility_Timer>();
                this.anchor = anchor;
                this.hardpoints = PrepareHardpoints(hardpoints);
                this.detectors = PrepareDetectors(detectors);
                this.viewModel = viewModel;

                iterator = new Fcs_Iterator(this.detectors.Keys.ToList(), GetHardpointInfos(this.hardpoints));
                weaponTitle = GetCurrentWeaponTitle();

                foreach (var hardpoint in this.hardpoints.Values)
                {
                    hardpoint.WeaponChanged += Hardpoint_WeaponChanged;
                    hardpoint.ReadinessChanged += Hardpoint_ReadinessChanged;
                }

                foreach (var detector in this.detectors.Values)
                {
                    detector.TargetChanged += Detector_TargetChanged;
                    detector.TargetLocked += Detector_TargetLocked;
                    detector.TargetLost += Detector_TargetLost;
                }

                anchor.PositionChanged += Anchor_PositionChanged;

                SetDescription("Fcs_Component_FireControlSystem");

                UpdateViewModel();
            }

            public override string GetContextName()
            {
                return "fire-control-system";
            }

            public void Fire()
            {
                var hardpoint = GetCurrentReadyHardpoint();

                if (hardpoint != null)
                {
                    var weapon = hardpoint.GetWeapon();

                    // Checks if current weapon model is synchronized with dynamic hardpoint weapon
                    if (weapon.GetModelName() != iterator.GetCurrentWeaponModel())
                    {
                        return;
                    }

                    weapon.Detonated += Weapon_Detonated;
                    weapon.TargetLocked += Weapon_TargetLocked;
                    weapon.TimeToHitChanged += Weapon_TimeToHitChanged;
                    weapon.FunctionalityChanged += Weapon_FunctionalityChanged;

                    weapons.Add(weapon);

                    if (!weapon.IsTargetLocked())
                    {
                        guidedWeapons.Add(weapon);
                    }

                    var guidance = iterator.GetCurrentGuidance().Value;
                    var targetInfo = GetCurrentDetector().GetTargetInfo();

                    hardpoint.LaunchWeapon(guidance, targetInfo);

                    UpdateViewModel();
                }
            }

            public void ToggleLock()
            {
                var detector = GetCurrentDetector();

                if (detector != null)
                {
                    if (detector.IsScanEnabled())
                    {
                        detector.DisableScan();
                    }
                    else
                    {
                        detector.EnableScan();
                    }

                    UpdateViewModel();
                }
            }

            public void SelectNextWeapon()
            {
                GetCurrentDetector()?.DisableScan();

                guidedWeapons.Clear();
                iterator.SelectNextWeapon();
                weaponTitle = GetCurrentWeaponTitle();

                UpdateViewModel();
            }

            public void SelectNextGuidance()
            {
                GetCurrentDetector()?.DisableScan();

                guidedWeapons.Clear();
                iterator.SelectNextGuidance();

                UpdateViewModel();
            }

            public void SelectNextDetector()
            {
                GetCurrentDetector()?.DisableScan();

                iterator.SelectNextDetector();

                UpdateViewModel();
            }

            public void UpdateHardpoints()
            {
                foreach (var weapon in weapons)
                {
                    weapon.Destruct();
                }
                weapons.Clear();
                guidedWeapons.Clear();

                cacheService.UpdateBlockCache(Framework_Actor_Data_BlockUpdateStrategy.Hard);

                foreach (var hardpoint in hardpoints.Values)
                {
                    hardpoint.UpdateWeapon();
                }

                UpdateViewModel();
            }

            private void Anchor_PositionChanged(object sender, EventArgs eventArgs)
            {
                if (anchor.IsFunctional())
                {
                    UpdateViewModel();
                }
            }

            private void Hardpoint_WeaponChanged(object sender, EventArgs eventArgs)
            {
                iterator.UpdateHardpoints(GetHardpointInfos(hardpoints));

                UpdateViewModel();
            }

            private void Hardpoint_ReadinessChanged(object sender, EventArgs eventArgs)
            {
                var hardpoint = (FcsApi_Component_IHardpoint)sender;

                if (GetCurrentHardpoints().Contains(hardpoint))
                {
                    UpdateViewModel();
                }
            }

            private void Detector_TargetChanged(object sender, EventArgs eventArgs)
            {
                var detector = (FcsApi_Component_IDetector)sender;

                if (detector == GetCurrentDetector())
                {
                    var targetInfo = detector.GetTargetInfo();

                    if (targetInfo != null)
                    {
                        var listCopy = guidedWeapons.ToList();
                        foreach (var weapon in listCopy)
                        {
                            weapon.UpdateTarget(targetInfo);
                        }
                    }

                    UpdateViewModel();
                }
            }

            private void Detector_TargetLocked(object sender, EventArgs eventArgs)
            {
                var detector = (FcsApi_Component_IDetector)sender;

                if (detector == GetCurrentDetector())
                {
                    UpdateViewModel();
                }
            }

            private void Detector_TargetLost(object sender, EventArgs eventArgs)
            {
                var detector = (FcsApi_Component_IDetector)sender;

                if (detector == GetCurrentDetector())
                {
                    UpdateViewModel();
                }
            }

            private void Weapon_Detonated(object sender, EventArgs eventArgs)
            {
                var weapon = (FcsApi_Component_IWeapon)sender;

                weapon.Destruct();
                weapons.Remove(weapon);
                guidedWeapons.Remove(weapon);

                UpdateViewModel();
            }

            private void Weapon_TimeToHitChanged(object sender, EventArgs eventArgs)
            {
                UpdateViewModel();
            }

            private void Weapon_TargetLocked(object sender, EventArgs eventArgs)
            {
                var weapon = (FcsApi_Component_IWeapon)sender;

                guidedWeapons.Remove(weapon);

                UpdateViewModel();
            }

            private void Weapon_FunctionalityChanged(object sender, EventArgs eventArgs)
            {
                var weapon = (FcsApi_Component_IWeapon)sender;

                if (!weapon.IsFunctional())
                {
                    timer.SetTimeout(detonationTime, () => {
                        weapon.Detonate();

                        UpdateViewModel();
                    });
                }
            }

            private List<FcsApi_Component_IHardpoint> GetCurrentHardpoints()
            {
                var currentHardpointPositions = iterator.GetCurrentHardpoints();

                var currentHardpoints = new List<FcsApi_Component_IHardpoint>();

                foreach (var hardpointPosition in currentHardpointPositions)
                {
                    currentHardpoints.Add(hardpoints[hardpointPosition]);
                }

                return currentHardpoints;
            }

            private FcsApi_Component_IDetector GetCurrentDetector()
            {
                var currentDetectorType = iterator.GetCurrentDetector();

                if (currentDetectorType == null)
                {
                    return null;
                }

                return detectors[currentDetectorType.Value];
            }

            private Dictionary<FcsApi_Data_HardpointPosition, FcsApi_Component_IHardpoint> PrepareHardpoints(FcsApi_Component_IHardpoint[] hardpoints)
            {
                var result = new Dictionary<FcsApi_Data_HardpointPosition, FcsApi_Component_IHardpoint>();
                
                foreach (var hardpoint in hardpoints)
                {
                    result[hardpoint.GetPosition()] = hardpoint;
                }

                return result;
            }

            private Dictionary<FcsApi_Data_DetectorType, FcsApi_Component_IDetector> PrepareDetectors(FcsApi_Component_IDetector[] detectors)
            {
                var result = new Dictionary<FcsApi_Data_DetectorType, FcsApi_Component_IDetector>();

                result[FcsApi_Data_DetectorType.None] = new Fcs_Component_NoneDetector(null);

                foreach (var detector in detectors)
                {
                    result[detector.GetDetectorType()] = detector;
                }

                return result;
            }

            private List<Fcs_Data_HardpointInfo> GetHardpointInfos(Dictionary<FcsApi_Data_HardpointPosition, FcsApi_Component_IHardpoint> hardpoints)
            {
                var hardpointInfos = new List<Fcs_Data_HardpointInfo>();

                foreach (var hardpoint in hardpoints.Values)
                {
                    var weapon = hardpoint.GetWeapon();

                    var hardpointInfo = new Fcs_Data_HardpointInfo()
                    {
                        HardpointType = hardpoint.GetHardpointType(),
                        Position = hardpoint.GetPosition(),
                        WeaponModelName = weapon?.GetModelName(),
                        WeaponType = weapon?.GetWeaponType()
                    };

                    hardpointInfos.Add(hardpointInfo);
                }

                return hardpointInfos;
            }

            private FcsApi_Data_FcsState GetState()
            {
                var state = FcsApi_Data_FcsState.Waiting;

                var detector = GetCurrentDetector();

                if (detector != null)
                {
                    if (detector.IsScanEnabled())
                    {
                        state = FcsApi_Data_FcsState.Scanning;
                    }

                    if (detector.IsTargetLocked())
                    {
                        state = FcsApi_Data_FcsState.Locked;
                    }
                }

                return state;
            }

            private double? GetTargetDistance()
            {
                var detector = GetCurrentDetector();

                if (!anchor.IsFunctional() || detector == null || detector.GetTargetInfo() == null)
                {
                    return null;
                }

                var targetPosition = detector.GetTargetInfo().Position;
                var anchorPosition = anchor.GetPosition().Value;

                return (targetPosition - anchorPosition).Length();
            }

            private double? GetTargetSpeed()
            {
                var detector = GetCurrentDetector();

                if (detector == null || detector.GetTargetInfo() == null)
                {
                    return null;
                }

                var targetInfo = detector.GetTargetInfo();

                return Math.Round(targetInfo.Velocity.Length());
            }

            private bool? IsHardpointReady()
            {
                if (GetCurrentHardpoints().Count == 0)
                {
                    return null;
                }

                return GetCurrentReadyHardpoint() != null;
            }

            private float? GetTimeToHit()
            {
                float? time = null;

                foreach (var weapon in weapons)
                {
                    if (weapon.GetTimeToHit() != null && time != null)
                    {
                        time = Math.Min(time.Value, weapon.GetTimeToHit().Value);
                    }
                    else if (weapon.GetTimeToHit() != null && time == null)
                    {
                        time = weapon.GetTimeToHit();
                    }
                }

                return time;
            }

            private FcsApi_Component_IHardpoint GetCurrentReadyHardpoint()
            {
                var hardpoints = GetCurrentHardpoints();

                foreach (var hardpoint in hardpoints)
                {
                    if (hardpoint.IsReady())
                    {
                        return hardpoint;
                    }
                }

                return null;
            }

            private string GetCurrentWeaponTitle()
            {
                var hardpoints = GetCurrentHardpoints();

                foreach (var hardpoint in hardpoints)
                {
                    var weapon = hardpoint.GetWeapon();

                    if (weapon != null)
                    {
                        return weapon.GetTitle();
                    }
                }

                return null;
            }

            private void UpdateViewModel()
            {
                if (viewModel != null)
                {
                    viewModel.State.RenderValue(GetState());
                    viewModel.TargetDistance.RenderValue(GetTargetDistance());
                    viewModel.TargetSpeed.RenderValue(GetTargetSpeed());
                    viewModel.WeaponTitle.SetValue(weaponTitle);
                    viewModel.IsHardpointReady.RenderValue(IsHardpointReady());
                    viewModel.GuidedWeaponsCount.RenderValue(guidedWeapons.Count);
                    viewModel.TimeToHit.RenderValue(GetTimeToHit());

                    viewModel.Hardpoints.HideIndicators();
                    viewModel.Hardpoints.DisableIndicators(iterator.GetHardpoints());
                    viewModel.Hardpoints.EnableIndicators(iterator.GetCurrentHardpoints());
                    viewModel.Guidances.HideIndicators();
                    viewModel.Guidances.DisableIndicators(iterator.GetGuidances());
                    var currentGuidance = iterator.GetCurrentGuidance();
                    if (currentGuidance != null)
                    {
                        viewModel.Guidances.EnableIndicator(currentGuidance.Value);
                    }
                    viewModel.Detectors.HideIndicators();
                    viewModel.Detectors.DisableIndicators(iterator.GetDetectors());
                    var currentDetector = iterator.GetCurrentDetector();
                    if (currentDetector != null)
                    {
                        viewModel.Detectors.EnableIndicator(currentDetector.Value);
                    }

                    viewModel.WarningSystemState.MapStatus(GetState());
                }
            }
        }
    }
}
