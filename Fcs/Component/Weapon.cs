﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VRage;
using VRage.Collections;
using VRage.Game;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ObjectBuilders.Definitions;
using VRageMath;

namespace IngameScript
{
    partial class Program
    {
        public class Fcs_Component_Weapon : Framework_Component_AbstractComponent, FcsApi_Component_IWeapon
        {
            private enum State
            {
                Disabled,
                Launched,
                Detonated
            }

            public event EventHandler FunctionalityChanged;
            public event EventHandler Detonated;
            public event EventHandler TargetLocked;
            public event EventHandler TimeToHitChanged;

            private const float armTime = 2;

            private State state = State.Disabled;
            private bool isTargetLocked;
            private bool isFunctional;
            private FcsApi_Data_GuidanceType guidanceType;
            private FcsApi_Data_TargetInfo targetInfo;
            private string title;
            private FcsApi_Data_WeaponType weaponType;
            private string modelName;
            private Framework_Utility_Timer timer;
            private ActorPack_Actor_MergeBlockCollection mergeBlock;
            private Framework_Actor_MutationObserver mergeBlockObserver;
            private FcsApi_Component_IWeaponMovement movement;
            private FcsApi_Component_IWeaponRadar radar;
            private FcsApi_Component_IWeaponWarhead warhead;
            private Framework_Component_Anchor anchor;
            private float decoupleTime;
            private double detonationDistance;

            public Fcs_Component_Weapon(
                string id,
                string title,
                FcsApi_Data_WeaponType weaponType,
                string modelName,
                Framework_Actor_Data_SearchCriteria mergeBlockSearchCriteria,
                FcsApi_Component_IWeaponMovement movement = null,
                FcsApi_Component_IWeaponRadar radar = null,
                FcsApi_Component_IWeaponWarhead warhead = null,
                Framework_Component_Anchor anchor = null,
                float decoupleTime = 0.5f,
                double detonationDistance = 0) : base(id)
            {
                this.title = title;
                this.weaponType = weaponType;
                this.modelName = modelName;
                timer = ObjectManager.GetObject<Framework_Utility_Timer>();
                mergeBlock = new ActorPack_Actor_MergeBlockCollection(mergeBlockSearchCriteria, true);
                this.movement = movement;
                this.radar = radar;
                this.warhead = warhead;
                this.anchor = anchor;
                this.decoupleTime = decoupleTime;
                this.detonationDistance = detonationDistance;

                mergeBlockObserver = new Framework_Actor_MutationObserver(mergeBlockSearchCriteria, true);
                mergeBlockObserver.FunctionalityChanged += Child_FunctionalityChanged;

                if (movement != null)
                {
                    movement.FunctionalityChanged += Child_FunctionalityChanged;
                }

                if (radar != null)
                {
                    radar.TargetLocked += Radar_TargetLocked;
                    radar.TargetChanged += Radar_TargetChanged;
                    radar.FunctionalityChanged += Child_FunctionalityChanged;
                }

                if (warhead != null)
                {
                    warhead.Detonated += Warhead_Detonated;
                    warhead.FunctionalityChanged += Child_FunctionalityChanged;
                }

                if (anchor != null)
                {
                    anchor.PositionChanged += Anchor_PositionChanged;
                    anchor.FunctionalityChanged += Child_FunctionalityChanged;
                }

                isFunctional = IsFunctionalImpl();

                SetDescription("Fcs_Component_Weapon");
            }

            public void Detonate()
            {
                if (state == State.Launched)
                {
                    if (warhead != null)
                    {
                        warhead.Detonate();
                    }
                    else
                    {
                        state = State.Detonated;
                        Detonated?.Invoke(this, new EventArgs());
                    }
                }
            }

            public override string GetContextName()
            {
                return "weapon";
            }

            public string GetModelName()
            {
                return modelName;
            }

            public float? GetTimeToHit()
            {
                if (targetInfo != null)
                {
                    if (anchor != null)
                    {
                        if (anchor.IsFunctional())
                        {
                            var targetDistance = (targetInfo.Position - anchor.GetPosition().Value).Length();

                            return (float?)(targetDistance / 100);
                        }
                    }
                }

                return null;
            }

            public string GetTitle()
            {
                return title;
            }

            public FcsApi_Data_WeaponType GetWeaponType()
            {
                return weaponType;
            }

            public bool IsFunctional()
            {
                return isFunctional;
            }

            public bool IsTargetLocked()
            {
                return isTargetLocked;
            }

            public void Launch(FcsApi_Data_GuidanceType guidanceType, FcsApi_Data_TargetInfo targetInfo)
            {
                if (state == State.Disabled)
                {
                    this.guidanceType = guidanceType;
                    this.targetInfo = targetInfo;

                    mergeBlock.Disable();
                    state = State.Launched;

                    if (guidanceType == FcsApi_Data_GuidanceType.None || guidanceType == FcsApi_Data_GuidanceType.GlobalPositioning)
                    {
                        isTargetLocked = true;
                        TargetLocked?.Invoke(this, new EventArgs());
                    }

                    if (movement != null)
                    {
                        if (guidanceType == FcsApi_Data_GuidanceType.BeamRiding || guidanceType == FcsApi_Data_GuidanceType.GlobalPositioning)
                        {
                            movement.Decouple(FcsApi_Data_AttackType.Top, targetInfo);
                        }
                        else
                        {
                            movement.Decouple(FcsApi_Data_AttackType.Direct, targetInfo);
                        }
                    }

                    if (radar != null)
                    {
                        timer.SetTimeout(decoupleTime, () => radar.EnableScan());
                    }

                    if (warhead != null)
                    {
                        timer.SetTimeout(decoupleTime + armTime, warhead.Arm);
                    }

                    UpdateTargetImpl();
                }
            }

            public void UpdateTarget(FcsApi_Data_TargetInfo targetInfo)
            {
                if (!isTargetLocked)
                {
                    this.targetInfo = targetInfo;
                    UpdateTargetImpl();
                }
            }

            public float GetDecoupleTime()
            {
                return decoupleTime;
            }

            protected override void Cleanup()
            {
                mergeBlock.Destruct();
                mergeBlockObserver.Destruct();
            }

            private void Child_FunctionalityChanged(object sender, EventArgs eventArgs)
            {
                var wasFunctional = isFunctional;
                isFunctional = IsFunctionalImpl();

                if (isFunctional != wasFunctional)
                {
                    FunctionalityChanged?.Invoke(this, new EventArgs());
                }
            }

            private void Anchor_PositionChanged(object sender, EventArgs eventArgs)
            {
                UpdateTargetImpl();
            }

            private void Radar_TargetChanged(object sender, EventArgs eventArgs)
            {
                var radar = (FcsApi_Component_IWeaponRadar)sender;

                var targetInfo = radar.GetTargetInfo();

                if (targetInfo != null)
                {
                    this.targetInfo = targetInfo;
                    UpdateTargetImpl();
                }
            }

            private void Radar_TargetLocked(object sender, EventArgs eventArgs)
            {
                isTargetLocked = true;

                TargetLocked?.Invoke(this, new EventArgs());
            }

            private void Warhead_Detonated(object sender, EventArgs eventArgs)
            {
                state = State.Detonated;
                Detonated?.Invoke(this, new EventArgs());
            }

            private bool IsFunctionalImpl()
            {
                return (movement == null || movement.IsFunctional())
                    && (radar == null || radar.IsFunctional())
                    && (warhead == null || warhead.IsFunctional())
                    && (anchor == null || anchor.IsFunctional())
                    && mergeBlockObserver.IsFunctional();
            }

            private void UpdateTargetImpl()
            {
                if (state == State.Launched)
                {
                    if (targetInfo == null)
                    {
                        return;
                    }

                    if (movement != null)
                    {
                        movement.UpdateTarget(targetInfo);
                    }

                    if (radar != null)
                    {
                        if (!isTargetLocked)
                        {
                            if (guidanceType == FcsApi_Data_GuidanceType.ActiveRadarHoming)
                            {
                                radar.LockTarget(targetInfo);
                            }
                        }
                    }

                    if (anchor != null)
                    {
                        if (anchor.IsFunctional())
                        {
                            var targetDistance = (targetInfo.Position - anchor.GetPosition().Value).Length();

                            if (targetDistance <= detonationDistance)
                            {
                                Detonate();
                            }
                        }
                    }

                    TimeToHitChanged?.Invoke(this, new EventArgs());
                }
            }
        }
    }
}
