﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System;
using VRage.Collections;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ObjectBuilders.Definitions;
using VRage.Game;
using VRage;
using VRageMath;

namespace IngameScript
{
    partial class Program
    {
        public class Fcs_Component_Turret : Framework_Component_AbstractComponent, FcsApi_Component_IDetector
        {
            private enum State
            {
                Disabled,
                Locked
            }

            public event EventHandler TargetLocked;
            public event EventHandler TargetLost;
            public event EventHandler TargetChanged;

            private const float timeBeforeLoss = 2;

            private State state = State.Disabled;
            private FcsApi_Data_TargetInfo targetInfo;
            private ActorPack_Actor_TurretCollection turret;
            private Framework_Utility_Timer timer;
            private string timeoutId;

            public Fcs_Component_Turret(
                string id,
                Framework_Actor_Data_SearchCriteria turretSearchCriteria) : base(id)
            {
                timer = ObjectManager.GetObject<Framework_Utility_Timer>();
                turret = new ActorPack_Actor_TurretCollection(turretSearchCriteria);

                turret.EntityDetected += Turret_EntityDetected;

                SetDescription("Fcs_Component_Turret");
            }

            public void EnableScan()
            { }

            public void DisableScan()
            { }

            public bool IsScanEnabled()
            {
                return false;
            }

            public bool IsTargetLocked()
            {
                return state == State.Locked;
            }

            public FcsApi_Data_TargetInfo GetTargetInfo()
            {
                return targetInfo;
            }

            public override string GetContextName()
            {
                return "turret";
            }

            public FcsApi_Data_DetectorType GetDetectorType()
            {
                return FcsApi_Data_DetectorType.Turret;
            }

            private void Turret_EntityDetected(object sender, EventArgs eventArgs)
            {
                targetInfo = new FcsApi_Data_TargetInfo(turret.GetLastEntityInfo(), timer.GetTimeNow());

                if (state == State.Disabled)
                {
                    timeoutId = timer.SetTimeout(timeBeforeLoss, LeaveTarget);
                    state = State.Locked;

                    TargetLocked?.Invoke(this, new EventArgs());
                }
                else if (state == State.Locked)
                {
                    timer.SetTimeout(timeBeforeLoss, timeoutId);
                }

                TargetChanged?.Invoke(this, new EventArgs());
            }

            private void LeaveTarget()
            {
                state = State.Disabled;

                targetInfo = null;

                TargetLost?.Invoke(this, new EventArgs());
            }
        }
    }
}
