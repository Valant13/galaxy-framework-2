﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VRage;
using VRage.Collections;
using VRage.Game;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ObjectBuilders.Definitions;
using VRageMath;

namespace IngameScript
{
    partial class Program
    {
        public class Fcs_Component_CompositeHardpoint : Framework_Component_AbstractComponent, FcsApi_Component_IHardpoint
        {
            public event EventHandler WeaponChanged;
            public event EventHandler ReadinessChanged;

            private int hardpointNumber;
            private List<Fcs_Component_DynamicHardpoint> hardpoints;
            private FcsApi_Data_HardpointPosition position;

            public Fcs_Component_CompositeHardpoint(
                string id,
                Fcs_Component_DynamicHardpoint[] hardpoints,
                FcsApi_Data_HardpointPosition position) : base(id)
            {
                this.hardpoints = hardpoints.ToList();
                this.position = position;

                foreach (var hardpoint in hardpoints)
                {
                    hardpoint.ReadinessChanged += Hardpoint_ReadinessChanged;
                    hardpoint.WeaponChanged += Hardpoint_WeaponChanged;
                }

                SetDescription("Fcs_Component_CompositeHardpoint");
            }

            public override string GetContextName()
            {
                return "composite-hardpoint";
            }

            public void UpdateWeapon()
            {
                foreach (var hardpoint in hardpoints)
                {
                    hardpoint.UpdateWeapon();
                }
            }

            public FcsApi_Component_IWeapon GetWeapon()
            {
                return hardpoints[hardpointNumber].GetWeapon();
            }

            public void SelectNextWeapon()
            {
                foreach (var hardpoint in hardpoints)
                {
                    hardpoint.SelectNextWeapon();
                }
            }

            public bool IsReady()
            {
                return hardpoints[hardpointNumber].IsReady();
            }

            public void LaunchWeapon(FcsApi_Data_GuidanceType guidanceType, FcsApi_Data_TargetInfo targetInfo)
            {
                var hardpoint = hardpoints[hardpointNumber];

                if (hardpoint.IsReady())
                {
                    hardpointNumber = hardpointNumber + 1 < hardpoints.Count() ? hardpointNumber + 1 : 0;

                    hardpoint.LaunchWeapon(guidanceType, targetInfo);

                    WeaponChanged?.Invoke(this, new EventArgs());
                    ReadinessChanged?.Invoke(this, new EventArgs());
                }
            }

            public FcsApi_Data_HardpointPosition GetPosition()
            {
                return position;
            }

            public FcsApi_Data_HardpointType GetHardpointType()
            {
                return FcsApi_Data_HardpointType.Dynamic;
            }

            private void Hardpoint_WeaponChanged(object sender, EventArgs eventArgs)
            {
                if (sender == hardpoints[hardpointNumber])
                {
                    WeaponChanged?.Invoke(this, new EventArgs());
                }
            }

            private void Hardpoint_ReadinessChanged(object sender, EventArgs eventArgs)
            {
                if (sender == hardpoints[hardpointNumber])
                {
                    ReadinessChanged?.Invoke(this, new EventArgs());
                }
            }
        }
    }
}
