﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System;
using VRage.Collections;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ObjectBuilders.Definitions;
using VRage.Game;
using VRage;
using VRageMath;

namespace IngameScript
{
    partial class Program
    {
        public class Fcs_Component_Radar : Framework_Component_AbstractComponent, FcsApi_Component_IDetector, FcsApi_Component_IWeaponRadar
        {
            private enum State
            {
                Disabled,
                Scanning,
                Locking,
                Locked
            }

            public event EventHandler TargetLocked;
            public event EventHandler TargetLost;
            public event EventHandler TargetChanged;
            public event EventHandler FunctionalityChanged;

            private State state = State.Disabled;
            private Framework_Component_Anchor anchor;
            private Fcs_ViewModel_RaycastBased viewModel;
            private Framework_Utility_Timer timer;
            private ActorPack_Actor_CameraCollection camera;
            private Framework_Actor_MutationObserver cameraObserver;
            private long? targetEntityId;
            private FcsApi_Data_TargetInfo targetInfo;
            private double raycastDistance;
            private int attemptsBeforeLoss;
            private int failedAttemptsCount = 0;

            public Fcs_Component_Radar(
                string id,
                Framework_Actor_Data_SearchCriteria cameraSearchCriteria,
                Framework_Component_Anchor anchor = null,
                Fcs_ViewModel_RaycastBased viewModel = null,
                double distance = 1000,
                int attemptsBeforeLoss = 3) : base(id)
            {
                timer = ObjectManager.GetObject<Framework_Utility_Timer>();
                this.anchor = anchor;
                this.viewModel = viewModel;
                raycastDistance = distance;
                this.attemptsBeforeLoss = attemptsBeforeLoss;

                camera = new ActorPack_Actor_CameraCollection(cameraSearchCriteria);
                camera.EntityDetected += Camera_TargetDetected;

                cameraObserver = new Framework_Actor_MutationObserver(cameraSearchCriteria);
                cameraObserver.FunctionalityChanged += Child_FunctionalityChanged;

                if (anchor != null)
                {
                    anchor.PositionChanged += Anchor_MovementChanged;
                    anchor.WorldMatrixChanged += Anchor_MovementChanged;
                    anchor.FunctionalityChanged += Child_FunctionalityChanged;
                }

                SetDescription("Fcs_Component_Radar");

                UpdateViewModel();
            }

            public void EnableScan()
            {
                if (state == State.Disabled)
                {
                    state = State.Scanning;

                    camera.EnableRaycast(raycastDistance);
                    UpdateScan();

                    UpdateViewModel();
                }
            }

            public void DisableScan()
            {
                if (state == State.Scanning || state == State.Locked)
                {
                    targetInfo = null;

                    if (state == State.Locked)
                    {
                        TargetLost?.Invoke(this, new EventArgs());
                    }

                    state = State.Disabled;
                    TargetChanged?.Invoke(this, new EventArgs());

                    camera.DisableRaycast();

                    UpdateViewModel();
                }
            }

            public void LockTarget(FcsApi_Data_TargetInfo targetInfo)
            {
                if (state == State.Scanning)
                {
                    state = State.Locking;

                    UpdateLock(targetInfo);
                }
            }

            public bool IsScanEnabled()
            {
                return state != State.Disabled;
            }

            public bool IsTargetLocked()
            {
                return state == State.Locked;
            }

            public bool IsFunctional()
            {
                return (anchor == null || anchor.IsFunctional()) && cameraObserver.IsFunctional();
            }

            public FcsApi_Data_TargetInfo GetTargetInfo()
            {
                return targetInfo;
            }

            public override string GetContextName()
            {
                return "radar";
            }

            public FcsApi_Data_DetectorType GetDetectorType()
            {
                return FcsApi_Data_DetectorType.Radar;
            }

            protected override void Cleanup()
            {
                camera.Destruct();
                cameraObserver.Destruct();
            }

            private void Child_FunctionalityChanged(object sender, EventArgs eventArgs)
            {
                FunctionalityChanged?.Invoke(this, new EventArgs());
            }

            private void Anchor_MovementChanged(object sender, EventArgs eventArgs)
            {
                if (state == State.Scanning)
                {
                    UpdateScan();
                }
            }

            private void Camera_TargetDetected(object sender, EventArgs eventArgs)
            {
                if (state == State.Scanning || state == State.Locking)
                {
                    var entityInfo = camera.GetLastEntityInfo();

                    if (IsTargetInfoValid(entityInfo))
                    {
                        failedAttemptsCount = 0;

                        targetEntityId = entityInfo.EntityId;
                        targetInfo = new FcsApi_Data_TargetInfo(entityInfo, timer.GetTimeNow());
                        UpdateLock(targetInfo);

                        state = State.Locked;
                        TargetLocked?.Invoke(this, new EventArgs());
                        TargetChanged?.Invoke(this, new EventArgs());
                    }
                }
                else if (state == State.Locked)
                {
                    var entityInfo = camera.GetLastEntityInfo();

                    if (IsTargetInfoValid(entityInfo) && entityInfo.EntityId == targetEntityId.Value)
                    {
                        failedAttemptsCount = 0;
                        
                        targetInfo = new FcsApi_Data_TargetInfo(entityInfo, timer.GetTimeNow());
                        UpdateLock(targetInfo);

                        TargetChanged?.Invoke(this, new EventArgs());
                    }
                    else
                    {
                        failedAttemptsCount++;

                        if (failedAttemptsCount > attemptsBeforeLoss)
                        {
                            state = State.Scanning;
                            targetEntityId = null;
                            targetInfo = null;

                            UpdateScan();

                            TargetLost?.Invoke(this, new EventArgs());
                            TargetChanged?.Invoke(this, new EventArgs());
                        }
                        else
                        {
                            UpdateLock(targetInfo);
                        }
                    }
                }
            }

            private void UpdateScan()
            {
                if (anchor.IsFunctional())
                {
                    var raycastPosition = anchor.GetPosition().Value + anchor.GetWorldMatrix().Value.Forward * raycastDistance;

                    camera.SetRaycastPosition(raycastPosition);
                }
                else
                {
                    camera.ResetRaycastPosition();
                }
            }

            private void UpdateLock(FcsApi_Data_TargetInfo targetInfo)
            {
                var timeNow = timer.GetTimeNow();
                var timeDiffirence = timeNow - targetInfo.DetectionTime + camera.GetRaycastIntervalTime();

                var targetNextPosition = targetInfo.Position + targetInfo.Velocity * timeDiffirence;

                camera.SetRaycastPosition(targetNextPosition);
            }

            private bool IsTargetInfoValid(MyDetectedEntityInfo targetInfo)
            {
                if (targetInfo.IsEmpty())
                {
                    return false;
                }

                if (targetInfo.Type != MyDetectedEntityType.SmallGrid && targetInfo.Type != MyDetectedEntityType.LargeGrid)
                {
                    return false;
                }

                return true;
            }

            private void UpdateViewModel()
            {
                if (viewModel != null)
                {
                    viewModel.IntervalTime.RenderValue(camera.GetRaycastIntervalTime());
                    viewModel.Distance.RenderValue(camera.GetRaycastDistance());
                }
            }
        }
    }
}
