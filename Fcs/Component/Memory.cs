﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System;
using VRage.Collections;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ObjectBuilders.Definitions;
using VRage.Game;
using VRage;
using VRageMath;

namespace IngameScript
{
    partial class Program
    {
        public class Fcs_Component_Memory : Framework_Component_AbstractComponent, FcsApi_Component_IDetector
        {
            public event EventHandler TargetLocked;
            public event EventHandler TargetLost;
            public event EventHandler TargetChanged;

            private Fcs_ViewModel_Memory viewModel;
            private FcsApi_Data_TargetInfo targetInfo;
            private FcsApi_Component_IDetector targetSource;

            public Fcs_Component_Memory(
                string id,
                FcsApi_Component_IDetector targetSource,
                Fcs_ViewModel_Memory viewModel = null) : base(id)
            {
                this.targetSource = targetSource;
                this.viewModel = viewModel;

                targetSource.TargetChanged += TargetSource_TargetChanged;

                SetDescription("Fcs_Component_Memory");

                UpdateViewModel();
            }

            public void EnableScan()
            { }

            public void DisableScan()
            { }

            public bool IsScanEnabled()
            {
                return false;
            }

            public bool IsTargetLocked()
            {
                return false;
            }

            public FcsApi_Data_TargetInfo GetTargetInfo()
            {
                return targetInfo;
            }

            public void SetTargetInfo(FcsApi_Data_TargetInfo targetInfo)
            {
                this.targetInfo = targetInfo;

                TargetChanged.Invoke(this, new EventArgs());

                UpdateViewModel();
            }

            public override string GetContextName()
            {
                return "memory";
            }

            public FcsApi_Data_DetectorType GetDetectorType()
            {
                return FcsApi_Data_DetectorType.Memory;
            }

            private void TargetSource_TargetChanged(object sender, EventArgs eventArgs)
            {
                var targetInfo = targetSource.GetTargetInfo();

                if (targetInfo != null)
                {
                    this.targetInfo = targetInfo;

                    TargetChanged.Invoke(this, new EventArgs());

                    UpdateViewModel();
                }
            }

            private void UpdateViewModel()
            {
                if (viewModel != null)
                {
                    viewModel.TargetPosition.SetValue(targetInfo == null ? null : new Vector3I(targetInfo.Position).ToString());
                }
            }
        }
    }
}
