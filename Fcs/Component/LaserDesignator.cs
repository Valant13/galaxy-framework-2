﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System;
using VRage.Collections;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ObjectBuilders.Definitions;
using VRage.Game;
using VRage;
using VRageMath;

namespace IngameScript
{
    partial class Program
    {
        public class Fcs_Component_LaserDesignator : Framework_Component_AbstractComponent, FcsApi_Component_IDetector
        {
            private enum State
            {
                Disabled,
                Scanning,
                Locked
            }

            public event EventHandler TargetLocked;
            public event EventHandler TargetLost;
            public event EventHandler TargetChanged;

            private State state = State.Disabled;
            private Framework_Component_Anchor anchor;
            private Fcs_ViewModel_RaycastBased viewModel;
            private Framework_Utility_Timer timer;
            private ActorPack_Actor_CameraCollection camera;
            private FcsApi_Data_TargetInfo targetInfo;
            private double raycastDistance;

            public Fcs_Component_LaserDesignator(
                string id,
                Framework_Actor_Data_SearchCriteria cameraSearchCriteria,
                Framework_Component_Anchor anchor = null,
                Fcs_ViewModel_RaycastBased viewModel = null,
                double distance = 1000) : base(id)
            {
                timer = ObjectManager.GetObject<Framework_Utility_Timer>();
                this.anchor = anchor;
                this.viewModel = viewModel;
                raycastDistance = distance;

                camera = new ActorPack_Actor_CameraCollection(cameraSearchCriteria);
                camera.EntityDetected += Camera_TargetDetected;

                if (anchor != null)
                {
                    anchor.PositionChanged += Anchor_MovementChanged;
                    anchor.WorldMatrixChanged += Anchor_MovementChanged;
                }

                SetDescription("Fcs_Component_LaserDesignator");

                UpdateViewModel();
            }

            public void EnableScan()
            {
                if (state == State.Disabled)
                {
                    state = State.Scanning;

                    camera.EnableRaycast(raycastDistance);
                    UpdateScan();

                    UpdateViewModel();
                }
            }

            public void DisableScan()
            {
                if (state == State.Scanning || state == State.Locked)
                {
                    targetInfo = null;

                    if (state == State.Locked)
                    {
                        TargetLost?.Invoke(this, new EventArgs());
                    }

                    state = State.Disabled;
                    TargetChanged?.Invoke(this, new EventArgs());

                    camera.DisableRaycast();

                    UpdateViewModel();
                }
            }

            public bool IsScanEnabled()
            {
                return state != State.Disabled;
            }

            public bool IsTargetLocked()
            {
                return state == State.Locked;
            }

            public FcsApi_Data_TargetInfo GetTargetInfo()
            {
                return targetInfo;
            }

            public override string GetContextName()
            {
                return "laser-designator";
            }

            public FcsApi_Data_DetectorType GetDetectorType()
            {
                return FcsApi_Data_DetectorType.LaserDesignator;
            }

            protected override void Cleanup()
            {
                camera.Destruct();
            }

            private void Anchor_MovementChanged(object sender, EventArgs eventArgs)
            {
                if (state == State.Scanning || state == State.Locked)
                {
                    UpdateScan();
                }
            }

            private void Camera_TargetDetected(object sender, EventArgs eventArgs)
            {
                if (state == State.Scanning)
                {
                    var entityInfo = camera.GetLastEntityInfo();

                    if (!entityInfo.IsEmpty())
                    {
                        targetInfo = new FcsApi_Data_TargetInfo(entityInfo.HitPosition.Value, Vector3D.Zero, timer.GetTimeNow());

                        state = State.Locked;
                        TargetLocked?.Invoke(this, new EventArgs());
                        TargetChanged?.Invoke(this, new EventArgs());
                    }
                }
                else if (state == State.Locked)
                {
                    var entityInfo = camera.GetLastEntityInfo();

                    if (!entityInfo.IsEmpty())
                    {
                        targetInfo = new FcsApi_Data_TargetInfo(entityInfo.HitPosition.Value, Vector3D.Zero, timer.GetTimeNow());

                        TargetChanged?.Invoke(this, new EventArgs());
                    }
                    else
                    {
                        state = State.Scanning;
                        targetInfo = null;

                        TargetLost?.Invoke(this, new EventArgs());
                        TargetChanged?.Invoke(this, new EventArgs());
                    }
                }
            }

            private void UpdateScan()
            {
                if (anchor.IsFunctional())
                {
                    var raycastPosition = anchor.GetPosition().Value + anchor.GetWorldMatrix().Value.Forward * raycastDistance;

                    camera.SetRaycastPosition(raycastPosition);
                }
                else
                {
                    camera.ResetRaycastPosition();
                }
            }

            private void UpdateViewModel()
            {
                if (viewModel != null)
                {
                    viewModel.IntervalTime.RenderValue(camera.GetRaycastIntervalTime());
                    viewModel.Distance.RenderValue(camera.GetRaycastDistance());
                }
            }
        }
    }
}
