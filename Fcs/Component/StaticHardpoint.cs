﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VRage;
using VRage.Collections;
using VRage.Game;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ObjectBuilders.Definitions;
using VRageMath;

namespace IngameScript
{
    partial class Program
    {
        public class Fcs_Component_StaticHardpoint : Framework_Component_AbstractComponent, FcsApi_Component_IHardpoint
        {
            public event EventHandler WeaponChanged;
            public event EventHandler ReadinessChanged;

            private ActorPack_Actor_MergeBlock mergeBlock;
            private FcsApi_Component_IWeapon weapon;
            private FcsApi_Data_HardpointPosition position;
            private BoundingBoxI weaponBoundingBox;
            private Framework_Actor_CacheService cacheService;
            private Framework_Actor_BlockService blockService;
            private FcsApi_IWeaponFactory weaponFactory;
            private FcsApi_WeaponBlueprintFactory blueprintFactory;
            private Fcs_MergeBlockService mergeBlockService;

            public Fcs_Component_StaticHardpoint(
                string id,
                Framework_Actor_Data_SearchCriteria mergeBlockSearchCriteria,
                FcsApi_Data_HardpointPosition position,
                BoundingBoxI? weaponBoundingBox = null) : base(id)
            {
                this.position = position;
                cacheService = ObjectManager.GetObject<Framework_Actor_CacheService>();
                blockService = ObjectManager.GetObject<Framework_Actor_BlockService>();
                weaponFactory = ObjectManager.GetObject<FcsApi_IWeaponFactory>();
                blueprintFactory = ObjectManager.GetObject<FcsApi_WeaponBlueprintFactory>();
                mergeBlockService = ObjectManager.GetObject<Fcs_MergeBlockService>();
                mergeBlock = new ActorPack_Actor_MergeBlock(mergeBlockSearchCriteria);

                // Bounding box min point: 3 11 2
                // Bounding box max point: 1 -9 0
                //
                // Merge block offset: 0 1 -1

                this.weaponBoundingBox = weaponBoundingBox ?? new BoundingBoxI(new Vector3I(1, -10, -1), new Vector3I(3, 10, 1));

                SetDescription("Fcs_Component_StaticHardpoint");

                UpdateWeapon();
            }

            public override string GetContextName()
            {
                return "static-hardpoint";
            }

            public void UpdateWeapon()
            {
                if (weapon != null)
                {
                    weapon.Destruct();
                    weapon = null;
                }

                cacheService.UpdateBlockCache();

                var weaponMergeBlock = mergeBlock.GetConnectedMergeBlock();
                if (weaponMergeBlock != null)
                {
                    var dataIni = blockService.GetBlockCustomDataIni(weaponMergeBlock);
                    if (dataIni != null && dataIni.ContainsSection(FcsApi_WeaponBlueprintFactory.Section))
                    {
                        var blueprint = blueprintFactory.CreateBlueprintFromIni(dataIni);
                        var adjustedBoundingBox = mergeBlockService.TransformBoundingBox(weaponBoundingBox, mergeBlock.GetBlock());

                        weapon = weaponFactory.CreateWeapon(blueprint, adjustedBoundingBox);
                        weapon.FunctionalityChanged += Weapon_FunctionalityChanged;
                    }
                }

                WeaponChanged?.Invoke(this, new EventArgs());
                ReadinessChanged?.Invoke(this, new EventArgs());
            }

            public FcsApi_Component_IWeapon GetWeapon()
            {
                return weapon;
            }

            public bool IsReady()
            {
                return weapon != null && weapon.IsFunctional();
            }

            public void LaunchWeapon(FcsApi_Data_GuidanceType guidanceType, FcsApi_Data_TargetInfo targetInfo)
            {
                if (!IsReady())
                {
                    return;
                }

                weapon.Launch(guidanceType, targetInfo);
                weapon.FunctionalityChanged -= Weapon_FunctionalityChanged;
                weapon = null;

                WeaponChanged?.Invoke(this, new EventArgs());
                ReadinessChanged?.Invoke(this, new EventArgs());
            }

            public FcsApi_Data_HardpointPosition GetPosition()
            {
                return position;
            }

            public FcsApi_Data_HardpointType GetHardpointType()
            {
                return FcsApi_Data_HardpointType.Static;
            }

            private void Weapon_FunctionalityChanged(object sender, EventArgs eventArgs)
            {
                ReadinessChanged?.Invoke(this, new EventArgs());
            }
        }
    }
}
