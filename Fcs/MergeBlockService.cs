﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VRage;
using VRage.Collections;
using VRage.Game;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ObjectBuilders.Definitions;
using VRageMath;

namespace IngameScript
{
    partial class Program
    {
        public class Fcs_MergeBlockService
        {
            public BoundingBoxI TransformBoundingBox(BoundingBoxI localBoundingBox, IMyShipMergeBlock mergeBlock)
            {
                var minPoint = localBoundingBox.Min;
                var maxPoint = localBoundingBox.Max;

                var transformation = new MatrixI(mergeBlock.Orientation);
                transformation.Translation = (mergeBlock.Min + mergeBlock.Max) / 2;
                // TODO: check if mergeBlock.Position can be used instead

                var adjustedMinPoint = Vector3I.Transform(minPoint, ref transformation);
                var adjustedMaxPoint = Vector3I.Transform(maxPoint, ref transformation);

                return BoundingBoxI.CreateFromPoints(new[] { adjustedMinPoint, adjustedMaxPoint });
            }
        }
    }
}
