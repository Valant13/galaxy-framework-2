﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VRage;
using VRage.Collections;
using VRage.Game;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ObjectBuilders.Definitions;
using VRageMath;

namespace IngameScript
{
    partial class Program
    {
        public class Fcs_WeaponFactory : FcsApi_IWeaponFactory
        {
            private const float defaultDecoupleTime = 0.5f;
            private const double defaultRaycastDistance = 1000;
            private const double defaultDetonationDistance = 0;

            private Association<FcsApi_IWeaponMovementFactory> movementFactories;
            private Association<FcsApi_IWeaponRadarFactory> radarFactories;
            private Association<FcsApi_IWeaponWarheadFactory> warheadFactories;

            public Fcs_WeaponFactory(
                Association<FcsApi_IWeaponMovementFactory> movements,
                Association<FcsApi_IWeaponRadarFactory> radars,
                Association<FcsApi_IWeaponWarheadFactory> warheads)
            {
                movementFactories = movements;
                radarFactories = radars;
                warheadFactories = warheads;
            }

            public FcsApi_Component_IWeapon CreateWeapon(FcsApi_Data_WeaponBlueprint blueprint, BoundingBoxI boundingBox)
            {
                if (string.IsNullOrEmpty(blueprint.Title))
                {
                    throw new Exception("Weapon title is missing");
                }

                if (string.IsNullOrEmpty(blueprint.ModelName))
                {
                    throw new Exception("Weapon model is missing");
                }

                var searchCriteriaBuilder = new Framework_Actor_SearchCriteriaBuilder();

                var mergeBlock = searchCriteriaBuilder
                    .SetCustomClass("WeaponMergeBlock")
                    .SetBoundingBox(boundingBox)
                    .Build();

                var anchor = GetAnchor(blueprint, boundingBox);
                var movement = GetMovement(blueprint, anchor, boundingBox);
                var radar = GetRadar(blueprint, anchor, boundingBox);
                var warhead = GetWarhead(blueprint, anchor, boundingBox);

                var weapon = new Fcs_Component_Weapon(
                    null,
                    blueprint.Title,
                    blueprint.WeaponType ?? FcsApi_Data_WeaponType.Unguided,
                    blueprint.ModelName,
                    mergeBlock,
                    movement,
                    radar,
                    warhead,
                    anchor,
                    blueprint.DecoupleTime ?? defaultDecoupleTime,
                    blueprint.DetonationDistance ?? defaultDetonationDistance);

                weapon.Destructed += (sender, eventArgs) => {
                    movement?.Destruct();
                    radar?.Destruct();
                    warhead?.Destruct();
                    anchor?.Destruct();
                };

                return weapon;
            }

            private FcsApi_Component_IWeaponMovement GetMovement(
                FcsApi_Data_WeaponBlueprint blueprint,
                Framework_Component_Anchor anchor,
                BoundingBoxI boundingBox)
            {
                var movementName = blueprint.MovementName;

                if (movementName != null)
                {
                    if (!movementFactories.ContainsKey(movementName))
                    {
                        throw new Exception($"Movement {movementName} does not exist");
                    }

                    return movementFactories[movementName].CreateMovement(anchor, boundingBox, blueprint.DecoupleTime ?? defaultDecoupleTime);
                }

                return null;
            }

            private FcsApi_Component_IWeaponRadar GetRadar(
                FcsApi_Data_WeaponBlueprint blueprint,
                Framework_Component_Anchor anchor,
                BoundingBoxI boundingBox)
            {
                var radarName = blueprint.RadarName;

                if (radarName != null)
                {
                    if (!radarFactories.ContainsKey(radarName))
                    {
                        throw new Exception($"Radar {radarName} does not exist");
                    }

                    return radarFactories[radarName].CreateRadar(anchor, boundingBox, blueprint.RaycastDistance ?? defaultRaycastDistance);
                }

                return null;
            }

            private FcsApi_Component_IWeaponWarhead GetWarhead(
                FcsApi_Data_WeaponBlueprint blueprint,
                Framework_Component_Anchor anchor,
                BoundingBoxI boundingBox)
            {
                var warheadName = blueprint.WarheadName;

                if (warheadName != null)
                {
                    if (!warheadFactories.ContainsKey(warheadName))
                    {
                        throw new Exception($"Warhead {warheadName} does not exist");
                    }

                    return warheadFactories[warheadName].CreateWarhead(anchor, boundingBox);
                }

                return null;
            }

            private Framework_Component_Anchor GetAnchor(FcsApi_Data_WeaponBlueprint blueprint, BoundingBoxI boundingBox)
            {
                var searchCriteriaBuilder = new Framework_Actor_SearchCriteriaBuilder();

                if (blueprint.HasAnchor)
                {
                    var anchorSearchCriteria = searchCriteriaBuilder
                        .SetCustomClass("WeaponAnchor")
                        .SetBoundingBox(boundingBox)
                        .Build();

                    return new Framework_Component_Anchor(null, anchorSearchCriteria, true);
                }

                return null;
            }
        }
    }
}
