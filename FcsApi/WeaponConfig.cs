﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VRage;
using VRage.Collections;
using VRage.Game;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ObjectBuilders.Definitions;
using VRageMath;

namespace IngameScript
{
    partial class Program
    {
        public class FcsApi_WeaponConfig
        {
            private Dictionary<FcsApi_Data_WeaponType, FcsApi_Data_WeaponInfo> weaponInfos = new Dictionary<FcsApi_Data_WeaponType, FcsApi_Data_WeaponInfo>();

            public FcsApi_WeaponConfig()
            {
                AddWeaponInfo(FcsApi_Data_WeaponType.Unguided, new[] {
                    FcsApi_Data_GuidanceType.None
                });

                AddWeaponInfo(FcsApi_Data_WeaponType.AntiTankGuided, new[] {
                    FcsApi_Data_GuidanceType.BeamRiding,
                    FcsApi_Data_GuidanceType.GlobalPositioning
                });

                AddWeaponInfo(FcsApi_Data_WeaponType.AntiAircraftGuided, new[] {
                    FcsApi_Data_GuidanceType.SemiactiveRadarHoming
                });

                AddWeaponInfo(FcsApi_Data_WeaponType.UniversalGuided, new[] {
                    FcsApi_Data_GuidanceType.SemiactiveRadarHoming,
                    FcsApi_Data_GuidanceType.BeamRiding,
                    FcsApi_Data_GuidanceType.GlobalPositioning
                });

                AddWeaponInfo(FcsApi_Data_WeaponType.ActiveAntiAircraftGuided, new[] {
                    FcsApi_Data_GuidanceType.ActiveRadarHoming
                });

                AddWeaponInfo(FcsApi_Data_WeaponType.ActiveUniversalGuided, new[] {
                    FcsApi_Data_GuidanceType.ActiveRadarHoming,
                    FcsApi_Data_GuidanceType.BeamRiding,
                    FcsApi_Data_GuidanceType.GlobalPositioning
                });
            }

            public FcsApi_Data_WeaponInfo GetWeaponInfo(FcsApi_Data_WeaponType weaponType)
            {
                return weaponInfos[weaponType];
            }

            public Dictionary<FcsApi_Data_WeaponType, FcsApi_Data_WeaponInfo> GetWeaponInfos()
            {
                var copy = new Dictionary<FcsApi_Data_WeaponType, FcsApi_Data_WeaponInfo>();

                foreach (var pair in weaponInfos)
                {
                    copy[pair.Key] = pair.Value;
                }

                return copy;
            }

            private void AddWeaponInfo(FcsApi_Data_WeaponType weaponType, FcsApi_Data_GuidanceType[] guidanceTypes)
            {
                var weaponInfo = new FcsApi_Data_WeaponInfo();
                weaponInfo.WeaponType = weaponType;
                weaponInfo.GuidanceTypes = guidanceTypes.ToList();

                weaponInfos[weaponType] = weaponInfo;
            }
        }
    }
}
