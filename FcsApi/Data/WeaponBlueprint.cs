﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VRage;
using VRage.Collections;
using VRage.Game;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ObjectBuilders.Definitions;
using VRageMath;

namespace IngameScript
{
    partial class Program
    {
        public class FcsApi_Data_WeaponBlueprint
        {
            public string Title { get; set; }
            public FcsApi_Data_WeaponType? WeaponType { get; set; }
            public string ModelName { get; set; }
            public string MovementName { get; set; }
            public string RadarName { get; set; }
            public string WarheadName { get; set; }
            public bool HasAnchor { get; set; }
            public float? DecoupleTime { get; set; }
            public double? RaycastDistance { get; set; }
            public double? DetonationDistance { get; set; }
        }
    }
}
