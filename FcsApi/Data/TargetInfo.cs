﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VRage;
using VRage.Collections;
using VRage.Game;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ObjectBuilders.Definitions;
using VRageMath;

namespace IngameScript
{
    partial class Program
    {
        public class FcsApi_Data_TargetInfo
        {
            public Vector3D Position { get; set; }
            public Vector3D Velocity { get; set; }
            public float DetectionTime { get; set; }

            public FcsApi_Data_TargetInfo()
            { }

            public FcsApi_Data_TargetInfo(MyDetectedEntityInfo entityInfo, float detectionTime)
            {
                Position = entityInfo.Position;
                Velocity = entityInfo.Velocity;
                DetectionTime = detectionTime;
            }

            public FcsApi_Data_TargetInfo(Vector3D position, Vector3D velocity, float detectionTime)
            {
                Position = position;
                Velocity = velocity;
                DetectionTime = detectionTime;
            }
        }
    }
}
