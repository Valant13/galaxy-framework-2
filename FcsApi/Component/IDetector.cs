﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VRage;
using VRage.Collections;
using VRage.Game;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ObjectBuilders.Definitions;
using VRageMath;

namespace IngameScript
{
    partial class Program
    {
        public interface FcsApi_Component_IDetector : Framework_Component_IComponent
        {
            event EventHandler TargetLocked;

            event EventHandler TargetChanged;

            event EventHandler TargetLost;

            void EnableScan();

            void DisableScan();

            bool IsScanEnabled();

            bool IsTargetLocked();

            FcsApi_Data_TargetInfo GetTargetInfo();

            FcsApi_Data_DetectorType GetDetectorType();
        }
    }
}
