﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VRage;
using VRage.Collections;
using VRage.Game;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ObjectBuilders.Definitions;
using VRageMath;

namespace IngameScript
{
    partial class Program
    {
        public interface FcsApi_Component_IWeapon : Framework_Component_IComponent
        {
            event EventHandler FunctionalityChanged;

            event EventHandler Detonated;

            event EventHandler TargetLocked;

            event EventHandler TimeToHitChanged;

            void Launch(FcsApi_Data_GuidanceType guidanceType, FcsApi_Data_TargetInfo targetInfo);

            void UpdateTarget(FcsApi_Data_TargetInfo targetInfo);

            bool IsFunctional();

            void Detonate();

            FcsApi_Data_WeaponType GetWeaponType();

            string GetModelName();

            string GetTitle();

            bool IsTargetLocked();

            float? GetTimeToHit();

            float GetDecoupleTime();
        }
    }
}
