﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VRage;
using VRage.Collections;
using VRage.Game;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ObjectBuilders.Definitions;
using VRageMath;

namespace IngameScript
{
    partial class Program
    {
        public interface FcsApi_Component_IHardpoint : Framework_Component_IComponent
        {
            event EventHandler WeaponChanged;

            event EventHandler ReadinessChanged;

            void LaunchWeapon(FcsApi_Data_GuidanceType guidanceType, FcsApi_Data_TargetInfo targetInfo);

            void UpdateWeapon();

            FcsApi_Component_IWeapon GetWeapon();

            bool IsReady();

            FcsApi_Data_HardpointPosition GetPosition();

            FcsApi_Data_HardpointType GetHardpointType();
        }
    }
}
