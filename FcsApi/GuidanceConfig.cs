﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VRage;
using VRage.Collections;
using VRage.Game;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ObjectBuilders.Definitions;
using VRageMath;

namespace IngameScript
{
    partial class Program
    {
        public class FcsApi_GuidanceConfig
        {
            private Dictionary<FcsApi_Data_GuidanceType, FcsApi_Data_GuidanceInfo> guidanceInfos = new Dictionary<FcsApi_Data_GuidanceType, FcsApi_Data_GuidanceInfo>();

            public FcsApi_GuidanceConfig()
            {
                AddGuidanceInfo(FcsApi_Data_GuidanceType.None, new[] {
                    FcsApi_Data_DetectorType.None
                });

                AddGuidanceInfo(FcsApi_Data_GuidanceType.ActiveRadarHoming, new[] {
                    FcsApi_Data_DetectorType.Radar,
                    FcsApi_Data_DetectorType.Turret
                });

                AddGuidanceInfo(FcsApi_Data_GuidanceType.SemiactiveRadarHoming, new[] {
                    FcsApi_Data_DetectorType.Radar,
                    FcsApi_Data_DetectorType.Turret
                });

                AddGuidanceInfo(FcsApi_Data_GuidanceType.BeamRiding, new[] {
                    FcsApi_Data_DetectorType.Radar,
                    FcsApi_Data_DetectorType.Turret,
                    FcsApi_Data_DetectorType.LaserDesignator
                });

                AddGuidanceInfo(FcsApi_Data_GuidanceType.GlobalPositioning, new[] {
                    FcsApi_Data_DetectorType.LaserDesignator,
                    FcsApi_Data_DetectorType.Memory
                });
            }

            public FcsApi_Data_GuidanceInfo GetGuidanceInfo(FcsApi_Data_GuidanceType guidanceType)
            {
                return guidanceInfos[guidanceType];
            }

            public Dictionary<FcsApi_Data_GuidanceType, FcsApi_Data_GuidanceInfo> GetGuidanceInfos()
            {
                var copy = new Dictionary<FcsApi_Data_GuidanceType, FcsApi_Data_GuidanceInfo>();

                foreach (var pair in guidanceInfos)
                {
                    copy[pair.Key] = pair.Value;
                }

                return copy;
            }

            private void AddGuidanceInfo(FcsApi_Data_GuidanceType guidanceType, FcsApi_Data_DetectorType[] detectorTypes)
            {
                var guidanceInfo = new FcsApi_Data_GuidanceInfo();
                guidanceInfo.GuidanceType = guidanceType;
                guidanceInfo.DetectorTypes = detectorTypes.ToList();

                guidanceInfos[guidanceType] = guidanceInfo;
            }
        }
    }
}
