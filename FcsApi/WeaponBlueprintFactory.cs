﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VRage;
using VRage.Collections;
using VRage.Game;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ObjectBuilders.Definitions;
using VRageMath;

namespace IngameScript
{
    partial class Program
    {
        public class FcsApi_WeaponBlueprintFactory
        {
            public const string Section = "Weapon";
            public const string TitleKey = "Title";
            public const string TypeKey = "Type";
            public const string ModelNameKey = "Model";
            public const string MovementNameKey = "Movement";
            public const string RadarNameKey = "Radar";
            public const string WarheadNameKey = "Warhead";
            public const string HasAnchorKey = "HasAnchor";
            public const string DecoupleTimeKey = "DecoupleTime";
            public const string RaycastDistanceKey = "RaycastDistance";
            public const string DetonationDistanceKey = "DetonationDistance";

            private Framework_Actor_IniService iniService;

            public FcsApi_WeaponBlueprintFactory()
            {
                iniService = ObjectManager.GetObject<Framework_Actor_IniService>();
            }

            public FcsApi_Data_WeaponBlueprint CreateBlueprintFromIni(MyIni ini)
            {
                FcsApi_Data_WeaponType? type = null;
                if (ini.ContainsKey(Section, TypeKey))
                {
                    type = ParseWeaponType(iniService.GetStringOrDefault(ini, Section, TypeKey));
                }

                bool hasAnchor = false;
                if (ini.ContainsKey(Section, HasAnchorKey))
                {
                    hasAnchor = bool.Parse(iniService.GetStringOrDefault(ini, Section, HasAnchorKey));
                }

                float? decoupleTime = null;
                if (ini.ContainsKey(Section, DecoupleTimeKey))
                {
                    decoupleTime = float.Parse(iniService.GetStringOrDefault(ini, Section, DecoupleTimeKey));
                }

                double? raycastDistance = null;
                if (ini.ContainsKey(Section, RaycastDistanceKey))
                {
                    raycastDistance = double.Parse(iniService.GetStringOrDefault(ini, Section, RaycastDistanceKey));
                }

                double? detonationDistance = null;
                if (ini.ContainsKey(Section, DetonationDistanceKey))
                {
                    detonationDistance = double.Parse(iniService.GetStringOrDefault(ini, Section, DetonationDistanceKey));
                }

                return new FcsApi_Data_WeaponBlueprint()
                {
                    Title = iniService.GetStringOrDefault(ini, Section, TitleKey),
                    WeaponType = type,
                    ModelName = iniService.GetStringOrDefault(ini, Section, ModelNameKey),
                    MovementName = iniService.GetStringOrDefault(ini, Section, MovementNameKey),
                    RadarName = iniService.GetStringOrDefault(ini, Section, RadarNameKey),
                    WarheadName = iniService.GetStringOrDefault(ini, Section, WarheadNameKey),
                    HasAnchor = hasAnchor,
                    DecoupleTime = decoupleTime,
                    RaycastDistance = raycastDistance,
                    DetonationDistance = detonationDistance
                };
            }

            private FcsApi_Data_WeaponType ParseWeaponType(string typeString)
            {
                switch (typeString)
                {
                    case "Unguided":
                        return FcsApi_Data_WeaponType.Unguided;

                    case "AntiTankGuided":
                        return FcsApi_Data_WeaponType.AntiTankGuided;

                    case "AntiAircraftGuided":
                        return FcsApi_Data_WeaponType.AntiAircraftGuided;

                    case "UniversalGuided":
                        return FcsApi_Data_WeaponType.UniversalGuided;

                    case "ActiveAntiAircraftGuided":
                        return FcsApi_Data_WeaponType.ActiveAntiAircraftGuided;

                    case "ActiveUniversalGuided":
                        return FcsApi_Data_WeaponType.ActiveUniversalGuided;

                    default:
                        throw new Exception("Incorrect weapon type");
                }
            }
        }
    }
}
