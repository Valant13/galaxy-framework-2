﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System;
using VRage.Collections;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ObjectBuilders.Definitions;
using VRage.Game;
using VRage;
using VRageMath;

namespace IngameScript
{
    partial class Program
    {
        public static class FcsApi_DependencyConfig
        {
            public static void Setup()
            {
                Framework_DependencyConfig.Setup();

                var module = ObjectSetup.CreateModule("FcsApi");

                module.DefineObject(
                    typeof(FcsApi_IWeaponFactory),
                    new string[] { "Movements", "Radars", "Warheads" });

                module.DefineObject<FcsApi_WeaponBlueprintFactory>();
                module.DefineObject<FcsApi_WeaponConfig>();
                module.DefineObject<FcsApi_GuidanceConfig>();
            }
        }
    }
}
