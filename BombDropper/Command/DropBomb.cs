﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System;
using VRage.Collections;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ObjectBuilders.Definitions;
using VRage.Game;
using VRage;
using VRageMath;

namespace IngameScript
{
    partial class Program
    {
        public class BombDropper_Command_DropBomb : Framework_Terminal_AbstractCommand
        {
            public BombDropper_Command_DropBomb()
            {
                SetDescription("Drop bomb by dropper");
            }

            public override int Execute(Framework_Terminal_ISubject subject, Framework_Terminal_Input input, Framework_Terminal_Output output)
            {
                var bombDropper = (BombDropper_Component_IBombDropper)subject;
                if (bombDropper.IsBombReloading())
                {
                    output.WriteLine("Bomb is not ready");
                }
                else
                {
                    bombDropper.DropBomb();
                    output.WriteLine("Bomb dropped");
                }
                
                return 0;
            }

            public override string GetContextName()
            {
                return "bomb-dropper";
            }

            public override string GetName()
            {
                return "drop-bomb";
            }
        }
    }
}
