﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System;
using VRage.Collections;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ObjectBuilders.Definitions;
using VRage.Game;
using VRage;
using VRageMath;

namespace IngameScript
{
    partial class Program
    {
        public class BombDropper_Component_EasyBombDropper : Framework_Component_AbstractComponent, BombDropper_Component_IBombDropper
        {
            private const float bombDropDelay = 2;

            private ActorPack_Actor_Projector projector;
            private ActorPack_Actor_MergeBlockCollection mergeBlock;
            private ActorPack_Actor_WelderCollection welder;
            private float bombWeldingDelay;
            private bool isBombReloading = false;
            private Framework_Utility_Timer timer;

            public BombDropper_Component_EasyBombDropper(
                string id,
                Framework_Actor_Data_SearchCriteria projectorSearchCriteria,
                Framework_Actor_Data_SearchCriteria mergeBlockSearchCriteria,
                Framework_Actor_Data_SearchCriteria welderSearchCriteria,
                float bombWeldingDelay) : base(id)
            {
                projector = new ActorPack_Actor_Projector(projectorSearchCriteria);
                mergeBlock = new ActorPack_Actor_MergeBlockCollection(mergeBlockSearchCriteria);
                welder = new ActorPack_Actor_WelderCollection(welderSearchCriteria);

                this.bombWeldingDelay = bombWeldingDelay;

                timer = ObjectManager.GetObject<Framework_Utility_Timer>();

                welder.BlockWelded += Welder_BlockWelded;

                SetDescription("BombDropper_Component_EasyBombDropper");
            }

            public override string GetContextName()
            {
                return "bomb-dropper";
            }

            public void DropBomb()
            {
                if (!isBombReloading)
                {
                    mergeBlock.Disable();

                    isBombReloading = true;

                    timer.SetTimeout(bombDropDelay, () => {
                        mergeBlock.Enable();
                        projector.Enable();

                        welder.WeldBlock(bombWeldingDelay);
                    });
                }
            }

            public bool IsBombReloading()
            {
                return isBombReloading;
            }

            public event EventHandler BombReloaded;

            protected override void Cleanup()
            {
                projector.Destruct();
                mergeBlock.Destruct();
                welder.Destruct();
            }

            private void Welder_BlockWelded(object sender, EventArgs eventArgs)
            {
                if (isBombReloading)
                {
                    projector.Disable();
                    isBombReloading = false;

                    BombReloaded?.Invoke(this, new EventArgs());
                }
            }
        }
    }
}
