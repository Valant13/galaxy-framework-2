﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System;
using VRage.Collections;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ObjectBuilders.Definitions;
using VRage.Game;
using VRage;
using VRageMath;

namespace IngameScript
{
    partial class Program
    {
        public class BombDropper_Component_BombDropper : Framework_Component_AbstractComponent, BombDropper_Component_IBombDropper
        {
            private const float pistonVelocity = 5;
            private const float bombDropDelay = 2;

            private ActorPack_Actor_Projector projector;
            private ActorPack_Actor_MergeBlockCollection mergeBlock;
            private ActorPack_Actor_PistonCollection piston;
            private ActorPack_Actor_WelderCollection welder;
            private float[] bombWeldingDelays;
            private int bombLength;
            private int weldedBombLength;
            private bool isBombReloading = false;
            private Framework_Utility_Timer timer;

            public BombDropper_Component_BombDropper(
                string id,
                Framework_Actor_Data_SearchCriteria projectorSearchCriteria,
                Framework_Actor_Data_SearchCriteria mergeBlockSearchCriteria,
                Framework_Actor_Data_SearchCriteria pistonSearchCriteria,
                Framework_Actor_Data_SearchCriteria welderSearchCriteria,
                float[] bombWeldingDelays) : base(id)
            {
                projector = new ActorPack_Actor_Projector(projectorSearchCriteria);
                mergeBlock = new ActorPack_Actor_MergeBlockCollection(mergeBlockSearchCriteria);
                piston = new ActorPack_Actor_PistonCollection(pistonSearchCriteria);
                welder = new ActorPack_Actor_WelderCollection(welderSearchCriteria);

                bombLength = bombWeldingDelays.Length;
                this.bombWeldingDelays = bombWeldingDelays;

                timer = ObjectManager.GetObject<Framework_Utility_Timer>();

                piston.PositionReached += Piston_PositionReached;
                welder.BlockWelded += Welder_BlockWelded;

                SetDescription("BombDropper_Component_BombDropper");
            }

            public override string GetContextName()
            {
                return "bomb-dropper";
            }

            public void DropBomb()
            {
                if (!isBombReloading)
                {
                    mergeBlock.Disable();

                    isBombReloading = true;
                    weldedBombLength = 0;

                    timer.SetTimeout(bombDropDelay, () => {
                        mergeBlock.Enable();
                        projector.Enable();

                        piston.ReachPosition((bombLength - 1) * Framework_Actor_Data_BlockSize.Large, pistonVelocity);
                    });
                }
            }

            public bool IsBombReloading()
            {
                return isBombReloading;
            }

            public event EventHandler BombReloaded;

            protected override void Cleanup()
            {
                projector.Destruct();
                mergeBlock.Destruct();
                piston.Destruct();
                welder.Destruct();
            }

            private void Piston_PositionReached(object sender, EventArgs eventArgs)
            {
                if (isBombReloading)
                {
                    welder.WeldBlock(bombWeldingDelays[weldedBombLength]);
                }
            }

            private void Welder_BlockWelded(object sender, EventArgs eventArgs)
            {
                if (isBombReloading)
                {
                    weldedBombLength++;

                    if (weldedBombLength < bombLength)
                    {
                        piston.ReachPosition((bombLength - weldedBombLength - 1) * Framework_Actor_Data_BlockSize.Large, pistonVelocity);
                    }
                    else
                    {
                        projector.Disable();
                        isBombReloading = false;

                        BombReloaded?.Invoke(this, new EventArgs());
                    }
                }
            }
        }
    }
}
